$(function (){
    var tb_data_match = $("#tb-data-match").DataTable({
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }
    });

    var modal_html = 
    '<div id="data-match-del" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="billRuleDelModalLabel" aria-hidden="true">' +
      '<div class="modal-dialog">' +
        '<div class="modal-content">' +
          '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>  ' + 
            '<h4 class="modal-title">确认删除?</h4>' +
          '</div>' +
          '<div class="modal-footer">' +
            '<button class="btn btn-default" data-dismiss="modal">取消</button>' +
            '<button class="btn btn-danger" data-dismiss="modal" id="data-match-del-btn">确认</button>' +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>';

    $('body').append(modal_html);

    // console.log(tb_data_match.ajax.url("text.json").load);
    $('#tb-data-match').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

    });

    $('#data-match-del-btn').click( function () {
        console.log(tb_data_match.rows('.selected'));
        console.log( tb_data_match.rows('.selected').data()[0]); //被选择的数据(array)
        console.log( tb_data_match.rows('.selected').data()[0][1]); //被选择的数据(array)
        console.log( tb_data_match.rows('.selected').data()); //被选择的数据(array)
        tb_data_match.row('.selected').remove().draw( false );
    }); 

    var url = "assets/php/";
    $('#fileupload').fileupload({
        url: url,
        add : function(e, data) {
            var uploadErrors = [];
            if (!(/(\.|\/)(xls|xlsx)$/i)
                    .test(data.files[0].name)) {
                uploadErrors
                        .push('文件格式错误!文件格式必须是xlx或者xlsx!');
            }
            if (data.files[0].size > 5000000) {
                uploadErrors
                        .push('文件过大!');
            }
            if (uploadErrors.length > 0) {
                alert(uploadErrors.join("\n"));
            } else {
                data.submit();
                $('#fileupload').fileupload('disable');
            }
        },
        dataType: 'json',
        autoUpload: true,
        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        // maxFileSize: 5000000,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                // console.log(file.name);
                $('<p/>').text(file.name).appendTo('#files');
                $('<p/>').text('上传成功').appendTo('#files');
                $("#staff-avatar").attr("src", "assets/php/files/"+file.name);
                $('input[name="file-name"]').val(file.name);
                console.log(tb_data_match);
                // tb_data_match.ajax.url("assets/text.json").load();  //模拟加载
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');


});