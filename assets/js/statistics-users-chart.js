var test_array = [1,2,3,4,5,6,7,8,9,0,11,12]; //statistics-users-chart需要显示的数据

$(function (){
        // 用户统计表 
   var users_statistics = $("#users-statistics")[0];
   var users_percent_statistics = $("#users-percent-statistics")[0];
        require.config({
            paths: {
                echarts: 'assets/echarts-2.2.2/build/dist'
            }
        });
        
    // 使用
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],
        function (ec) {
            // 基于准备好的dom，初始化echarts图表
            var users_chart = ec.init(users_statistics, theme); 
            var users_percent_chart = ec.init(users_percent_statistics, theme); 
            
            var users_chart_option = {
                title: {
                    x: 'center',
                    text: '统计用户数'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    data:['业主', '亲戚', '租客']
                },
                toolbox: {
                    show: true,
                    orient: 'horizontal',
                    x: 'right',
                    y: 'bottom',
                    color : ['#1e90ff','#22bb22','#4b0082','#d2691e'],
                    backgroundColor:'rgba(0,0,0,0)',
                    borderColor: '#ccc',
                    borderWidth: 0,            
                    padding: 5,                
                    showTitle: true,
                    feature : {
                        mark : {show: true},
                        // dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        data : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        "name":"业主",
                        "type":"bar",
                        // "data": test_array
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                    {
                        "name":"亲戚",
                        "type":"bar",
                        // "data": test_array
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                    {
                        "name":"租客",
                        "type":"bar",
                        // "data": test_array
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    }
                ]
            };

            var users_percent_statistics_option = {
                title : {
                    text: '用户类型百分比统计',
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    data:['业主','亲戚','租客']
                },
                toolbox: {
                    x: 'right',
                    y: 'bottom',
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: false, readOnly: false},
                        magicType : {
                            show: true, 
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                series : [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius : '55%',
                        center: ['50%', '60%'],
                        data:[
                            {value:335, name:'业主'},
                            {value:310, name:'亲戚'},
                            {value:234, name:'租客'},
                        ]
                    }
                ]
            };
                    
    
            // 为echarts对象加载数据 
            users_chart.setOption(users_chart_option); 
            users_percent_chart.setOption(users_percent_statistics_option); 
        }
    ); 

    $("#table-chart-select .choose-year li").click(function (){
        console.log('user-chart-year:' + $(this).text());
        //ajax
    });
});

