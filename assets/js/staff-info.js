$(function() {
    var tb_staff = $("#tb-staff").DataTable({
        'dom':'lrtip',
        responsive: true,
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ],
    });

    $('#tb-staff tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

    });

    var modal_html =
    '<div id="staff-del" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="userDelModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
              '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button> ' +  
              '<h4 class="modal-title">确认删除?</h4>' +
            '</div>' +
            '<div class="modal-footer">' +
              '<button class="btn btn-default" data-dismiss="modal">取消</button>' +
              '<button id="staff-del-btn" class="btn btn-danger" data-dismiss="modal">确认</button>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>';

    $('body').append(modal_html);

    $('#staff-del-btn').click( function () {
        console.log(tb_staff.rows('.selected'));
        console.log( tb_staff.rows('.selected').data()[0]); //被选择的数据(array)
        console.log( tb_staff.rows('.selected').data()[0][1]); //被选择的数据(array)
        console.log( tb_staff.rows('.selected').data()); //被选择的数据(array)
        tb_staff.row('.selected').remove().draw( false );
    } );

} );

