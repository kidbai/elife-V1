$(function(){
    $("#tb-public-monitoring").DataTable({
        'dom':'lrtip',
        "processing": true,					//加载数据时显示正在加载信息
    		"serverSide": true,
    		"iDisplaylength":10,
    		"sPaginationType" : "full_numbers",
    		"bSort":true,
        "language": {
            "url": "assets/js/dtCH.json"
        },
        "ajax":{
        	"url":"home/getRepairJson",
        	"type":"POST"
        },
        "columns":[
        		{"data":"rowNo"},
		        {"data":"orderNo"},
		        {"data":"status"},
		        {"data":"roomId"},
		        {"data":"ownerUser"},
		        {"data":"addTime"},
		        {"data":"forwardTime"},
		        {"data":"staffName"},
		        {"data":"staffMobile"},
		        {"data":"serviceStar"},
		        {"data":"techStar"},
		        {"data":null}
        ],
        "columnDefs" : [
         	{
		        "render" : function(data, type, row) {
		          	switch(data){
		          		case "0":
		          			return "<label class='label label-danger'>待处理</lable>";
		          		break;
		          		case "1":
		          			return "<label class='label label-primary'>处理中</lable>";
		          		break;
		          		case "2":
		          			return "<label class='label label-info'>已完成(待评价)</lable>";
		          		break;
		          		case "3":
		          			return "<label class='label label-success'>已完成(已评价)</lable>";
		          		break;
		          		case "4":
		          			return "<label class='label label-default'>无效数据</lable>";
		          		break;
		          		case "5":
		          			return "<label class='label label-warning'>用户撤销</lable>";
		          		break;
		          		default:
		          			return "<label class='label label-danger'>未知数据</lable>";
		          		break;
		          	}
		        },
		        "targets" : 2
      		},{
      			"render":function(data,type,row){ 
      			    var room = data.split("-");
      			    return room[1]+"栋"+room[2]+"单元"+room[3];
      			},
      			"targets":3
      		},{
      			"render" : function(data,type,row){
      			    var str = data.roomId.split("-");
      			    var proc = " <a href='home/resident_repair_edit/"+str[0]+"/"+data.orderNo+"' class='btn btn-primary' rel='bill-rule-edit'>处理</a>";
      			    var success = "<a href='home/resident_repair_proc/"+str[0]+"/"+data.orderNo+"' class='btn btn-success'>完工</a>";
      			    var view = " <a href='home/resident_repair_edit/"+str[0]+"/"+data.orderNo+"' class='btn btn-primary' rel='bill-rule-edit'>查看</a>";
      				switch(data.status){
      					case "0" :
      						return proc;
      					break;
      					case "1" :
      						return proc+"&nbsp;"+success;
      					break;
      					case "2" :
      					case "3" :
      						return proc;
      					break;
      					case "4" :
      					case "5" :
      					default :
      						return view;
      					break;
      				}
      			},
      			"targets" :11
      		}
      	]
    });

})