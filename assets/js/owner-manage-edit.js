$(function (){
    $('#register-time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });

    // 设置时间空间最小日期
    var date = new Date();
    $("#register-time").datetimepicker('setStartDate', date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate());
});