$(function (){
    $("#tb-role").DataTable({
        responsive: true,
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ]
    });
});