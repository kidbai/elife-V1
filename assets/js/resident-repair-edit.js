$(function (){
    // 时间控件初始化
    $('#appointment-startTime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        showMeridian: 1,
        pickerPosition: "bottom-left"
    });

    // 设置时间空间最小日期
    var date = new Date();
    $("#change-appointment-time").click(function (){
        // $(this).siblings('.btn-success').toggleClass('on');
        $(".change-appointment-time").toggleClass('on');
        if( $(this).siblings('.btn.success').hasClass('on'))
        {
            $("#change-appointment-time").text('返回');
        }
        else
        {
            $("#change-appointment-time").text('修改预约时间');
        }
    });
    $("#change-appointment-time-submit").click(function (){
        console.log($(".change-appointment-time input").val());
        console.log($(".change-appointment-time select option:selected").text());
        var day = $(".change-appointment-time input").val();
        var time = $(".change-appointment-time select option:selected").text();
        $('input[name="forwardTime"]').val(day + time);
    });

    $("#reset-appointment-time").click(function (){
        console.log("123");
        var date = $('input[name="forwardTime"]').siblings('input[type="hidden"]').val();
        $('input[name="forwardTime"]').val(date);
    });

    // Ajax 
    var modalId;
    $("#repair-edit-btn").click(function (){
        BootstrapDialog.show({
            title: '系统提示',
            message: '确认修改?',
            type: 'type-danger',
            buttons: [{
                label: '取消',
                cssClass: 'btn-default',
                action: function(dialog) {
                    dialog.close();
                }
            }, {
                label: '确认',
                cssClass: 'btn-danger',
                action: function(dialog) {
                    dialog.close();
                    // var orderNo = $('[name="orderNo"]').val();
                    // var forwardTime = $('[name="forwardTime"]').val();
                    // var orderDesc = $('[name="orderDesc"]').val();
                    // var staffCode = $('[name="staffCode"]').val();
                    // var propReply = $('[name="propReply"]').val();
                    // var status = $('[name="status"]').val();
                    $.ajax({
                        url: 'home/repairSubmit',
                        type: 'POST',
                        dataType: 'json',
                        data: $("#repairForm").serialize(),
                        success: function(data)
                        {
                            var info = '';
                            if(data)
                            {
                                BootstrapDialog.show({
                                    title: '结果',
                                    message: '修改成功',
                                    buttons: [{
                                        label: '确定',
                                        cssClass: 'btn-default',
                                        action: function(dialog) {
                                            dialog.close();
                                        }
                                    }],
                                    type: 'type-success',
                                    size: 'size-small',
                                    action: function(dialog){
                                        dialog.close();
                                    }
                                });
                            }
                            else
                            {
                                info = '修改失败';
                                BootstrapDialog.alert("修改失败");
                            }
                        } 
                    });
                }
            }]
        });
    });
});

