$(function (){
    $("#tb-statistics-visitor").DataTable({
        responsive: true,
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ]
    });
    $(".dataTables_length").parent().addClass('col-xs-6');
    $(".dataTables_filter").parent().addClass('col-xs-6');

    $('#statistics-visitor-years').change(function (){
        var select_year = $(this).children('option:selected').val();
        console.log(select_year);
        //ajax
    });
});