$(function() {
    var tb_news = $("#tb-news").DataTable({
        'dom':'lrtip',
        responsive: true,
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "../../assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ],
        fnDrawCallback: function( oSettings ) {
          console.log( 'DataTables has redrawn the table' );
        }
    });

    $('#tb-news tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

    });

    var modal_html = 
    '<div id="news-del" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="communityNewsModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
              '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>   ' +
              '<h4 class="modal-title">确认删除?</h4>' +
            '</div>' +
            '<div class="modal-footer">' +
              '<button class="btn btn-default" data-dismiss="modal">取消</button>' +
              '<button id="news-del-btn" class="btn btn-danger" data-dismiss="modal">确认</button>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>';

    $('body').append(modal_html);

    $('#news-del-btn').click( function () {
        console.log(tb_news.rows('.selected'));
        console.log( tb_news.rows('.selected').data()[0]); //被选择的数据(array)
        console.log( tb_news.rows('.selected').data()[1]); //被选择的数据(array)
        console.log( tb_news.rows('.selected').data()[0][1]); //被选择的数据(array)
        console.log( tb_news.rows('.selected').data()); //被选择的数据(array)
        tb_news.row('.selected').remove().draw( false );
    } ); 

    //置顶
    $('button[rel="top"]').click(function (){
        if($(this).hasClass('btn-default'))
        {
            $(this).removeClass('btn-default');
            $(this).addClass('btn-warning');
            $(this).addClass('top');
            console.log($(this).parent().parent());
            $("#tb-news tbody tr:eq(0)").before($(this).parent().parent());
        }
        else
        {
            $(this).addClass('btn-default');
            $(this).removeClass('btn-warning');
            $(this).removeClass('top');
        }
    });
} );

