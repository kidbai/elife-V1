$(function (){

    //选择缴费类型
    $('input[rel="offline-bill-type"]').change(function (){
        if($(this).is(":checked"))
        {
            $(this).parent('label').siblings('label[rel="offline-months"]').children('select').removeAttr('disabled');
            // $(this).parent('label').siblings('label[rel="offline-total"]').children('input').removeAttr('readonly');
            var bill_rule_array = $(this).parent('label').siblings('input[rel="bill-rule"]').val().split('-'); 
            var bill_total = parseInt(bill_rule_array[0]) * parseInt(bill_rule_array[1]);
            var bill_init = parseInt($(this).parent('label').siblings('label[rel="offline-months"]').children('select').val());
            bill_total *= bill_init;
            $(this).parent('label').siblings('label[rel="offline-total"]').children('input').val(bill_total + '元');
            console.log(bill_total);

        }
        else
        {
            console.log( $(this).parent('label').siblings('label[rel="offline-months"]').children('select'));
            $(this).parent('label').siblings('label[rel="offline-months"]').children('select').attr('disabled', 'disabled');
            // $(this).parent('label').siblings('label[rel="offline-total"]').children('input').attr('readonly', 'readonly');
            $(this).parent('label').siblings('label[rel="offline-total"]').children('input').val('');
            $(this).parent('label').siblings('label[rel="offline-months"]').children('select').val('1');
        }
    });

    //根据缴费月数显示缴费总额
    $('label[rel="offline-months"] > select').change(function (){
        console.log($(this).val());
        var bill_rule_array = $(this).parent().siblings('input[rel="bill-rule"]').val().split('-');
        var bill_total = parseInt(bill_rule_array[0]) * parseInt(bill_rule_array[1]) * parseInt($(this).val());
        console.log(bill_rule_array);
        console.log(bill_total);
        $(this).parent().siblings('label[rel="offline-total"]').children('input').val(bill_total + '元');
    });

    //确认缴费信息
    $("#offline-bill-pay").click(function (){
        
        BootstrapDialog.show({
          title: '确认缴费?',
          type: 'type-warning',
          buttons: [{
              label: '取消',
              action: function(dialog){
                  dialog.close();
              }
          }, {
              label: '确认',
              cssClass: 'btn-primary',
              action: function(dialog){
                dialog.close();
                //  获取缴费结果
                var bill_total = 0;
                var bill_data = {};
                $.each($('input[rel="offline-bill-type"]'), function(){
                    var parseInt_bill = 0;
                    if($(this).is(":checked"))
                    {
                        if($(this).parent().siblings('label[rel="offline-total"]').children('input').val() != '')
                        {
                            var bill_name = $(this).parent().siblings('label[rel="offline-total"]').attr('for');
                            parseInt_bill = parseInt($(this).parent().siblings('label[rel="offline-total"]').children('input').val());
                            bill_data[bill_name] = parseInt_bill;
                            console.log(JSON.stringify(bill_data));
                        }
                    }
                    bill_total += parseInt_bill;
                    
                });
                console.log("总额");
                console.log(bill_total);
                //  Ajax
                BootstrapDialog.show({
                  title: '缴费结果',
                  type: 'type-danger',  //type-success  
                  message: '缴费失败',  // 缴费成功
                  buttons: [{
                      label: '确定',
                      action: function(dialog){
                          dialog.close();
                      }
                  }]
                });
              }
          }]
        });
    });

});
