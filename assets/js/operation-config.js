$(function (){
    var tb_operation_config = $("#tb-operation-config").DataTable({
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }
    });

    var modal_html = 
    '<div id="operation-config-del" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="billRuleDelModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
              '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button> ' +  
              '<h4 class="modal-title">确认删除?</h4>' +
            '</div>' +
            '<div class="modal-footer">' +
              '<button class="btn btn-default" data-dismiss="modal">取消</button>' +
              '<button class="btn btn-danger" data-dismiss="modal" id="operation-config-del-btn">确认</button>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>'; 

    $('body').append(modal_html);

    //删除news
    $('#tb-operation-config tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

    });

    $('#operation-config-del').click( function () {
        console.log(tb_operation_config.rows('.selected'));
        console.log( tb_operation_config.rows('.selected').data()[0]); //被选择的数据(array)
        console.log( tb_operation_config.rows('.selected').data()[0][1]); //被选择的数据(array)
        console.log( tb_operation_config.rows('.selected').data()); //被选择的数据(array)
        tb_operation_config.row('.selected').remove().draw( false );
    } ); 
    
    $(".operation-config-select").change(function (){
        console.log($(this).val());
        switch($(this).val())
        {
          case 'numeric' :
              $('.operation-config').removeClass('on');
              $('input[rel="numeric"]').addClass('on');
              break;

          case 'string' :
              $('.operation-config').removeClass('on');
              $('input[rel="string"]').addClass('on');
              break;

          case 'date' :
              $('.operation-config').removeClass('on');
              $('input[rel="date"]').addClass('on');
              break;
        }
    });

    $("#operation-config-btn").click(function (){
        console.log('helo');
        var config_arg = $('input[name="config-arg"]').val();
        var config_setting = $('.operation-config.on').val();
        console.log(config_arg);
        console.log(config_setting);
        $.ajax({
          url: '/path/to/file',
          type: 'POST',
          data: {config_arg: config_arg, config_setting: config_setting },
          success:function(data)
          {

          }
        });
    });
});