var startDate=null;
var endDate=null;
$(function (){
    var tb_resident_repair = $("#tb-resident-repair").DataTable({
        'dom':'lrtip',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
       	"processing": true,					//加载数据时显示正在加载信息
    		"serverSide": true,
    		"iDisplaylength":10,
    		"sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        "ajax":{
        	"url":"home/getRepairJson",
        	"type":"POST",
          // 1434962757322
          "data": function ( d ) {
            //添加额外的参数传给服务器
            var data = $.getToolBarValue();
            if(data == undefined )
            {
              return;
            }
            else 
            {
              d.extra_search = data;
            }
          }
        },
        "columns":[
            {"data": null},
        		{"data":"rowNo"},
		        {"data":"orderNo"},
		        {"data":"status"},
		        {"data":"roomId"},
		        {"data":"ownerUser"},
		        {"data":"addTime"},
		        {"data":"forwardTime"},
		        {"data":"staffName"},
		        {"data":"staffMobile"},
		        {"data":"serviceStar"},
		        {"data":"techStar"},
		        {"data":null}
        ],
         "columnDefs" : [
         	{
		        "render" : function(data, type, row) {
		          	switch(data){
		          		case "0":
		          			return "<label class='label label-danger'>待处理</lable>";
		          		break;
		          		case "1":
		          			return "<label class='label label-primary'>处理中</lable>";
		          		break;
		          		case "2":
		          			return "<label class='label label-info'>已完成(待评价)</lable>";
		          		break;
		          		case "3":
		          			return "<label class='label label-success'>已完成(已评价)</lable>";
		          		break;
		          		case "4":
		          			return "<label class='label label-default'>无效数据</lable>";
		          		break;
		          		case "5":
		          			return "<label class='label label-warning'>用户撤销</lable>";
		          		break;
		          		default:
		          			return "<label class='label label-danger'>未知数据</lable>";
		          		break;
		          	}
		        },
		        "targets" : 3
      		},{
      			"render":function(data,type,row){ 
      			    var room = data.split("-");
      			    return room[1]+"栋"+room[2]+"单元"+room[3];
      			},
      			"targets":4
      		},{
      			"render" : function(data,type,row){
      			    var str = data.roomId.split("-");
      			    var proc = " <a href='home/resident_repair_edit/"+str[0]+"/"+data.orderNo+"' class='btn btn-primary' rel='bill-rule-edit'>处理</a>";
      			    var success = "<a href='home/resident_repair_proc/"+str[0]+"/"+data.orderNo+"' class='btn btn-success'>完工</a>";
      			    var view = " <a href='home/resident_repair_edit/"+str[0]+"/"+data.orderNo+"' class='btn btn-primary' rel='bill-rule-edit'>查看</a>";
      				switch(data.status){
      					case "0" :
      						return proc;
      					break;
      					case "1" :
      						return proc+"&nbsp;"+success;
      					break;
      					case "2" :
      					case "3" :
      						return proc;
      					break;
      					case "4" :
      					case "5" :
      					default :
      						return view;
      					break;
      				}
      			},
      			"targets" :12
      		}
      	],
        "fnDrawCallback": function() {
          $.initTableRowNumber(tb_resident_repair);
        }
    });
  
    //  给每行加上索引

    // 初始化筛选框参数 searchArrayItem
    var searchArrayItem = {};
    searchArrayItem['roomId'] = '房间号';
    searchArrayItem['orderNo'] = '保修单';
    searchArrayItem['appUser'] = '报修人';
    searchArrayItem['appUser-phone'] = '报修人电话';

    // 初始化状态栏参数 statusArray
    var statusArray = {};
    statusArray['1'] = '无效';
    statusArray['2'] = '已完成';

    // 初始化类型参数 typeArray
    var typeArray = {};
    typeArray['1'] = '设备';
    typeArray['2'] = '环境';
    typeArray['3'] = '人员';
    typeArray['4'] = '其他';

    // 初始化url地址、tableId
    var url = "/home/getRepairJson";
    var tableId = 'tb-resident-repair';

    // 初始化操作按钮
    var buttonGroup = {};
    buttonGroup['add'] = '添加';
    buttonGroup['del'] = '删除';
    buttonGroup['search'] = '搜索';
    // 对应编辑界面url
    var editUrl = "home/resident_repair_edit";
    // $.buttonGroupInit(buttonGroup, 'tb-resident-repair', editUrl);
    $.buttonGroupInit(true, 'tb-resident-repair', editUrl);   // 第一个参数如果是true，则会默认加载全部三个按钮
    $.tableDelDisabled('tb-resident-repair');
    $.tableDataDel('tb-resident-repair', '/home/repairDel', tb_resident_repair);
    $.toolbarInit(tableId, tb_resident_repair, searchArrayItem, statusArray, false, url);
});

