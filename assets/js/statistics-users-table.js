$(function (){
    $("#tb-statistics-users").DataTable({
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        // ajax: {
        //     "url": "scripts/server_processing.php",
        //     "data": function ( d ) {
        //         d.myKey = "myValue";
        //         // d.custom = $('#myInput').val();
        //         // etc
        //     }
        // }
        order: [ 0, 'asc' ]
    });

    $(".dataTables_length").parent().addClass('col-xs-6');
    $(".dataTables_filter").parent().addClass('col-xs-6');

    $('#statistics-users-years').change(function (){
        var select_year = $(this).children('option:selected').val();
        console.log(select_year);
        //ajax
    });
});