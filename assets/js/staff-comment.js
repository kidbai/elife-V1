$(function (){
    $("#tb-staff-comment").dataTable({
        'dom':'lrtip',
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        },  
        order: [ 0, 'asc' ]
    });
});