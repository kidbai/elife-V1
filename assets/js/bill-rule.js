$(function() {
    //初始化账单规则数据表 
    var tb_bill_rule = $("#tb-bill-rule").DataTable({
        'dom':'lrtip', 
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
    });

    $('#tb-bill-rule tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        if($("#tb-bill-rule tbody tr").hasClass('selected'))
        {
            $('button[data-target="#bill-rule-del"]').removeClass("disabled");
        }
        else 
        {
            $('button[data-target="#bill-rule-del"]').addClass("disabled");
        }
    });

    $('#bill-rule-del-btn').click( function () {
        console.log(tb_bill_rule.rows('.selected'));
        console.log( tb_bill_rule.rows('.selected').data()[0]); //被选择的数据(array)
        console.log( tb_bill_rule.rows('.selected').data()[0][1]); //被选择的数据(array)
        console.log( tb_bill_rule.rows('.selected').data()); //被选择的数据(array)
        tb_bill_rule.row('.selected').remove().draw( false );
    } );

});


