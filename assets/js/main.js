function startTime()
{
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m=checkTime(m);
    s=checkTime(s);
    $('#clock i.time').text(h+":"+m+":"+s);
    setTimeout('startTime()',500);
}

function checkTime(i)
{
    if (i<10) 
      {i="0" + i}
      return i;
}

//cookie类
var cookieTool = {
//读取cookie
    getCookie: function(c_name) {
        if (document.cookie.length > 0) {
                c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    },
    //设置cookie
    setCookie: function(c_name, value, expiredays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + expiredays); //设置日期
        document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
    },
    //删除cookie
    delCookie: function(c_name) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() - 1); //昨天日期
        document.cookie = c_name + "=;expires=" + exdate.toGMTString();
    }
};

// function get_push_msg()
// {
//     var get_push_msg = true;
//     $.ajax({
//         url: '/stencil-master/index.php/home/get_push_msg',
//         type: 'GET',
//         data:{ get_push_msg },
//         success: function(data)
//         {
//             //if data === '' push-msg-wrapper hide
//             //else show
//             console.log('返回值')
//             console.log(data); 
//             $(".push-msg-wrapper .push-msg span").append(data['num']); 
//             $("ul.user li .badge").append(data['num']);
//             $(".push-msg-wrapper").addClass('on');
//             setTimeout('$(".push-msg-wrapper").removeClass("on")', 10000);
//         }
//     });
// }

$(function (){

    //设置时钟
    startTime();

    $("#dataTables-fix").DataTable({
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ]
    });
    

    $("#dataTables-bill").DataTable({
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ]
    });

    $("#setting").click(function (){
        $("#function").addClass('on');
    });
    $('[data-toggle=offcanvas]').click(function() {
        $('.content-wrap').toggleClass('on');
    });
    
    $("#login-btn").click(function (){
        $("#login-window").slideToggle("fast");
    });
    $(".resident-info .box-content ul > li").click(function (ev){
        ev.preventDefault();
        console.log($(this).children('a').attr('href'));
        var index_tab_id = $(this).children('a').attr('href');
        $("#tabs-content .tabs-content-list").removeClass('active');
        $(index_tab_id).addClass("active");
        $(".resident-info .box-content ul > li").removeClass('active');
        $(this).addClass('active');
    });

    $("#admin-dropdown").click(function (e){
        console.log(e.target);
        $(this).toggleClass('on');
    });
    $("#admin-dropdown .admin-menu li a").click(function (e){
        e.preventDefault();
    });

    //统计按钮
    $("#statistics-nav li").click(function (){
        $("#statistics-nav li").removeClass('active');
        $(this).addClass('active'); 
        var tab = "#" + $(this).attr('rel');
        console.log(tab);
    });

    $("#table-chart-select .fa-list").click(function (e){
        console.log("this");
        e.preventDefault();
        $(this).toggleClass('on')
    });


    //sidebar
    var Sidebar = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.link');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown);
    }

    Sidebar.prototype.dropdown = function(e) {
        var $el = e.data.el;
            $this = $(this),as
            // $next = $this.next();

        // $next.slideToggle();
        $("#sidebar li").removeClass('open');
        $this.parent().toggleClass('open');
        var c_array = [];
        //激活添加cookie ，取消删除
        console.log($this);
        if(parseInt($this.parent().children('a').length) > 0)
        {
            if($this.parent().hasClass('open'))
            {
                c_array.push($this.parent().attr('rel'));
                c_array.push('open');
                cookieTool.setCookie('c_array', c_array, 10);
            }
            else
            {
                cookieTool.delCookie('c_array');
            }
        }
        // if (!e.data.multiple) {
        //     $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        // };
    }   

    var sidebar = new Sidebar($('#sidebar'), false);

    $(".submenu a").click(function (e){
        // console.log('submenu click');
        // console.log($(this).parent().parent().parent().attr('rel'));
        // console.log($(this).attr('rel'));
        var c_array = [];
        
        if($(this).parent().parent().parent().hasClass('open'))
        {
            cookieTool.delCookie('c_array');
            c_array.push($(this).parent().parent().parent().attr('rel'));
            c_array.push($(this).attr('rel'));
            c_array.push('open');
            cookieTool.setCookie('c_array', c_array, 10);
        }
        else
        {
            cookieTool.delCookie('c_array');
        }
        console.log(cookieTool.getCookie('c_array'));
    });

    //加载菜单cookie设置
    var cookie_array = cookieTool.getCookie('c_array').split(',');
    if(cookie_array.length <= 2)
    {
        $("#sidebar li").each(function (){
            // console.log($(this).attr('rel'));
            if($(this).attr('rel') === cookie_array[0])
            {
                $(this).addClass(cookie_array[1]);
            }
        });
    }
    else
    {
        $("#sidebar li").each(function (){
            if($(this).attr('rel') === cookie_array[0])
            {
                $(this).addClass('active');
                // console.log($(this).children('.link').siblings('.submenu').children('li'));
                $(this).children('.nav-second-level').siblings('li').children('a').each(function (){
                    // console.log($(this).children('a').attr('rel'));
                    if($(this).children('a').attr('rel') === cookie_array[1])
                    {
                        $(this).children('a').addClass('active');
                    }
                }); 
            }
        });
    }
    //推送消息
    // setTimeout('get_push_msg()', 1000);
});
    
    
