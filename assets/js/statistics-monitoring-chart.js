$(function (){
        // 用户统计表 
   var monitoring_statistics = $("#monitoring-statistics")[0];
   var monitoring_percent_statistics = $("#monitoring-percent-statistics")[0];
        require.config({
            paths: {
                echarts: 'assets/echarts-2.2.2/build/dist'
            }
        });
        
    // 使用
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],
        function (ec) {
            // 基于准备好的dom，初始化echarts图表
            var monitoring_chart = ec.init(monitoring_statistics, theme); 
            var monitoring_percent_chart = ec.init(monitoring_percent_statistics, theme); 
            
            var monitoring_option = {
                title: {
                    x:'center',
                    text: '监察统计'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    orient : 'vertical',
                    x:'left',
                    data:['环境', '设备', '人员', '其他']
                },
                toolbox: {
                    show: true,
                    orient: 'horizontal',
                    x: 'right',
                    y: 'bottom',
                    color : ['#1e90ff','#22bb22','#4b0082','#d2691e'],
                    backgroundColor:'rgba(0,0,0,0)',
                    borderColor: '#ccc',
                    borderWidth: 0,            
                    padding: 5,                
                    showTitle: true,
                    feature : {
                        mark : {show: true},
                        // dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        data : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        "name":"环境",
                        "type":"bar",
                        "data": (function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                    {
                        "name":"设备",
                        "type":"bar",
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                    {
                        "name":"人员",
                        "type":"bar",
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                    {
                        "name":"其他",
                        "type":"bar",
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    }
                ]
            };

            var monitoring_percent_option = {
                title : {
                    text: '监察类型百分比统计',
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    data:['环境','设备','人员','其他']
                },
                toolbox: {
                    x: 'right',
                    y: 'bottom',
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: false, readOnly: false},
                        magicType : {
                            show: true, 
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                series : [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius : '55%',
                        center: ['50%', '60%'],
                        data:[
                            {value:1023, name:'环境'},
                            {value:899, name:'设备'},
                            {value:1259, name:'人员'},
                            {value:1259, name:'其他'}
                        ]
                    }
                ]
            };
    
            // 为echarts对象加载数据 
            monitoring_chart.setOption(monitoring_option); 
            monitoring_percent_chart.setOption(monitoring_percent_option); 
        }
    );
    $("#table-chart-select .choose-year li").click(function (){
        console.log('monitoring-chart-year:' + $(this).text());
        //ajax
    });
});

