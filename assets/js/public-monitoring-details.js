$(function (){
    var tb_guide = $("#tb-public-monitoring-comment").DataTable({
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ],
    });

    var html = 
    '<div id="blueimp-gallery" class="blueimp-gallery">' +
      '<div class="slides"></div>' +
      '<h3 class="title"></h3>' +
      '<a class="prev">‹</a>' +
      '<a class="next">›</a>' +
      '<a class="close">×</a>' +
      '<a class="play-pause"></a>' +
      '<ol class="indicator"></ol>' +
    '</div>' +

    '<div id="public-monitoring-update-status" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="guideModalLabel" aria-hidden="true">' +
      '<div class="modal-dialog">' +
        '<div class="modal-content">' +
          '<div class="modal-header">' +
            '<div class="alert alert-success">更新成功</div>' +
            '<div class="alert alert-danger">更新失败</div>' +
          '</div>' +
          '<div class="modal-footer">' +
            '<button class="btn btn-primary" data-dismiss="modal">确认</button>' +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>';

    $('body').append(html)

    $("#public-status-btn").click(function (){
        // $.ajax
        $("#public-monitoring-update-status").modal("show");
    }); 

    $("#public-comment-btn").click(function (){
        //$.ajax
        $("#public-monitoring-update-status").modal("show");
    });
    
    document.getElementById('links').onclick = function (event) {
        console.log(event);
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    };
});