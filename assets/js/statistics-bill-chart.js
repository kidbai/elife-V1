$(function (){
        // 用户统计表 
   var bill_total_statistics = $("#bill-total-statistics")[0];
   var bill_fee_statistics = $("#bill-fee-statistics")[0];
   var bill_percent_statistics = $("#bill-percent-statistice")[0];

        require.config({
            paths: {
                echarts: 'assets/echarts-2.2.2/build/dist'
            }
        });
        
    // 使用
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],
        function (ec) {
            // 基于准备好的dom，初始化echarts图表
            var bill_total_chart = ec.init(bill_total_statistics, theme); 
            var bill_fee_chart = ec.init(bill_fee_statistics, theme);
            var bill_percent_chart = ec.init(bill_percent_statistics, theme);
            
            var bill_total_option = {
                title: {
                    text: '账单总计'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data:['物业费', '水电费', '停车费']
                },
                toolbox: {
                    show: true,
                    orient: 'horizontal',
                    x: 'right',
                    y: 'bottom',
                    color : ['#1e90ff','#22bb22','#4b0082','#d2691e'],
                    backgroundColor:'rgba(0,0,0,0)',
                    borderColor: '#ccc',
                    borderWidth: 0,            
                    padding: 5,                
                    showTitle: true,
                    feature : {
                        mark : {show: true},
                        // dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : false,
                xAxis : [
                    {
                        type : 'category',
                        data : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        "name" : "物业费",
                        "type" : "bar",
                        "data" : [1000, 500, 300, 200, 300 ,500, 1000, 500, 300, 200, 300 ,500],
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        }
                    },
                    {
                        "name" : "水电费",
                        "type" : "bar",
                        "data" : [500, 400, 200, 300 ,500 ,800, 500, 400, 200, 300 ,500 ,800],
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        }
                    },
                    {
                        "name" : "停车费",
                        "type" : "bar",
                        "data" : [400, 200, 750, 300, 250, 120, 400, 200, 750, 300, 250, 120],
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        }
                    }
                ]
            };

            var bill_fee_option = {
                title: {
                    text: '缴费情况'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data:['已缴', '未缴', '实际缴费']
                },
                toolbox: {
                    show: true,
                    orient: 'horizontal',
                    x: 'right',
                    y: 'bottom',
                    color : ['#1e90ff','#22bb22','#4b0082','#d2691e'],
                    backgroundColor:'rgba(0,0,0,0)',
                    borderColor: '#ccc',
                    borderWidth: 0,            
                    padding: 5,                
                    showTitle: true,
                    feature : {
                        mark : {show: true},
                        // dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar','stack', 'tiled']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : false,
                xAxis : [
                    {
                        type : 'category',
                        data : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        "name":"已缴",
                        "type":"line",
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                    {
                        "name":"未缴",
                        "type":"line",
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                    {
                        "name":"实际缴费",
                        "type":"line",
                        "data":(function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                ]
            };

            var bill_percent_option = {
                title : {
                    text: '账单百分比统计',
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    data:['物业费总计','水电费总计','停车费总计']
                },
                toolbox: {
                    x: 'right',
                    y: 'bottom',
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: false, readOnly: false},
                        magicType : {
                            show: true, 
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                series : [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius : '55%',
                        center: ['50%', '60%'],
                        data:[
                            {value:1023, name:'物业费总计'},
                            {value:899, name:'水电费总计'},
                            {value:1259, name:'停车费总计'},
                        ]
                    }
                ]
            };
    
            // 为echarts对象加载数据 
            bill_total_chart.setOption(bill_total_option); 
            bill_fee_chart.setOption(bill_fee_option); 
            bill_percent_chart.setOption(bill_percent_option); 
        }
    ); 
    $("#table-chart-select .choose-year li").click(function (){
        console.log('bill-chart-year:' + $(this).text());
        //ajax
    });
});