$(function (){
        // 用户统计表 
   var visitor_statistics = $("#visitor-total-statistics")[0];
        require.config({
            paths: {
                echarts: 'assets/echarts-2.2.2/build/dist'
            }
        });
        
    // 使用
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line',
        ],
        function (ec) {
            // 基于准备好的dom，初始化echarts图表
            var visitor_chart = ec.init(visitor_statistics, theme); 
            
            var visitor_option = {
                title: {
                    text: '访客统计'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data:['人数']
                },
                toolbox: {
                    show: true,
                    orient: 'horizontal',
                    x: 'right',
                    y: 'bottom',
                    color : ['#1e90ff','#22bb22','#4b0082','#d2691e'],
                    backgroundColor:'rgba(0,0,0,0)',
                    borderColor: '#ccc',
                    borderWidth: 0,            
                    padding: 5,                
                    showTitle: true,
                    feature : {
                        mark : {show: true},
                        // dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        data : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        "name":"人数",
                        "type":"bar",
                        "data": (function (){
                            var res = [];
                            var len = 12;
                            while (len--) {
                                res.push((Math.round(Math.random()*10) + 5));
                            }
                            return res;
                        })(),
                        "markPoint" : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                    },
                ]
            };
    
            // 为echarts对象加载数据 
            visitor_chart.setOption(visitor_option); 
        }
    );
    $("#table-chart-select .choose-year li").click(function (){
        console.log('visitor-chart-year:' + $(this).text());
        //ajax
    });
});

