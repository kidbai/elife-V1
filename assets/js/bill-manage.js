$(function (){
    //初始化账单管理表
    $("#tb-bill-manage").DataTable({
        'dom':'lrtip',
        sPaginationType: "full_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ],
    });

    //初始化缴费详情打印表
    $("#tb-bill-print").DataTable({
        dom: "ltp",
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "所有"]],
        order: [ 0, 'asc' ],
    });

    $(".dataTables_length").parent().addClass('col-xs-6');
    $(".dataTables_filter").parent().addClass('col-xs-6');
    $("#tb-bill-print_wrapper").removeClass('col-xs-6');

    //显示缴费详情
    $("#tb-bill-manage").delegate('button[rel="check-bill"]', 'click', function(event) {
        $("#bill-print").addClass('on');
        $(".shade").addClass('on'); 
    });
    $(".shade").click(function (){
        $("#bill-print").removeClass('on');
        $(this).removeClass('on');
    });

    $(window).keydown(function (ev){
        if(ev.keyCode === 27)
        {
            $("#bill-print").removeClass('on');
            $(".shade").removeClass('on');
        }
    });

    //打印
    $("#btn-print").click(function (){
        // var count = 0;
        // $.each($("#tb-bill-print_paginate .paginate_button"), function(){
        //     count++;
        // });
        // count = count - 2;
        // console.log(count);
        // for(var i = 0;i < count;i++)
        // {
        //     i += 1;
        //     console.log(i);
        //     $("#tb-bill-print_paginate .paginate_button:eq(" + i + ")").trigger('click');
        //     $("#print-template").append($("#tb-bill-print"));
            
        // }
        $("#tb-bill-print").jqprint();
    });
    $("#close-print").click(function (){
        $("#bill-print").removeClass('on');
        $(".shade").removeClass('on');
    });
    $(window).keydown(function (e){
        if(e.keyCode == '27')
        {
            $("#bill-print").removeClass('on');
            $(".shade").removeClass('on');
        }
    });
});