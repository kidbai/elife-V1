$(function(){
    var tb_visitor = $("#tb-visitor").DataTable({
        'dom':'lrtip',
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ],
    });

    // 初始化操作按钮
    var buttonGroup = {};
    buttonGroup['add'] = '添加';
    buttonGroup['search'] = '搜索';
    // 对应编辑界面url
    var editUrl = "home/resident_repair_edit";
    // $.buttonGroupInit(buttonGroup, 'tb-resident-repair', editUrl);
    $.buttonGroupInit(buttonGroup, 'tb-visitor', editUrl);   // 第一个参数如果是true，则会默认加载全部三个按钮

    // 初始化筛选框参数
    var searchArrayItem = {};
    searchArrayItem['roomId'] = '房号';
    searchArrayItem['visitorName'] = '访客名';
    searchArrayItem['visitorPhone'] = '访客手机号';

    // 初始化url地址、tableId
    var url = "home/visitor";
    var tableId = 'tb-visitor';
    $.toolbarInit(tableId, tb_visitor, searchArrayItem, false, url);
});