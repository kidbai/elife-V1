//富文本编辑器初始化
KindEditor.ready(function(K) {
  window.editor = K.create('#community-news-editor',{
    pasteType: 1
  });
});

$(function (){
  //编辑界面确认按钮 

  var modal_html = 
  '<div class="modal alert fade" tabindex="-1" role="dialog" aria-labelledby="guideModalLabel" aria-hidden="true">' +
    '<div class="modal-dialog">' +
      '<div class="modal-content">' +
        '<div class="modal-header">' +
          '<h5>请填写完整数据</h5>' +
        '</div> ' +
        '<div class="modal-footer">' +
          '<button class="btn btn-danger" data-dismiss="modal">确认</button>' +
        '</div>' +
      '</div>' +
    '</div>' +
  '</div>';

  $('body').append(modal_html);

  $(".form-group").delegate("#community-news-actions-btn", "click", function (){
    var community_news_array = [];
    if(!($('select[rel="community-news-type"]').val() === ''))
    { 
      community_news_array.push($('select[rel="community-news-type"]').val());
    }
    console.log(community_news_array);
    $.each($('.box-content input[rel="community-info"]'), function (){
      if($(this).val() === '')
      {
        $(".modal.alert").modal("show"); 
        return false; 
      }
      community_news_array.push($(this).val());
    });
    if($.trim(window.editor.html()) === '')
    {
      $(".modal.alert").modal("show");
      return false;
    }
    else
    {
      community_news_array.push(window.editor.html());
    }
    console.log(community_news_array);
    // $.ajax({
    //   url: '/path/to/file',
    //   type: 'default GET (Other values: POST)',
    //   dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
    //   data: {param1: 'value1'},
    // })
    // .done(function() {
    //   console.log("success");
    // })
    // .fail(function() {
    //   console.log("error");
    // })
    // .always(function() {
    //   console.log("complete");
    // });
    
  });

  $("#news-editor").focus(function (){
    $(this).removeClass('warning');
  });

  //取消颜色警告
  $(".box-content input").focus(function (){
      $(this).removeClass('alert-warning');
  });

});