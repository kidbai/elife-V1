$(function (){
    var tb_order_record = $("#tb-order-record").DataTable({
        'dom':'lrtip',   // l - length changing input control
                        // f - filtering input
                        // t - The table!
                        // i - Table information summary
                        // p - pagination control
                        // r - processing display element
        // "processing": true,                 //加载数据时显示正在加载信息
        // "serverSide": true,
        // "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        },
        "ajax":{
            "url":"",
            "type":"",
        }
    })   
});
