function clearGuide() {
    $("#guide-details .title h4").text('');
    $("#guide-details .guide-content").text('');
}

$(function (){
    var tb_guide = $("#tb-guide").DataTable({
        'dom':'lrtip',
        sPaginationType: "simple_numbers", 
        "language": {
            "url": "assets/js/dtCH.json"
        }, 
        order: [ 0, 'asc' ],
    });


    var html = 
    '<div id="guide-details" class="col-md-offset-1 col-md-10 col-xs-12">' +
      '<div class="guide-details-header">' +
        '<div class="title">' +
          '<i class="fa fa-times"></i>' +
          '<h4>标题</h4>' +
        '</div>' +
      '</div>' +
      '<div class="guide-content"> ' +
       '<p>1<br></p><p>外公死的时候，人们都哇哇大哭。</p>' +
      '</div>' +
    '</div>' +
    '<div class="shade"></div>';

    $('body').prepend(html);


    //查看办事指南详情
    $("#tb-guide").delegate('button[rel="guide-details"]', 'click', function(event) {
        console.log("hehe");
        $("#guide-details").addClass('on');
        $(".shade").addClass('on'); 
    });

    //关闭办事指南详情
    $(".shade").click(function(){
        $("#guide-details").removeClass('on');
        $(this).removeClass('on');
        clearGuide();
    });

    $("#guide-details .guide-details-header i").click(function (){
        $("#guide-details").removeClass('on');
        $(".shade").removeClass('on');
        clearGuide();
    });
    $(window).keydown(function (ev){
        if(ev.keyCode === 27)
        {
            $("#guide-details").removeClass('on');
            $(".shade").removeClass('on');
            clearGuide();
        }
    });
});