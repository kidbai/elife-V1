$.extend({
    //  要求: 删除按钮 需要有rel属性，需要和table的id一致
    //  Demo 
    //  <button class="btn btn-danger disabled" rel="tb-feedback">删除</button>
    //  <table id="tb-feedback" class="display responsive nowrap table table-striped"> 
    tableDelDisabled: function(tableId)
    {
        $("#" + tableId + " tbody").on('click', 'tr', function() {
            if($(this).hasClass('selected')) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                $('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            if($("#" + tableId + " tbody tr").hasClass('selected'))
            {
                $('button[rel="' + tableId + "\"" + ']').removeClass("disabled");
            }
            else 
            {
                $('button[rel="' + tableId + "\"" + ']').addClass("disabled");
            }
        }); 
    },

    // @parameter
    // delBtnRel: 删除按钮的rel属性;
    // url: ajax请求地址
    // table: DataTable对象
    // 
    //  require:  
    //  bootstrap-dialog.js
    //  bootstrap-dialog.min.css
    tableDataDel: function(delBtnRel, ajaxUrl, table) 
    {
        $('[rel="' + delBtnRel + '"]').click(function (){
            var delTh = $('table th:eq(1)').text();
            var delTd = $('table tr.selected td:eq(1)').text();
            console.log(delTd);
            BootstrapDialog.show({
                title: '系统提示',
                message: '确认删除<strong style="font-size: 14px; color:#e51c23">' + delTh + '</strong>是<strong style="font-size: 14px; color:#e51c23">' + delTd + '</strong>的数据?',
                type: 'type-danger',
                buttons: [{
                    label: '取消',
                    action: function(dialog){
                        dialog.close();
                    }
                }, {
                    label: '确认',
                    cssClass: 'btn-danger',
                    action: function(dialog){
                        // console.log("Del");
                        // console.log(table.rows('.selected'));
                        // console.log(table.rows('.selected').data()[0]); //被选择的数据(array)
                        // console.log(table.rows());
                        // console.log(table.rows('.selected').data()); //被选择的数据(array)
                        // table.row('.selected').remove().draw( false ); 
                        console.log(table.rows('.selected'));
                        dialog.close();
                        var data = table.rows('.selected').data();
                        var orderNoArray = new Array();
                        $.each(data, function() {
                            // console.log($(this)[0]['orderNo']);
                            orderNoArray.push($(this)[0]['orderNo']);
                            // console.log(orderNoArray);
                        });
                        $.ajax({
                            url: ajaxUrl,
                            type: 'POST',
                            dataType: 'json',
                            data: { orderNoArray : orderNoArray },
                            success: function (data) {
                                // console.log(data);
                                if(data)
                                {
                                    BootstrapDialog.show({
                                        title: '结果',
                                        type: 'type-success',
                                        message: '删除成功'
                                    });
                                    table.ajax.reload();
                                }
                                else
                                {
                                    BootstrapDialog.show({
                                        title: '结果',
                                        type: 'type-danger',
                                        message: '删除失败'
                                    });
                                }
                            }
                        });
                    }
                }]
            });
        });
    },

    // 初始化搜索工具框
    // tableId: table 的id 属性名称
    //table: datatables 的table对象
    //toolbarSearchArray: 搜索内容的类别
    //toolbarStatusArray: 搜索工具框的状态
    //url: 接口url地址
    toolbarInit: function(tableId, table, toolbarSearchArray, toolbarStatusArray, toolbarTypeArray, url)
    {
        // 筛选框模板
        var template = '<div class="toolbar-wrapper">' +
                            '<div class="toolbar-search col-md-8 col-xs-12">' +
                                '<div class="form-group toolbar-search-item">' +
                                    '<div class="col-md-2">' +
                                        '<select class="form-control">' +
                                            '{selectOption}' +
                                        '</select>' +
                                    '</div>' +
                                    '<div class="col-md-4">' +
                                        '<input class="form-control">' +
                                    '</div>' +
                                '</div>' +
                                // '<div class="form-group col-md-3">' +
                                //     '<button class="btn btn-success toolbar-search-btn-add">添加条件</button>' +
                                //     '<button class="btn btn-danger toolbar-search-btn-cancel">取消</button>' +
                                // '</div>' +
                            '</div>' +
                            '<div class="clear"></div>' +
                            '<div class="col-md-5 col-xs-12">' +
                                '{typeUl}' +
                                '<div class="clear"></div>' +
                                '<ul class="toolbar-status col-md-12">' +
                                    '<li class="header">状态:</li>' +
                                    '<li class="toolbar-status-item active" rel="all">全部</li>' +
                                    '{statusItem}' +
                                '</ul>' +
                            '</div>' +
                            '<div class="toolbar-date col-md-7 col-xs-12">' +
                                '<div class="toolbar-date-title col-md-2">起止日期:</div>' +
                                '<div class="form-group col-md-5">' +
                                    // '<label for="start-date" class="control-label">开始日期:</label>' +
                                    '<div class="input-group date" id="search-startDate" data-date-format="yyyy-MM-dd">' +
                                        '<input class="form-control" value="" size="" type="text" rel="startDate" readonly>' +
                                        '<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>' +
                                        '<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="form-group col-md-5">' +
                                    // '<label for="end-date" class="control-label">结束日期:</label>' +
                                    '<div class="input-group date" id="search-endDate" data-date-format="yyyy-MM-dd">' +
                                        '<input class="form-control" value="" size="" type="text" rel="endDate" readonly>' +
                                        '<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>' +
                                        '<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="clear"></div>' +
                            '<div class="col-md-1 padding-left-30">' + 
                                '<div class="form-group">' +
                                    '<button class="btn btn-primary margin-top-10 searchBtn">' +
                                        '搜索' +
                                    '</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>';

        // 搜索框模板
        var inputTemplate =     '<div class="form-group col-md-6 toolbar-search-item">' +
                                    '<div class="col-md-5">' +
                                        '<select class="form-control">' +
                                            '{selectOption}' +
                                        '</select>' +
                                    '</div>' +
                                    '<div class="col-md-6 col-md-offset-1">' +
                                        '<input class="form-control">' +
                                    '</div>' +
                                    '<i class="fa fa-times"></i>' +
                                '</div>';
                           

        //  toolbarSearchArray
        var selectOption = '';
        $.each(toolbarSearchArray, function(index, item) {
            var tpl = '<option value="{index}">{item}</option>';
            tpl = tpl.replace(/{index}/g, index).replace(/{item}/g, item);
            selectOption += tpl;
        });

        // toolbarStatusArray
        var statusItem = '';
        $.each(toolbarStatusArray, function(index, item) {
            var tpl = '<li class="toolbar-status-item" rel="{index}">{item}</li>';
            tpl = tpl.replace(/{index}/g, index).replace(/{item}/g, item);
            statusItem += tpl;
        });

        // toolbarTypeArray
        var typeUlTmp = '';
        var typeItem = '';
        console.log(toolbarTypeArray != '');
        if(toolbarTypeArray != '')
        {
            typeUlTmp = '<ul class="toolbar-type col-md-12">' +
                                '<li class="header">类型:</li>' +
                                '<li class="toolbar-type-item active" rel="all">全部</li>' +
                                '{typeItem}' +
                            '</ul>';
            
            $.each(toolbarTypeArray, function(index, item) {
                var tpl = '<li class="toolbar-type-item" rel="{index}">{item}</li>';
                tpl = tpl.replace(/{index}/g, index).replace(/{item}/g, item);
                typeItem += tpl;
            });
            typeUlTmp = typeUlTmp.replace(/{typeItem}/g, typeItem);
        }

        template = template.replace(/{selectOption}/g, selectOption).replace(/{typeUl}/g, typeUlTmp).replace(/{statusItem}/g, statusItem);
        
        // 插入dom中
        $("#" + tableId).before(template);


        // 搜索控件按钮
        // $(".toolbar-search-item input").focus(function (){
        //     $(".toolbar-search button").addClass('on');
        // });

        $(".toolbar-search-btn-cancel").click(function (){
            $(".toolbar-search button").removeClass('on');
            $(".toolbar-search-item:not(:first)").remove();
        });

        $(".toolbar-search-btn-add").click(function (){
            var selectAddInput = '';
            $.each(toolbarSearchArray, function(index, item) {
                // console.log(index); 
                // console.log(item);
                var tpl = '<option value="{index}">{item}</option>';
                tpl = tpl.replace(/{index}/g, index).replace(/{item}/g, item);
                selectAddInput += tpl;
            });
            inputTemplate = inputTemplate.replace(/{selectOption}/g, selectAddInput);
            // console.log(inputTemplate);
            $(".toolbar-search-item:last").after(inputTemplate);
            $(".toolbar-search-item").on("click", "i.fa", function (){
                $(this).parent().remove();
            });
        });

        //  日期控件初始化
        var startDate = $('#search-startDate').datetimepicker({
            language:  'zh-CN',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            showMeridian: 1,
            // endDate: true,
        }).on("changeDate", function(ev){
            // console.log(ev.date != null);
            if(ev.date != null)
            {
                startDate=new Date(ev.date.getFullYear(),ev.date.getMonth(),ev.date.getDate(),0,0,0);
                if(endDate!=null&&endDate!='undefined'){
                    if(endDate<startDate){
                        BootstrapDialog.show({
                            title: "错误!",
                            message: "结束日期小于开始日期",
                            type: "type-danger",
                            buttons: [{
                                label: '关闭',
                                cssClass: 'btn-default',
                                action: function(dialog) {
                                    $('input[rel="startDate"]').val('');
                                    dialog.close();
                                }
                            }]
                        });
                    }
                }
            }
            
        });
        var endDate = $('#search-endDate').datetimepicker({
            language:  'zh-CN',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            showMeridian: 1
        }).on("changeDate", function(ev){
            if(ev.date != null)
            {
                endDate=new Date(ev.date.getFullYear(),ev.date.getMonth(),ev.date.getDate(),0,0,0);
                if(startDate!=null&&startDate!='undefined'){
                    if(endDate<startDate){
                        BootstrapDialog.show({
                            title: "错误!",
                            message: "结束日期小于开始日期",
                            type: "type-danger",
                            buttons: [{
                                label: '关闭',
                                cssClass: 'btn-default',
                                action: function(dialog) {
                                    $('input[rel="endDate"]').val('');
                                    dialog.close();
                                }
                            }]
                        });
                    }
                }
            }
            
        });

        // 设置结束日期
        var moment = new Date();
        var today = moment.getFullYear() + '-' + (moment.getMonth()+1) + '-' + moment.getDate();
        startDate.datetimepicker('setEndDate', today);
        endDate.datetimepicker('setEndDate', today);

        $(".toolbar-status-item").click(function (){
            var status = $(this).attr('rel');
            console.log(status);
            $(".toolbar-status-item").removeClass('active');
            $(this).addClass('active');
        });

        $(".toolbar-type-item").click(function (){
            var type = $(this).attr('rel');
            console.log(type);
            $(".toolbar-type-item").removeClass('active');
            $(this).addClass('active');
        });

        $(".searchBtn").click(function (){
            var ajaxReload = true;
            var dateFirst = $(".toolbar-date input:eq(0)").val();
            var dateSecond = $(".toolbar-date input:eq(1)").val();
            if( dateFirst === '' && dateSecond === '')
            {
                table.ajax.reload();
            }
            else if( dateFirst === '' || dateSecond === '' )
            {   
                BootstrapDialog.show({
                    title: "错误!",
                    message: "请查看日期是否被完整选取",
                    type: "type-danger",
                    buttons: [{
                        label: '关闭',
                        cssClass: 'btn-default',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false
            }
            else 
            {
                if( dateFirst != '' && dateSecond != '' )
                {
                    table.ajax.reload();
                }
            }
            //获取dt请求参数
            var args = table.ajax.params();
        });
    },

    // 添加、删除、搜索按钮
    // buttonObj: 如果值为true，会默认加载三个按钮,如果是个对象，则会根据对象里的内容加载
    // buttonObj中如果没有添加按钮， editUrl设置为任何值都可以，但不能为空
    buttonGroupInit: function(buttonObj, tableId, editUrl)
    {
        if(buttonObj === true)
        {
            var tpl =   '<button class="btn btn-primary" rel="tb-search-toolbar">搜索</button>'+
                        '<button class="btn btn-danger disabled" rel="' + tableId + '">删除</button>'+
                        '<a href="'+ editUrl +'" class="btn btn-success" rel="resident-repair-add">添加</a>';
            var buttonGroup = '<div class="buttonGroup"></div>';
            $(".box-content").prepend(buttonGroup);
            $(".buttonGroup").prepend(tpl);
        }
        else
        {
            var buttonGroup = '<div class="buttonGroup"></div>';
            $(".box-content").prepend(buttonGroup);
            var addBtnTpl = '<a href="'+ editUrl +'" class="btn btn-success" rel="resident-repair-add">添加</a>';
            var delBtnTpl = '<button class="btn btn-danger disabled" rel="' + tableId + '">删除</button>';
            var searchBtnTpl = '<button class="btn btn-primary" rel="tb-search-toolbar">搜索</button>';

            $.each(buttonObj, function(index, el) {
                if( index == 'add')
                {
                    $(".buttonGroup").prepend(addBtnTpl);
                }
                else if( index == 'del' )
                {
                    $(".buttonGroup").prepend(delBtnTpl);
                }
                else
                {
                    $(".buttonGroup").prepend(searchBtnTpl);
                }
            });
        }

        $('button[rel="tb-search-toolbar"]').click(function (){
          $('.toolbar-wrapper').toggle();
          if( $('.toolbar-wrapper').is(':hidden'))
          {
            $(this).text("搜索")
          }
          else
          {
            $(this).text("返回")
          }
        });
    },

    // 获取toolbar里的参数值
    getToolBarValue: function ()
    {
        var data = {};

        // 搜索框
        var itemKey = $(".toolbar-search-item").find('select').val();
        var itemValue = $(".toolbar-search-item").find('input').val();
        if(itemValue != '')
        {
            data[itemKey] = itemValue;
            console.log(data);
        }
        
        // // 日期
        // var date = new Array();
        // $.each($(".input-group.date"), function () {
        //   var item = $(this).children('input').val();
        //   if( item != '')
        //   {
        //     date.push(item);
        //     data.push(date); 
        //   }
        //   else
        //   {
        //     return;
        //   }
        // });

        // 状态
        if($(".toolbar-status-item.active").index() != 1)
        {
            data['status'] = $(".toolbar-status-item.active").attr('rel');
        }
        if($(".toolbar-type-item.active").index() != 1)
        {
            data['type'] = $(".toolbar-type-item.active").attr('rel');
        }
        return data;
    },

    //  初始化行号
    initTableRowNumber: function ( table )
    {
        table.column(0).nodes().each( function (cell, index) {
            $cell = $(cell);
            index += table.page.info()['start'] + 1;
            $cell.text(index);
        });
    }
});
