$(function (){
    var tb_feedback = $("#tb-feedback").DataTable({
        'dom':'lrtip',
        // "processing": true,                 //加载数据时显示正在加载信息
        // "serverSide": true,
        "iDisplaylength":10,
        "sPaginationType" : "full_numbers",
        "language": {
            "url": "assets/js/dtCH.json"
        }
    });

    var searchArrayItem = {};
    searchArrayItem['title'] = '标题';

    var statusArray = {};
    statusArray['invalid'] = ('无效');
    statusArray['done'] = ('已完成');

    var url = "/home/feedback";
    var tableId = 'tb-feedback';

    var buttonGroup = {};
    buttonGroup['del'] = '删除';
    buttonGroup['search'] = '搜索';

    $.buttonGroupInit(buttonGroup, 'tb-feedback', true);   // 第一个参数如果是true，则会默认加载全部三个按钮

    $.tableDelDisabled('tb-feedback');
    $.tableDataDel('tb-feedback', '/home/feedback', tb_feedback);
    $.toolbarInit(tableId, tb_feedback, searchArrayItem, statusArray, url);
});