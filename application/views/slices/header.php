<div class="navbar-inner">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="javascript:void(0)" class="navbar-brand title"><img class="logo hidden-xs" src="../assets/img/logo.png" width="20" height="20">华润物业</a>
        <a href="javascript:void(0)" class="navbar-brand title"><img class="logo hidden-xs" src="../assets/img/logo.png" width="20" height="20">
            <?php 
                if(!isset($userInfo['commdesc'])){
                    $commdesc = "";
                }else{
                    $commdesc = $userInfo['commdesc'];
                }
            ?>
            <? echo $commdesc ?>
        </a>
    </div>
    <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="user nav navbar-nav navbar-right">
            <li>
                <a href="javascript:void(0)">
                    <?php 
                        if(!isset($userInfo['commdesc'])){
                            $commdesc = "";
                        }else{
                            $commdesc = $userInfo['commdesc'];
                        }
                    ?>
                </a>
            </li>
            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <i id="bellLabel" class="fa fa-bell cursor"></i>
                    <span class="badge">4</span> <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="home/bill_manage"><i class="fa fa-money"></i>3条新账单记录<span class="float-right"><em>1分钟前</em></span></a></li>
                <li><a href="home/"><i class="fa fa-wrench"></i>1条新报修记录<span class="float-right"><em>2分钟前</em></span></a></li>
              </ul>
            </li>
            <li>
                <a href="#" id="userLabel" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <img src="assets/img/avatar.png" class="img-circle" alt="">
                    <span class="userName">顾小松</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="home/staff_info_edit"><i class="fa fa-gear"></i>我的信息</a></li>
                    <li><a href="home/staff_info_edit"><i class="fa fa-key"></i>修改密码</a></li>
                    <li class="divider"></li>
                    <li><a id="logout" href="home/logout"><i class="fa fa-times"></i>退出</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)">
                    <div id="clock"><i class="fa fa-clock-o"></i><i class="time"></i></div> 
                </a>
            </li>
        </ul>
    </div>
</div>