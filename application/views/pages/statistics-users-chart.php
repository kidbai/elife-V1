<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="row position-relative z-index-1">
    <div id="table-chart-select">
      <div class="col-md-2 col-xs-4 select-item">
        <select class="form-control">
          <option value="2015">2015年</option>
          <option value="2014">2014年</option>
        </select>
      </div>
      <div class="col-md-offset-8 col-md-1 col-xs-offset-3 col-xs-2 select-item">
        <a href="/home/statistics_users_table" class="fa fa-align-justify"><div class="table-target"></div></a>
      </div>
       <div class="col-md-1 col-xs-1 select-item">
        <a href="/home/statistics_users_chart" class="fa fa-bar-chart"><div class="chart-target"></div></a>
      </div>
    </div>
  </div>
  <div class="chart-box">
    <div id="users-statistics" class="col-md-12 col-xs-12 tab"></div>
  </div>
  <div class="chart-box">
    <div id="users-percent-statistics" class="col-md-12 col-xs-12 tab"></div>
  </div>
</div>