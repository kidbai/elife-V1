<div class="col-md-12 col-xs-12">
    <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
    </div>
    <div class="row">
      <div id="table-chart-select">
        <div class="col-md-offset-10 col-md-1 col-xs-offset-8 col-xs-2">
          <a href="/home/statistics_bill_table" class="fa fa-align-justify"><div class="table-target"></div></a>
        </div>
        <div class="col-md-1 col-xs-1">
          <a href="/home/statistics_bill_chart" class="fa fa-bar-chart"><div class="chart-target"></div></a>
        </div>
      </div>
    </div>
    
    <div class="box">
      <div class="box-inner">
        <div class="box-header">
            <h5><i class="fa fa-money"></i>账单统计</h5>
        </div>
        <div class="box-content">
            <select name="statistics-bill-years" id="statistics-bill-years" class="form-control">
                <option value="2015">2015年</option>
                <option value="2014">2014年</option>
                <option value="2013">2013年</option>
            </select>
            <div class="clear-20"></div>
          <table id="tb-statistics-bill" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>序号</th>
                <th>类型\日期</th>
                <th>一月份</th>
                <th>二月份</th>
                <th>三月份</th>
                <th>四月份</th>
                <th>五月份</th>
                <th>六月份</th>
                <th>七月份</th>
                <th>八月份</th>
                <th>九月份</th>
                <th>十月份</th>
                <th>十一月份</th>
                <th>十二月份</th>
                <th>年份</th>
                <th>总计</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>物业费</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>2015</td>
                <td>1200</td>
              </tr>
              <tr>
                <td>2</td>
                <td>水电费</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>2014</td>
                <td>1200</td>
              </tr>
              <tr>
                <td>3</td>
                <td>停车费</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>2013</td>
                <td>1200</td>
              </tr>
              <tr>
                <td>4</td>
                <td>停车费</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
                <td>2014</td>
                <td>1200</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div> 
    </div>
  </div>