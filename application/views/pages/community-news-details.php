<div class="col-md-12 col-xs-12">
    <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
       <i class="fa fa-arrow-left"></i> 
    </div>
    <div class="box">
      <div class="box-inner">
        <div class="box-header">
          <h5><i class="fa fa-gear"></i>小区动态详情</h5>
          
        </div>
        <div id="community-news-details" class="box-content">
          <div class="news-header">
            <div class="form-group">
              <label for="" class="label-control">类型: <label for="" class="label label-default">公告</label></label>
            </div>
            <div class="title">临时停水通知</div>
            <div class="info col-md-offset-6 col-xs-offset-5">
              <div class="author">天府豪庭 <em>2015-10-10 8:00</em></div>
            </div>
          </div> 
          <div class="news-content">
            <p></p><p>虽然，这篇文章的标题里说的是我们想办一场“盛宴”，但这并不是一场以吃为主要目的的活动（吃货们对不起了2333）——这场活动最核心的目标，其实是“集齐十个互联网餐饮公司，一起讨论如何改变人类的吃饭方式”。</p><p>好了，上面是开玩笑的，但认真严肃地说，过去这一两年，我们的吃饭方式真的被互联网改变了，我们不再依靠街边散发的小宣传单来发现周遭的外卖商家，一个手机app就能解决这一切，还不用到处找单子、担心一家新餐厅味道信不过，一个人/多个人在家吃饭的场景，变得从未有过的便捷起来。 </p><p>而除了外卖，我们的堂食也正在被改造，排队软件的火爆让人们可以在未到店时就估算好到店的时间，避免排队对时间的耗损，同时又能提高商家利润。而除了堂食，又还有觅食、下厨房、妈妈的菜、我有饭等等这样的公司，希望用互联网的方式把手艺人的余力挖掘、共享出来，给天下吃货生产更多美食。更不用说青年菜君或是爱大厨、好厨师这类的公司，直接把原材料或者厨师直接送到家门，让用户很短时间内可以吃到他所想吃的菜品。</p><p>所以，如果十年后回头来看，我们“吃”的变革一定在这两年表现得尤为猛烈，而正是这批公司，在为我们开拓更多的吃饭场景、优化原有餐饮的效率、甚至开创一种新的供给方式。因此我们觉得有必要，把这些公司聚在一块，聊一聊他们都是怎么来改变我们的吃饭方式的？在这过程中，会面临什么样的挑战？</p><p>如果你是这个行业的早期创业者，或者是正蠢蠢欲动打算进入这个行业的从业者，不要犹豫，<strong><a href="http://www.mikecrm.com/f.php?t=Ka2G4i" target="_blank" class="external">来报名</a>36氪和小饭桌创业课堂一起举办的这场线下活动吧</strong>，听听先发创业者们的经验与思考。</p>
            <img src="../public/images/background.jpg" alt="" width="800" height="500">
          </div>
        </div>
      </div>
    </div>
</div>