<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>职员评论</h5>
      </div>
      <div class="box-content">
        <table id="tb-staff-comment" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>姓名</th>
              <th>本月点赞数</th>
              <th>总点赞数</th>
              <th>查看评论</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>4</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>5</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>6</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>7</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>8</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>9</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
            <tr>
              <td>10</td>
              <td>杨柏</td>
              <td>158</td>
              <td>168</td>
              <td>
                <a href="/home/staff_comment_details" class="btn btn-info">查看</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>