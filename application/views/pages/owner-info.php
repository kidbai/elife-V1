<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>业主信息</h5>
      </div>
      <div class="box-content">
        <div class="clear-20"></div>
        <table id="tb-owner-info" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>业主名称</th>
              <th>房号</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>张三</td>
              <td>1-1-1</td>
            </tr>
            <tr>
              <td>2</td>
              <td>张x</td>
              <td>1-1-1</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>