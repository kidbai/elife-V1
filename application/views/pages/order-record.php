<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>订单记录</h5>
      </div>
      <div class="box-content">
        <div class="clear-20"></div>
        <table id="tb-order-record" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>订单号</th>
              <th>时间</th>
              <th>订单状态</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>1</td>
              <td>1</td>
              <td>done</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>