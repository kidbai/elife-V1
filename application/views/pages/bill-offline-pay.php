<div class="col-md-12 col-xs-12">
    <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
       <i class="fa fa-arrow-left"></i> 
    </div>
    <div class="box">
      <div class="box-inner">
        <div class="box-header">
          <h5><i class="fa fa-gear"></i>线下缴费</h5>
        </div>
        <div class="box-content">
          <div class="box-block overflow-hidden">
            <div class="title-block">
              <h3>缴费信息</h5>
            </div>
            <div class="content-block col-md-6 col-xs-12">
              <div class="row">
                <div class="form-group col-md-10">
                  <label for="" class="control-label">业主房号</label>
                  <input type="text" class="form-control" value="1-2-2903" readonly>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-10">
                  <label for="" class="control-label">截止时间</label>
                  <input type="date" class="form-control" value="2015-01-01" readonly>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-10">
                  <label for="" class="control-label">上次缴费时间</label>
                  <input type="date" class="form-control" value="2015-11-02" readonly>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-10">
                  <label for="" class="control-label">需缴费用(历史欠费)</label>
                  <input type="text" class="form-control" value="1200(200)" readonly>
                </div>
              </div>
            </div>
            <div class="content-block col-md-5 col-xs-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-12">
                      <input type="checkbox" rel="offline-bill-type"> 物业费
                    </label>
                    <label class="offline-bill-month col-md-6" rel="offline-months">缴费月数:
                      <select name="" id="" rel="offline-months-select" class="form-control" disabled="disabled">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                      </select>     
                    </label>
                    <label class="offline-bill-total col-md-6" rel="offline-total" for="property">总额: <input type="text" class="form-control" readonly></label>
                    <input type="text" rel="bill-rule" value="2-103">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-12">
                      <input type="checkbox" rel="offline-bill-type"> 停车费
                    </label>
                    <label class="offline-bill-month col-md-6" rel="offline-months">缴费月数:
                      <select name="" id="" rel="offline-months-select" class="form-control" disabled="disabled">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                      </select>     
                    </label>
                    <label class="offline-bill-total col-md-6" rel="offline-total" for="parking">总额: <input type="text" class="form-control" readonly></label>
                    <input type="text" rel="bill-rule" value="2-103">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-12">
                      <input type="checkbox" rel="offline-bill-type"> 水电费
                    </label>
                    <label class="offline-bill-month col-md-6" rel="offline-months">缴费月数:
                      <select name="" id="" rel="offline-months-select" class="form-control" disabled="disabled">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                      </select>     
                    </label>
                    <label class="offline-bill-total col-md-6" rel="offline-total" for="water-electricity">总额: <input type="text" class="form-control" readonly></label>
                    <input type="text" rel="bill-rule" value="2-103">
                  </div>
                </div>
              </div>
            </div>
            <div class="foot-block col-md-12">
              <div class="row">
                <div class="form-group text-center">
                  <button id="offline-bill-pay" class="btn btn-primary" data-toggle="modal" data-target="#offline-bill-makesure">缴费</button>
                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
</div>
