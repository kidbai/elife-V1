<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
    <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>小区动态信息编辑</h5>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group col-md-5">
              <label for="" class="label-control">类型</label>
              <select class="form-control" rel="community-news-type" name="" id="">
                <option value="important">重要</option>
                <option value="activity">活动</option>
                <option value="greeting">问候</option>
                <option value="notice">公告</option>
              </select>

            </div>
            <div class="clear"></div>
            <div class="form-group col-md-5">
              <label for="" class="label-control">标题</label>
              <input rel="community-info" type="text" class="form-control" placeholder="标题长度不要超过**个字">
            </div>
            <div class="clear"></div>
            <div class="form-group col-md-5">
              <label for="" class="label-control">时间</label>
              <input rel="community-info" type="datetime-local" class="form-control">
            </div>
            <div class="clear"></div>
            <div class="col-md-12">
              <label for="" class="label-control">内容</label>
              <textarea name="content" id="community-news-editor" style="width:900px;height:300px;"></textarea>
            </div>
            <div class="clear-10"></div>
            <div class="form-group col-md-5">
              <button class="btn btn-primary" id="community-news-actions-btn">确定</button> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>