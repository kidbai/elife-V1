<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>数据匹配编辑</h5>
      </div>
      <div class="box-content">
        <form action="" class="overflow-hidden">
          <div class="form-group col-md-5">
            <label class="control-label">房号</label>
            <input name="bill-name" type="text" class="form-control" required="required" placeholder="格式:1-2-3">
          </div> 
          <div class="clear"></div>
          <div class="form-group col-md-5">
            <label class="control-label">业主名</label>
            <input name="bill-rule" type="text" class="form-control" onkeyup="value=value.replace(/[^\u4E00-\u9FA5]/g,'')" required="required">
          </div>
          <div class="clear"></div>
          <div class="form-group col-md-5">
            <label class="control-label">联系方式</label>
            <input name="bill-rule" type="text" class="form-control" maxlength="11" placeholder="手机号码" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" required="required">
          </div>
          <div class="clear"></div>
          <div class="form-group col-md-5">
            <input type="submit" class="btn btn-primary" id="bill-rule-edit-btn"></button>
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>