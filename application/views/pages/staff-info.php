<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>职员管理</h5>
       
      </div>
      <div class="box-content">
        <a href="home/staff_info_edit" class="btn btn-success" rel="staff-add">添加</a> 
        <button class="btn btn-danger" data-toggle="modal" data-target="#staff-del">删除</button>
        <div class="clear-20"></div>
        <table id="tb-staff" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>职员名称</th>
              <th>职员工号</th>
              <th>电话</th>
              <th>所属部门</th>
              <th>入职时间</th>
              <th>状态</th>
              <th>照片</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              foreach($result as $item){
                  switch ($item->status) {
                    case '0':
                      $statusDesc = "离线";
                      break;
                    case '1':
                      $statusDesc = "在线";
                      break;
                    case '9':
                      $statusDesc = "禁用";
                    default:
                      $statusDesc = "未知";
                      break;
                  }
                  echo "<tr>
                        <td>$item->sid</td>
                        <td>$item->name</td> 
                        <td>$item->code</td>
                        <td>$item->mobile</td>
                        <td>$item->department</td>
                        <td>$item->add_time</td>
                        <td>$statusDesc</td>
                        <td><img src='".$item->head_thumb."' width='30' height='30' ></td>
                        <td>
                          <a href='home/staff_info_edit/$item->comm_id/$item->code' class='btn btn-info' rel='staff-edit'>编辑</a>
                        </td>
                        </tr>";
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>