<div class="col-md-12 col-xs-12">
    <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
      <i class="fa fa-arrow-left"></i> 
    </div>
    <div class="box">
      <div class="box-inner">
        <div class="box-header">
          <h5><i class="fa fa-wrench"></i>用户管理</h5>
        </div>
        <div class="box-content">
           <form action="" class="overflow-hidden">
            <div class="form-group col-md-5">
              <label for="username" class="label-control">业主名称</label>
              <input name="username" type="text" class="form-control" required="required">
            </div>
            <div class="clear"></div>
            <div class="form-group col-md-5">
              <label for="user-phone" class="label-control">联系方式</label>
              <input name="user-phone" type="text" class="form-control" maxlength="11" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" placeholder="手机号码" required="required">
            </div>
            <div class="clear"></div>
            <div class="form-group col-md-5">
              <label for="address" class="label-control">住址</label>
              <input name="address" type="text" class="form-control" required="required">
            </div>
            <div class="clear"></div>
           <!--  <div class="form-group col-md-5">
              <label for="register-date" class="label-control">入户时间</label>
              <input name="register-date" type="date" class="form-control" required="required">
            </div>
            <div class="clear"></div> -->
            <div class="form-group col-md-5">
              <label class="control-label">入户时间</label>
              <div class="input-group date" id="register-time" data-date-format="yyyy-MM-dd hh:ii" data-link-field="dtp_input1">
                  <input class="form-control" name="register-date" value="" size="" type="text" required="required" readonly>
                  <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
              </div>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
            <div class="form-group col-md-5">
              <input type="submit" class="btn btn-primary" id="user-edit-btn"></input> 
            </div>
          </form> 
        </div>
      </div>
    </div>
</div>