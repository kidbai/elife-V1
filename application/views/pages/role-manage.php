<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>角色管理</h5>
      </div>
      <div class="box-content">
        <a href="/stencil-master/index.php/home/role_manage_edit" class="btn btn-success tb-data-match-item">添加</a>
        <button class="btn btn-danger tb-data-match-item" data-toggle="modal" data-target="
        #data-match-del">删除</button>
        <div class="clear-20"></div>
        <table id="tb-role" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>角色类型</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>财务</td>
              <td>
                <a href="/home/role_manage_edit" class="btn btn-info" rel="role-manage-edit">编辑</a>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>管理</td>
              <td>
                <a href="/home/role_manage_edit" class="btn btn-info" rel="role-manage-edit">编辑</a>
              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>人事资源</td>
              <td>
                <a href="/home/role_manage_edit" class="btn btn-info" rel="role-manage-edit">编辑</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>