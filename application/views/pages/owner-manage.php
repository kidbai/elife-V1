<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
    <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>用户管理</h5>
      </div>
      <div class="box-content">
        <div class="clear-20"></div>
          <table id="tb-owner-manage" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>序号</th>
                <th>名称</th>
                <th>联系方式</th>
                <th>住址</th>
                <th>入户时间</th>
                <th>操作</th>
              </tr>
            </thead> 
            <tbody>
              <tr>
                <td>1</td>
                <td>张三</td>
                <td>13800000</td>
                <td>1-2-2903</td>
                <td>2015-01-01</td>
                <td>
                  <a href="/home/owner_manage_edit" class="btn btn-info" rel="owner-edit">编辑</a>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>222222</td>
                <td>3332222</td>
                <td>332222</td>
                <td>2014-04-05</td>
                <td>
                  <a href="/home/owner_manage_edit" class="btn btn-info" rel="owner-edit">编辑</a>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>11222222 </td>
                <td>11222222222222 </td>
                <td>112222222222 </td>
                <td>2014-05-05</td>
                <td>
                  <a href="/home/owner_manage_edit" class="btn btn-info" rel="owner-edit">编辑</a>
                </td>
              </tr>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
