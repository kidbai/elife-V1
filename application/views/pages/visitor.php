<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>访客通行</h5>
      </div>
      <div class="box-content">
        <div class="clear-20"></div>
        <table id="tb-visitor" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>用户编号</th>
              <th>房号</th>
              <th>用户名称</th>
              <th>访问时间</th>
              <th>提交时间</th>
              <th>访客名称</th>
              <th>访客电话</th>
              <th>状态</th>
              <th>备注</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>cd123123123123</td>
              <td>1-1-1</td>
              <td>张三</td>
              <td>2015-1-2 08：30</td>
              <td>2015-1-1 08：30</td>
              <td>李四</td>
              <td>3813818313</td>
              <td><label class="label label-info">已提交</label></td>
              <td>他带有其他三个朋友，请放行</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>