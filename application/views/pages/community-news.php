<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
    <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>小区动态</h5>
      </div>
      <div class="box-content">
        <a href="/home/community_news_actions" class="btn btn-success" rel="news-add">添加</a> 
        <button class="btn btn-danger" data-toggle="modal" data-target="#news-del">删除</button>
        <div class="clear-20"></div>
            <table id="tb-news" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>序号</th>
                  <th>类型</th>
                  <th>标题</th>
                  <th>时间</th>
                  <th>图片</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td><label class="label label-danger">重要</label></td>
                  <td>关于凤凰城2期的停电公告</td>
                  <td>08:30</td>
                  <td>有</td>
                  <td>
                    <a href="/home/community_news_details" class="btn btn-primary" rel="news-details">查看</i></a>
                    <a href="/home/community_news_actions" class="btn btn-info" rel="news-actions">编辑</a>
                    <button class="btn btn-default" rel="top">置顶</button>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td><label class="label label-success">活动</label></td>
                  <td>万圣节</td>
                  <td>08:30</td>
                  <td>无</td>
                  <td>
                    <a href="/home/community_news_details" class="btn btn-primary" rel="news-details">查看</i></a>
                    <a href="/home/community_news_actions" class="btn btn-info" rel="news-actions">编辑</a>
                    <button class="btn btn-default" rel="top">置顶</button>
                </tr>
                <tr>
                  <td>3</td>
                  <td><label class="label label-primary">问候</label></td>
                  <td>天气降温</td>
                  <td>08:30</td>
                  <td>无</td>
                  <td>
                    <a href="/home/community_news_details" class="btn btn-primary" rel="news-details">查看</i></a>
                    <a href="/home/community_news_actions" class="btn btn-info" rel="news-actions">编辑</a>
                    <button class="btn btn-default" rel="top">置顶</button>
                </tr>
                <tr>
                  <td>4</td>
                  <td><label class="label label-default">公告</label></td>
                  <td>关于凤凰城2期的停电公告</td>
                  <td>08:30</td>
                  <td>有</td>
                  <td>
                    <a href="/home/community_news_details" class="btn btn-primary" rel="news-details">查看</i></a>
                    <a href="/home/community_news_actions" class="btn btn-info" rel="news-actions">编辑</a>
                    <button class="btn btn-default" rel="top">置顶</button>
                  </td>
                </tr>
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>