<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>业主报修</h5>
      </div>
      <div class="box-content">
        <div class="box-block">
          <div class="title-block">
            <h3>详细信息</h3>
          </div>
          <div class="content-block">
            <form id="repairForm" class="overflow-hidden">
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group col-md-12">
                  <label class="control-label">报修单</label>
                  <input name="orderNo" type="text" class="form-control" value="<?=$result['orderNo']?>" for="resident-repair" readonly="readonly" required="required">
                </div> 
                <div class="form-group col-md-12">
                  <label class="control-label">报修内容</label>
                  <textarea name="orderDesc" for="resident-repair" style="text-align:left;" cols="120" rows="3"  class="form-control" required="required"><?=$result['orderDesc']?></textarea> 
                  <div id="links" class="repairGallery">
                    <a href="../../assets/img/background.jpg" title=''>
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                    <a href="../../assets/img/background.jpg">
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                    <a href="../../assets/img/background.jpg" >
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                    <a href="../../assets/img/background.jpg" >
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                    <a href="../../assets/img/background.jpg" >
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                    <a href="../../assets/img/background.jpg" >
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                    <a href="../../assets/img/background.jpg" >
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                    <a href="../../assets/img/background.jpg" >
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                    <a href="../../assets/img/background.jpg" >
                        <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
                    </a>
                  </div> 
                </div>
                <div class="form-group col-md-12">
                  <label class="control-label">预约时间</label>
                  <div class="input-group date col-md-12" >
                      <input class="form-control" name="forwardTime" value="<?=$result['forwardTime']?>" size="" type="text" value="" readonly>
                      <input type="hidden" for="resident-repair" value="<?=$result['forwardTime']?>">
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <div class="btn btn-primary" id="change-appointment-time">
                    修改预约时间
                  </div>
                  <div class="btn btn-info" id="reset-appointment-time">
                    恢复原始时间
                  </div>
                  <div class="btn btn-success change-appointment-time" id="change-appointment-time-submit">
                    确认
                  </div>
                </div>
                <div class="form-group col-md-6 change-appointment-time">
                    <label for="dtp_input1" class="control-label">日期</label>
                    <div class="input-group date" id="appointment-startTime" data-date-format="yyyy-MM-dd">
                        <input class="form-control" value="" size="" type="text" readonly>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                    </div>
                </div>
                <div class="form-group col-md-6 change-appointment-time">
                    <label for="dtp_input1" class="control-label">时间段</label>
                    <select class="form-control">
                      <option value="morning">上午 9:00-12:00</option>
                      <option value="afternoon">下午 14:00-18:00</option>
                      <option value="night">上午 19:00-22:00</option>
                    </select>
                </div>
                <div class="form-group col-md-12">
                  <label class="control-label">报修状态</label>
                    <?php 
                      switch ($result['status']) {
                        case '0':
                          $statusDesc = '待处理';
                          break;
                        case '1':
                          $statusDesc = '处理中';
                          break;
                        case '2':
                          $statusDesc = '已完成(待评价)';
                          break;
                        case '3':
                          $statusDesc = '已完成(已评价)';
                          break;
                        case '4':
                          $statusDesc = '无效数据';
                          break;
                        case '5':
                          $statusDesc = '用户撤销';
                          break;
                        default:
                          $statusDesc = '缺省数据';
                          break;
                      }
                    ?>
                  <input name="statusDesc" type="text" class="form-control" readonly="readonly" value="<?=$statusDesc?>" required="required">
                  <input name="status" for="resident-repair" type="hidden" value="<?=$result['status']?>" /> 
                </div>
              </div>
              <!-- end -->
              
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group col-md-12">
                  <label class="control-label">物业回复</label>
                   <textarea name="propReply" placeholder="字数不能多于150！" maxlength="150" for="resident-repair" style="text-align:left;padding:0" cols="120" rows="3"  class="form-control" required="required"><?=$result['propReply']?></textarea> 
                </div>
                <div class="clear"></div>
                <div class="form-group col-md-12">
                  <label class="control-label">报修人</label>
                  <input name="repairOwner" class="form-control" readonly>
                </div>
                <div class="form-group col-md-12">
                  <label class="control-label">报修人联系方式</label>
                  <input name="repairOwnerPhone" class="form-control" readonly>
                </div>
                <div class="clear"></div>
                <div class="form-group col-md-12">
                  <label class="control-label">维修人</label>
                  <select name="staffCode" id="repair-staff" class="form-control" for="resident-repair">
                    <?php
                         foreach($staff as $item){
                            if($item->code == $result['staffCode']){
                              echo "  <option value='".$item->code."' selected='selected'>工号:".$item->code."|姓名:".$item->name."|电话:".$item->mobile."</option>";
                            }else{
                              echo "  <option value='".$item->code."'>工号:".$item->code."|姓名:".$item->name."|电话:".$item->mobile."</option>";
                            } 
                         }
                    ?>
                  </select>
                </div> 
                <div class="form-group col-md-12">
                  <label class="control-label">报修评价</label>
                   <textarea name="repair-response" for="resident-repair" style="text-align:left;" readonly="readonly" cols="120" rows="3"  class="form-control" required="required"><?=$result['comment']?></textarea> 
                </div>
                <div class="clear"></div>
                <div class="form-group col-md-12">
                  <label class="control-label">服务评分</label>
                  <?php 
                    if($result['serviceStar']=="0"||$result['serviceStar']==""){
                      echo "<input name='serviceStar' type='text' class='form-control' for='resident-repair' readonly='readonly' value='暂无' required= 'required' />";
                    }else{
                      echo "<input name='serviceStar' type='text' class='form-control' for='resident-repair' readonly='readonly' value='".$result['serviceStar']."分' required= 'required' />";
                    }
                  ?>
                </div>
                <div class="form-group col-md-12">
                  <label class="control-label">技术评分</label>
                  <?php 
                    if($result['techStar']=="0"||$result['techStar']==""){
                      echo "<input name='serviceStar' type='text' class='form-control' for='resident-repair' readonly='readonly' value='暂无' required= 'required' />";
                    }else{
                      echo "<input name='serviceStar' type='text' class='form-control' for='resident-repair' readonly='readonly' value='".$result['techStar']."分' required= 'required' />";
                    }
                  ?>
                </div>
                <div class="clear"></div>
              </div>
              <!-- end -->
              
              
              <div class="clear"></div>
              <div class="col-md-12">
                <div class="form-group">
                <script>
                    function cancelOrder(){
                      var form = document.repairForm;
                      form.action = 'home/repair_order_cancel';
                      form.submit();
                    }
                </script>
                  <?php
                      if($result['status']!=4&&$result['status']!=5){
                        echo '<input type="button" class="btn btn-primary" id="repair-edit-btn" value="提交">&nbsp;';
                      }
                      if($result['orderNo']!=""&&$result['status']=='0'){
                        echo "<a href='javascript:void(0)' onclick='cancelOrder()' class='btn btn-warning' id='repair-edit-btn'>作废</a>";
                      }
                  ?> 
                  <a href="home/resident_repair" class="btn btn-success" rel="resident-repair-back">返回</a>          
                </div>
              </div>
            </form>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>