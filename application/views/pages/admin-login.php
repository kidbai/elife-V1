<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="col-md-12 col-xs-12 login-wrapper">
    <div class="col-md-offset-3 col-md-6 form-wrapper">
        <div class="login-form">
            <form action="propLogin/loader" method="post">
                <div class="form-group col-md-offset-2 col-md-8 col-sm-offset-3 col-sm-5 col-xs-12 logo">
                    <img class="company-logo" src="assets/img/icon_hexinyi2.png" alt="">
                    <h3>享生活后台管理系统</h3>
                </div>
                <div class="form-group col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-5 col-xs-12">
                    <div class="input-group">
                        <input type="text" name="username" class="form-control" required="required">
                        <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-5 col-xs-12">
                    <div class="input-group">
                        <input type="password" name="password" class="form-control" required="required">
                        <span class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-offset-3 col-md-3 col-sm-offset-3 col-sm-2 col-xs-5">
                    <div class="input-group">
                        <input type="text" name="validatecode" class="form-control" required="required" placeholder="验证码">
                        <span class="input-group-addon">
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-2 col-xs-3" name="securimage" style="cursor:pointer" >
                   <img id="imgObj" src="propLogin/securimage" width=130 height=36 
                    style="cursor:pointer;" onclick="changeImg()"/>
                </div> 
                <div class="clear"></div>
                <div class="checkbox col-md-offset-3 col-md-3 col-sm-offset-3 col-sm-3 col-xs-12">
                    <label>
                        <input type="checkbox" name="remember-me"> 记住我
                    </label>
                </div>
                <div class="form-group col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-5 col-xs-12">
                    <input type="submit" class="form-control btn btn-primary"> 
                </div>
                
            </form>
        </div>
    </div>
    <div id="login-footer" class="form-group col-md-12 col-sm-12 col-xs-12">
        版权所有：成都合心毅科技有限公司 备案号：<a href="http://www.miitbeian.gov.cn">蜀ICP备15012845号</a>
    </div>
</div>
<script>
    function changeImg(){
        var imgSrc = $("#imgObj");
        var src = imgSrc.attr("src");
        imgSrc.attr("src",chgUrl(src));
    }
    //时间戳
    //为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
    function chgUrl(url){
        var timestamp = (new Date()).valueOf();
        url = url.substring(0,17);
        if((url.indexOf("&")>=0)){
            url = url + "×tamp=" + timestamp;
        }else{
            url = url + "?timestamp=" + timestamp;
        }
        return url;
    }
    function isRightCode(){
        var code = $("#veryCode").attr("value");
        code = "c=" + code;
        $.ajax({
            type:"POST",
            url:"propLogin/securimage",
            data:code,
            success:callback
        });
    }
    function callback(data){
        //$("#info").html(data);
    }
</script>
<script src="assets/js/jquery-1.11.2.min.js"></script>
<script src="assets/js/admin-login.js"></script>
