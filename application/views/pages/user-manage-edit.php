<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>权限设置</h5>
      </div>
      <div class="box-content">
        <div class="box-block">
          <div class="title-block">
            <h3>基本信息设置</h3>
          </div>
          <div class="content-block">
            <form action="">
              <div class="form-group col-md-5">
                <label for="" class="control-label">用户姓名</label> 
                <input type="text" name="username" class="form-control" required="required">
              </div>
              <div class="clear"></div>
              <div class="form-group col-md-5">
                <label for="" class="control-label">性别</label> 
                <select name="gender" class="form-control">
                  <option value="male">男</option>
                  <option value="female">女</option>
                </select>
              </div>
              <div class="clear"></div>
              <div class="form-group col-md-5">
                <label for="" class="control-label">联系方式</label> 
                <input type="text" name="username" class="form-control" required="required" maxlength="11">
              </div>
              <div class="clear"></div>
              <div class="form-group col-md-5">
                <label for="" class="control-label">所属部门</label> 
                <select name="department" class="form-control">
                  <option value="property">物业部</option>
                  <option value="guard">安保部</option>
                </select>
              </div>
              <div class="clear"></div>
              <div class="form-group col-md-5">
                <label for="" class="control-label">角色类型</label> 
                <select name="role" class="form-control">
                  <option value="finance">财务</option>
                  <option value="manage">管理</option>
                  <option value="staffing">人事资源</option>
                </select>
              </div>
              <div class="clear-20"></div>
              <div class="form-group col-md-2">
                <input type="submit" class="form-control btn btn-primary">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>