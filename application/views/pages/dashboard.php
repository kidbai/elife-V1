<div class="col-md-12 col-xs-12">
    <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
      <i class="fa fa-arrow-left"></i> 
    </div>
    <div class="box">
      <div class="box-inner">
        <div class="box-header">
          <h5><i class="fa fa-wrench"></i>最近报修状态</h5>
        </div>
        <div class="box-content">
          <table id="dataTables-fix" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
           <thead>
              <tr>
                <th>序号</th>
                <th>报修项目</th>
                <th>业主姓名</th>
                <th>居住单元</th>
                <th>日期</th>
                <th>状态</th>
                <!-- <th>None</th> -->
              </tr>
            </thead>  
            <tbody>
              <tr>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>2</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>3</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>4</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>5</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>6</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>7</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>8</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>9</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
              <tr>
                <td>10</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div> 
    </div>
    <div class="box">
        <div class="box-inner">
            <div class="box-header">
                <h5><i class="fa fa-money"></i>业主账单最新状态</h5>
            </div>
            <div class="box-content">
              <table id="dataTables-bill" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>序号</th>
                    <th>水电费</th>
                    <th>物业费</th>
                    <th>停车费</th>
                    <th>业主姓名</th>
                    <th>居住单元</th>
                    <th>电话</th>
                  </tr>
                </thead> 
                <tbody>
                  <tr>
                    <td>1</td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾大松</td>
                    <th>1-2-2901</th>
                    <th>13800000000</th>
                  </tr>
                  
                  <tr>
                    <td>2</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td>顾二松</td>
                    <th>1-2-2902</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾三松</td>
                    <th>1-2-2903</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾四松</td>
                    <th>1-2-2904</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾五松</td>
                    <th>1-2-2905</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>6</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾6松</td>
                    <th>1-2-2906</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>7</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾8松</td>
                    <th>1-2-2907</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>8</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾8松</td>
                    <th>1-2-2907</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>9</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾8松</td>
                    <th>1-2-2907</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>10</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾8松</td>
                    <th>1-2-2907</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>11</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾8松</td>
                    <th>1-2-2907</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>12</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>龚云彬</td>
                    <th>1-2-2907</th>
                    <th>13800000000</th>
                  </tr>
                  <tr>
                    <td>13</td>
                    <td><span class="label label-danger">欠费</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td><span class="label label-success">已缴</span></td>
                    <td>顾8松</td>
                    <th>1-2-2907</th>
                    <th>13800000000</th>
                  </tr>
                </tbody>
            </table> 
           
            </div>
        </div>
    </div>
</div>