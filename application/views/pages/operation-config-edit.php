<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>参数设置编辑</h5>
      </div>
      <div class="box-content">
        <form action="" class="overflow-hidden">
          <div class="form-group col-md-5">
            <label class="control-label">参数名称</label>
            <input type="text" name="config-arg" class="form-control" required="required">
          </div> 
          <div class="clear"></div>
          <div class="form-group col-md-5">
            <label class="control-label">参数类型</label>
            <select class="form-control operation-config-select">
              <option value="numeric">数字</option>
              <option value="string">字符串</option>
              <option value="date">日期</option>
            </select>
          </div>
          <div class="clear"></div>
          <div class="form-group col-md-5">
            <label class="control-label">设置</label>
            <input type="text" rel="numeric" name="config-setting" class="form-control operation-config on" required="required" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"> 
            <input type="text" rel="string" name="config-setting" class="form-control operation-config" required="required" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
            <input type="datetime-local" rel="date" name="config-setting" class="form-control operation-config" required="required">
          </div>
          <div class="clear"></div>
          
        </form> 
        <div class="form-group col-md-5">
          <button class="btn btn-primary" id="operation-config-btn">提交</button>
        </div>
      </div>
    </div>
  </div>
</div>