<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>部门管理</h5>
      </div>
      <div class="box-content">
        <div class="clear-20"></div>
        <table id="tb-department" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>部门名称</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>保安部</td>
            </tr>
            <tr>
              <td>2</td>
              <td>事业部</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>