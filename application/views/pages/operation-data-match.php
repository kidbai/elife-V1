<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>数据匹配</h5>
       
      </div>
      <div class="box-content">
        <form action="">
          <div class="form-group">
            <span class="btn btn-success fileinput-button">
                <span>导入Excel表</span>
                <input id="fileupload" type="file" name="files[]" multiple>
            </span>
            <input class="display-none" type="text" name="file-name" value="">
            <div class="clear-10"></div>
            <!-- The global progress bar -->
            <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>
            <!-- The container for the uploaded files -->
            <div id="files" class="files"></div>
          </div>
        </form>
        <div class="clear-10"></div>
        <a href="/stencil-master/index.php/home/operation_data_match_edit" class="btn btn-success tb-data-match-item">添加</a>
        <button class="btn btn-danger tb-data-match-item" data-toggle="modal" data-target="
        #data-match-del">删除</button>
        <div class="clear-10"></div>
        <table id="tb-data-match" class="tb-data-match-item display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>房号</th>
              <th>业主名</th>
              <th>手机</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>1-2-2903</td>
              <td>张三</td>
              <td>18388183881</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_data_match_edit" class="btn btn-info">编辑</a>
              </td>
            </tr>
             <tr>
              <td>2</td>
              <td>内容1</td>
              <td>内容1</td>
              <td>内容2</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_data_match_edit" class="btn btn-info">编辑</a>
              </td>
            </tr>
             <tr>
              <td>3</td>
              <td>内容1</td>
              <td>内容1</td>
              <td>内容2</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_data_match_edit" class="btn btn-info">编辑</a>
              </td>
            </tr>
             <tr>
              <td>4</td>
              <td>内容1</td>
              <td>内容1</td>
              <td>内容2</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_data_match_edit" class="btn btn-info">编辑</a>
              </td>
            </tr>
             <tr>
              <td>5</td>
              <td>内容1</td>
              <td>内容1</td>
              <td>内容2</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_data_match_edit" class="btn btn-info">编辑</a>
              </td>
            </tr>
             <tr>
              <td>6</td>
              <td>内容1</td>
              <td>内容1</td>
              <td>内容2</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_data_match_edit" class="btn btn-info">编辑</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>