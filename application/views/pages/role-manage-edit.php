<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>权限设置</h5>
      </div>
      <div class="box-content">
        <div class="box-block">
          <div class="title-block">
            <h3>(角色名称)操作权限设置</h5>
          </div>
          <div class="content-block">
            <form action="" class="col-md-4">
              <ul class="sidebar limit">
                <li rel="0">
                  <div class="link"><i class="fa fa-home"></i>主界面<input type="checkbox" class="float-right" name="dashboard"></div>
                </li>
                <li rel="1">
                  <div class="link"><i class="fa fa-users"></i>业主管理<input type="checkbox" class="float-right" name="owner-manage"></div>
                </li>
                <li rel="2">
                  <div class="link"><i class="fa fa-money"></i>账单管理<input type="checkbox" class="float-right" name="bill"></div>
                </li>
                <li rel="3">
                  <div class="link"><i class="fa fa-cart-plus"></i>社区服务<input type="checkbox" class="float-right" name="community-serve"></i></div>
                </li>
                <li rel="4">
                  <div class="link"><i class="fa fa-user-secret"></i>职员管理<input type="checkbox" class="float-right" name="staff"></div>
                </li>
                <li rel="5"> 
                  <div class="link"><i class="fa fa-user-plus"></i>便民信息<input type="checkbox" class="float-right" name="resident-news"></div>
                </li>
                <li rel="6">
                  <div class="link"><i class="fa fa-line-chart"></i>统计分析<input type="checkbox" class="float-right" name="statistics"></div>
                </li>
                <li rel="7">
                  <div class="link"><i class="fa fa-gears"></i>系统配置<input type="checkbox" class="float-right" name="system"></div>
                </li>
              </ul>
              <div class="form-group">
                <input type="submit" class="form-control btn btn-primary">
              </div>
            </form>
            
          </div>
        </div> 
      </div>
    </div>
  </div>
</div>