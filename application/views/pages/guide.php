<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
    <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-pencil"></i>办事指南</h5>
      </div>
      <div class="box-content">
        <div class="clear-20"></div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <table id="tb-guide" class="display responsive nowrap table table-striped">
              <thead>
                <tr>
                  <th>序号</th>
                  <th>标题</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>结婚证办理流程</td>
                  <td>
                    <button class="btn btn-primary" rel="guide-details">查看</i></button>
                    <a href="/home/guide_actions" class="btn btn-info" rel="guide-actions">编辑</a>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>护照办理流程</td>
                  <td>
                    <button class="btn btn-primary" rel="guide-details">查看</i></button>
                    <a href="/home/guide_actions" class="btn btn-info" rel="guide-actions">编辑</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
