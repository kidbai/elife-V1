<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>参数设置</h5>
      </div>
      <div class="box-content">
        <a href="/stencil-master/index.php/home/operation_config_edit" class="btn btn-success" rel="operation-config-add">添加</a>
        <button class="btn btn-danger" data-toggle="modal" data-target="#operation-config-del">删除</button>
        <div class="clear-20"></div>
        <table id="tb-operation-config" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>参数名称</th>
              <th>设置</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>规则生效时间</td>
              <td>2015-02-04</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_config_edit" class="btn btn-info" rel="config-edit">编辑</a> 
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>图片大小</td>
              <td>5.2</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_config_edit" class="btn btn-info" rel="config-edit">编辑</a> 
              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>附件大小</td>
              <td>3.2</td>
              <td>
                <a href="/stencil-master/index.php/home/operation_config_edit" class="btn btn-info" rel="config-edit">编辑</a> 
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>