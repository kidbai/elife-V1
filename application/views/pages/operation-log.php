<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>操作日记</h5>
      </div>
      <div class="box-content">
        <table id="tb-operation-log" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>操作员</th>
              <th>操作类型</th>
              <th>操作时间</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>杨柏</td>
              <td>删除用户A</td>
              <td>2015-01-01</td>
            </tr>
            <tr>
              <td>2</td>
              <td>杨柏</td>
              <td>增加用户A</td>
              <td>2015-01-01</td>
            </tr>
            <tr>
              <td>3</td>
              <td>杨柏</td>
              <td>删除用户A</td>
              <td>2015-01-01</td>
            </tr>
            <tr>
              <td>4</td>
              <td>杨柏</td>
              <td>删除用户A</td>
              <td>2015-01-01</td>
            </tr>
            <tr>
              <td>5</td>
              <td>杨柏</td>
              <td>删除用户A</td>
              <td>2015-01-01</td>
            </tr>
            <tr>
              <td>6</td>
              <td>杨柏</td>
              <td>删除用户A</td>
              <td>2015-01-01</td>
            </tr>
            <tr>
              <td>7</td>
              <td>杨柏</td>
              <td>删除用户A</td>
              <td>2015-01-01</td>
            </tr>
            <tr>
              <td>8</td>
              <td>杨柏</td>
              <td>删除用户A</td>
              <td>2015-01-01</td>
            </tr>
            <tr>
              <td>9</td>
              <td>杨柏</td>
              <td>删除用户A</td>
              <td>2015-01-01</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>