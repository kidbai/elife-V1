<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>权限设置</h5>
      </div>
      <div class="box-content">
        <div class="box-block">
          <div class="title-block">
            <h3>基本信息设置</h3>
          </div>
          <div class="content-block">
            <form action="">
              <div class="form-group col-md-5">
                <label for="" class="control-label">用户姓名</label> 
                <input type="text" name="username" class="form-control" required="required">
              </div>
              <div class="clear"></div>
              <div class="form-group col-md-5">
                <label for="" class="control-label">性别</label> 
                <select name="gender" class="form-control">
                  <option value="male">男</option>
                  <option value="female">女</option>
                </select>
              </div>
              <div class="clear"></div>
              <div class="form-group col-md-5">
                <label for="" class="control-label">联系方式</label> 
                <input type="text" name="username" class="form-control" required="required" maxlength="11">
              </div>
              <div class="clear"></div>
              <div class="form-group col-md-5">
                <label for="" class="control-label">所属部门</label> 
                <select name="department" class="form-control">
                  <option value="property">物业部</option>
                  <option value="guard">安保部</option>
                </select>
              </div>
              <div class="clear-20"></div>
              <div class="form-group col-md-2">
                <input type="submit" class="form-control btn btn-primary">
              </div>
            </form>
          </div>
        </div>
        <div class="box-block">
          <div class="title-block">
            <h3>菜单操作权限设置</h5>
          </div>
          <div class="content-block">
            <form action="" class="col-md-4">
              <ul class="sidebar limit">
                <li rel="0">
                  <div class="link"><i class="fa fa-home"></i>主界面<input type="checkbox" class="float-right" name="dashboard"></div>
                </li>
                <li rel="1">
                  <div class="link"><i class="fa fa-users"></i>业主管理<input type="checkbox" class="float-right" name="owner-manage"></div>
                </li>
                <li rel="2">
                  <div class="link"><i class="fa fa-money"></i>账单管理<input type="checkbox" class="float-right" name="bill"></div>
                  <!-- <ul class="submenu">
                    <li><a href="/stencil-master/index.php/home/bill_manage" rel="1">业主账单</a></li>
                    <li><a href="/stencil-master/index.php/home/bill_rule" rel="2">规则设置</a></li>
                  </ul> -->
                </li>
                <li rel="3">
                  <div class="link"><i class="fa fa-cart-plus"></i>社区服务<input type="checkbox" class="float-right" name="community-serve"></i></div>
                  <!-- <ul class="submenu">
                    <li><a href="/stencil-master/index.php/home/resident_repair" rel="1">业主报修</a></li>
                    <li><a href="/stencil-master/index.php/home/public_monitoring" rel="2">公共监察</a></li>
                    <li><a href="/stencil-master/index.php/home/visitor" rel="3">访客通讯</a></li>
                  </ul> -->
                </li>
                <li rel="4">
                  <div class="link"><i class="fa fa-user-secret"></i>职员管理<input type="checkbox" class="float-right" name="staff"></div>
                  <!-- <ul class="submenu">
                    <li><a href="/stencil-master/index.php/home/staff_info" rel="1">职员信息</a></li>
                    <li><a href="/stencil-master/index.php/home/staff_comment" rel="2">职员评论</a></li>
                  </ul> -->
                </li>
                <li rel="5"> 
                  <div class="link"><i class="fa fa-user-plus"></i>便民信息<input type="checkbox" class="float-right" name="resident-news"></div>
                  <!-- <ul class="submenu">
                    <li><a href="/stencil-master/index.php/home/community_news" rel="1">小区动态</a></li>
                    <li><a href="/stencil-master/index.php/home/guide" rel="2">办事指南</a></li>
                  </ul> -->
                </li>
                <li rel="6">
                  <div class="link"><i class="fa fa-line-chart"></i>统计分析<input type="checkbox" class="float-right" name="statistics"></div>
                  <!-- <ul class="submenu">
                    <li><a href="/stencil-master/index.php/home/statistics_users_table" rel="1">用户统计</a></li>
                    <li><a href="/stencil-master/index.php/home/statistics_bill_table" rel="2">账单统计</a></li>
                    <li><a href="/stencil-master/index.php/home/statistics_monitoring_table" rel="3">监察统计</a></li>
                    <li><a href="/stencil-master/index.php/home/statistics_visitor_table" rel="4">访客统计</a></li>
                  </ul> -->
                </li>
                <li rel="7">
                  <div class="link"><i class="fa fa-gears"></i>系统配置<input type="checkbox" class="float-right" name="system"></div>
                  <!-- <ul class="submenu">
                    <li><a href="/stencil-master/index.php/home/operation_log" rel="1">操作日记</a></li>
                    <li><a href="/stencil-master/index.php/home/operation_data_match" rel="2">数据匹配</a></li>
                    <li><a href="/stencil-master/index.php/home/operation_config" rel="3">参数设置</a></li>
                    <li><a href="/stencil-master/index.php/home/operation_sys_backup" rel="4">系统备份</a></li>
                    <li><a href="/stencil-master/index.php/home/user_manage" rel="5">用户管理</a></li>
                  </ul> -->
                </li>
              </ul>
              <div class="form-group">
                <input type="submit" class="form-control btn btn-primary">
              </div>
            </form>
            
          </div>
        </div> 
      </div>
    </div>
  </div>
</div>