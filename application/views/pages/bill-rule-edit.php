<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>规则编辑</h5>
      </div>
      <div class="box-content">
        <form action="" class="overflow-hidden">
          <div class="form-group col-md-5">
            <label class="control-label">账单项目</label>
            <input name="bill-name" type="text" class="form-control" required="required">
          </div> 
          <div class="clear"></div>
          <div class="form-group col-md-5">
            <label class="control-label">规则</label>
            <input name="bill-rule" type="text" class="form-control" required="required">
          </div>
          <div class="clear"></div>
          <div class="form-group col-md-5">
            <input type="submit" class="btn btn-primary" id="bill-rule-edit-btn"></button>
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>