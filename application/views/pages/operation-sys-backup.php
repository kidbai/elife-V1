<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-wrench"></i>系统备份</h5>
      </div>
      <div class="box-content">
        <div class="box-block">
          <div class="title-block">
            <h3>系统备份</h3>
          </div>
          <div class="content-block">
            <button id="sys-import" class="btn btn-default">导入</button> 
            <button id="sys-export" class="btn btn-info">导出</button> 
          </div>
        </div>
        <div class="box-block">
          <div class="title-block">
            <h3>数据库备份</h3>
          </div>
          <div class="content-block">
            <button id="db-import" class="btn btn-default">导入</button> 
            <button id="db-export" class="btn btn-info">导出</button> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>