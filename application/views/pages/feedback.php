<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
    <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-pencil"></i>反馈建议</h5>
      </div>
      <div class="box-content">
        <div class="clear-20"></div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <table id="tb-feedback" class="display responsive nowrap table table-striped">
              <thead>
                <tr>
                  <th>序号</th>
                  <th>标题</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>反馈1</td>
                  <td>
                    <a href="/home/feedback_edit" class="btn btn-info" rel="feedback-action">回复</a>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>反馈2</td>
                  <td>
                    <a href="/home/feedback_edit" class="btn btn-info" rel="feedback-actions">回复</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
