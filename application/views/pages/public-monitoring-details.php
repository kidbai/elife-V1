 <div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>公共监察详情</h5>
      </div>
      <div class="box-content">
        <div class="box-block">
          <div class="title-block">
            <h3>图片</h3>
          </div>
          <div class="content-block">
            <div id="links">
              <a href="../../assets/img/background.jpg" title=''>
                  <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
              </a>
              <a href="../../assets/img/background.jpg">
                  <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
              </a>
              <a href="../../assets/img/background.jpg" >
                  <img src="../../assets/img/background.jpg" for="public-monitoring-img" width="100" height="100">
              </a>
            </div> 
          </div>
        </div>
        <div class="box-block">
          <div class="title-block">
            <h3>基本信息</h3>
          </div>
          <div class="content-block">
            <div class="form-group">
              <label>
                类型:
                <label class="label label-danger">设备</label>
              </label>
            </div>
            <div class="form-group">
              <label>
                说明:
                <label class="text-indent">小区有脏东西a小区有脏东西i小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西i小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西i小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西i小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西i小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西小区有脏东西</label>
              </label>
            </div>
            <div class="form-group">
              <label>
                监察人:
                <label>杨柏</label>
              </label>
            </div>
            <div class="form-group">
              <label>
                监察人住址:
                <label>1-2-2903</label>
              </label>
            </div>
            <div class="form-group">
              <label>
                时间:
                <label>2015-09-09 08:00</label>
              </label>
            </div>
          </div>
        </div>
        <div class="box-block">
          <div class="title-block">
            <h3>监察状态</h3>
          </div>
          <div class="content-block">
            <div class="form-group padding-5">
              <label class="radio-inline">
                <input type="radio" name="public-monitoring-status" value="processing" checked> 处理中 
              </label>
              <label class="radio-inline">
                <input type="radio" name="public-monitoring-status" value="done"> 已完成
              </label>
              <label class="radio-inline">
                <input type="radio" name="public-monitoring-status" value="invalid"> 无效
              </label>
            </div>
            <div class="form-group">
              <button type="submit" id="public-status-btn" class="btn btn-primary">提交</button>
            </div> 
          </div>
        </div>
        <div class="box-block">
          <div class="title-block">
            <h3>评论审核</h3>
          </div>
          <div class="content-block">
            <table id="tb-public-monitoring-comment" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>序号</th>
                  <th>评论内容</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>1</th>
                  <th>非常不错</th>
                  <th>
                    <label>
                      <input type="checkbox"> 审核通过
                    </label>
                  </th>
                </tr>
                <tr>
                  <th>2</th>
                  <th>非常不错</th>
                  <th>
                    <label>
                      <input type="checkbox"> 审核通过
                    </label>
                  </th>
                </tr>
                <tr>
                  <th>3</th>
                  <th>非常不错</th>
                  <th>
                    <label>
                      <input type="checkbox"> 审核通过
                    </label>
                  </th>
                </tr>
              </tbody>
            </table>
            <button id="public-comment-btn" class="btn btn-primary">提交</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>