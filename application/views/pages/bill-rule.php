<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>规则设置</h5>
      </div>
      <div class="box-content">
        <a href="home/bill_rule_edit" class="btn btn-success" rel="bill-rule-add">添加</a>
        <button class="btn btn-danger disabled" data-toggle="modal" data-target="#bill-rule-del">删除</button>
        <div class="clear-20"></div>
        <table id="tb-bill-rule" class="display responsive nowrap table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>序号</th>
              <th>账单项目</th>
              <th>规则</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>水电费</td>
              <td>x + y</td>
              <td>
                <a href="home/bill_rule_edit" class="btn btn-info" rel="bill-rule-edit">编辑</a>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>物业费</td>
              <td>x + y</td>
              <td>
                <a href="home/bill_rule_edit" class="btn btn-info" rel="bill-rule-edit">编辑</a>
              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>停车费</td>
              <td>x + y</td>
              <td>
                <a href="home/bill_rule_edit" class="btn btn-info" rel="bill-rule-edit">编辑</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>