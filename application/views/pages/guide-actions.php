<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
    <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-pencil"></i>办事指南</h5>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="form-group">
              <label for="" class="label-control">标题</label>
              <input type="text" class="form-control" placeholder="标题长度不要超过**个字">
            </div> 
            <div class="form-group">
              <label for="" class="label-control">内容</label>
              <textarea name="content" id="guide-editor" style="width:900px;height:300px;"></textarea>
            </div>
            <div class="clear-10"></div>
            <div class="form-group">
              <button class="btn btn-primary" id="guide-actions-btn">确定</button> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>