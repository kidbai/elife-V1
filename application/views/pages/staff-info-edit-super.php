<div class="col-md-12 col-xs-12">
  <div class="open-sidebar visible-xs-block" data-toggle="offcanvas">
     <i class="fa fa-arrow-left"></i> 
  </div>
  <div class="box">
    <div class="box-inner">
      <div class="box-header">
        <h5><i class="fa fa-gear"></i>职员信息编辑</h5>
      </div>
      <div class="box-content">
       <form id="staff-form" action="">
        <div class="form-group">
          <label for="staff-name" class="label-control">职员名称</label>
          <input name="staff-name" type="text" class="form-control" required="required">
        </div>
        <div class="form-group">
          <label for="staff-gender" class="label-control">性别</label>
          <!-- <input name="staff-gender" type="text" class="form-control" required="required"> -->
          <select name="staff-gender" class="form-control">
            <option value="male">男</option>
            <option value="female">女</option>
          </select>
        </div>
        <div class="form-group">
          <label for="number" class="label-control">职员工号</label>
          <input name="number" type="text" class="form-control" required="required">
        </div>
        
        <div class="form-group">
          <label for="staff-phone" class="label-control">电话</label>
          <input name="phone" type="text" class="form-control" required="required" maxlength="11">
        </div>
        <div class="form-group">
          <label for="department" class="label-control">部门</label>
          <select name="department" class="form-control">
            <option value="wuye">物业部</option>
            <option value="guard">警卫部</option>
            <option value="clear">清洁部</option>
          </select>
        </div>
        <div class="form-group">
          <label for="register-date" class="label-control">入职时间</label>
          <input name="register-date" type="date" class="form-control" required="required">
        </div>
       
        <!-- *****上传照片***** -->
        <div class="form-group">
          <!-- The fileinput-button span is used to style the file input field as button -->
          <img id="staff-avatar" src="" alt="" width="50" height="50">
          <div class="clear-10"></div>
          <span class="btn btn-success fileinput-button">
              <span>上传照片</span>
              <input id="fileupload" type="file" name="files[]" multiple>
          </span>
          <input class="display-none" type="text" name="file-name" value="">
          <div class="clear-10"></div>
          <!-- The global progress bar -->
          <div id="progress" class="progress">
              <div class="progress-bar progress-bar-success"></div>
          </div>
          <!-- The container for the uploaded files -->
          <div id="files" class="files"></div>
        </div> 
        <div class="form-group">
          <select name="limit" class="form-control">
            <option value=""></option>
          </select>
        </div>
        <button class="btn btn-primary" id="staff-edit-btn">提交</button>
      </form> 
      </div>
    </div>
  </div>
</div>