<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<?php echo $head; ?>
</head>
<body>
	<nav class="navbar navbar-default navigation">
		<?php echo $header; ?>
	</nav>

	<div class="content-wrap content-wrap-left">
		<div class="content-sidebar">
			<?php echo $sidebar; ?>
		</div>
		<div class="content-body">
			<?php echo $content; ?>
		</div>
	</div>

	<div class="push-msg-wrapper hidden-xs">
		<div class="push-msg">
			<ul>
				<li>
					<a href="home/resident_repair">
						<i class="fa fa-info-circle"></i>
						有<span></span>条新的保修
					</a>
				</li>
			</ul>
   		</div>
	</div>
	
	<div class="load-animation"></div>
	

	<?php echo $jsfile ?>
</body>
</html>
