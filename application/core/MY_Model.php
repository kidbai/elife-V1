<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 * 封装Model基础类
	 * 
	 * @author 顾小松(npctest@cdhexinyi.com)
	 * @since 1.0.0
	 * @date 2015-04-09 20:39:44
	 */
	class MY_Model extends CI_Model{

	public function __construct($model='')
	{
		parent::__construct($model);
	}
	// ------------------------------------------------------------------------
	// 查询数据部分
	// ------------------------------------------------------------------------
	/**
	 * 基类查询入口
	 * 
	 * @args array 获取方法参数
	 * @argsCount int 获取参数个数
	 * @return 根据参数个数返回对应方法
	 */
	public function queryObject()
	{

		$args = func_get_args();
		$argsCount=count($args);
		switch ($argsCount) {
			case 1:
				return self::queryObjectByQuery($args[0]);	//1.string 查询语句
				break;
			case 2:
				return self::queryObject2Params($args[0],$args[1]);   //1.string-表名 2.array-条件
				break;
			case 3:
				return self::queryObject3Params($args[0],$args[1],$args[2]);	//1.string-表名 2.int-数据条数 3.int-当前页数
				break;
			case 4:
				return self::queryObject4Params($args[0],$args[1],$args[2],$args[3]);	//1.string-表名 2.array-条件数组 3.int-数据条数 4.int-当前页数
				break;
			case 5:
				return self::queryObject5Params($args[0],$args[1],$args[2],$args[3],$args[4]);	//1.string-表名 2.string-列名 3.array-条件数组 4.int-数据条数 5.int-当前页数
				break;
			case 6:
				return self::queryObject6Params($args[0],$args[1],$args[2],$args[3],$args[4],$args[5]);	//1.string-表名 2.string-列名 3.array-条件数组 4.string-排序条件 5.int-数据条数 6.int-当前页数
				break;
			default:
				# code...
				break;
		}
	}

	public function queryObjectNum($table,$strWhere)
	{
		$res=$this->db->get_where($table,$strWhere);
		//echo $this->db->last_query();
		//return $res->num_rows();
		return $res->row_array();
	}

	public function queryObjectReturnCount($table,$strWhere)
	{
		$this->db->where($strWhere);
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	//查询表的列名属性
	public function queryTableFields($tableName)
	{
		$strSQL = "SHOW COLUMNS FROM $tableName";
		$res = $this->db->query($strSQL);
		return $res->result();
	}

	//查询一个表所有数据
	public function queryTable($table)
	{
		$res=$this->db->get($table);
		return $res->result();
	}

	//通过SQL语句查询数据集
	public function queryObjectByQuery($query)
	{
		$res=$this->db->query($query);
		//log_message('INFO',$this->db->last_query());
		return $res->result();
	}

	//通过SQL语句查询一条数据
	public function queryObjectBySQLData($query)
	{
		$res=$this->db->query($query);
		return $res->row_array();
	}

	public function queryObject2Params($table,$strWhere)
	{
		$res=$this->db->get_where($table,$strWhere);
		return $res->result();
	}

	public function queryObjectData($table,$strWhere)
	{
		$res=$this->db->get_where($table,$strWhere);
		// log_message('INFO','查询的是：'.$this->db->last_query());
		return $res->row_array();
	}

	public function queryObject3Params($table,$num,$offset)
	{
		$this->db->order_by('sid','desc');
		$res=$this->db->get($table,$num,$offset);
		//log_message('INFO',$this->db->last_query());
		return $res->result();
	}

	public function queryObject4Params($table,$strWhere,$num,$offset)
	{
		$this->db->order_by('sid','desc');
		$res=$this->db->get_where($table,$strWhere,$num,$offset);
		log_message('INFO',$this->db->last_query());
		return $res->result();
	}

	public function queryObject5Params($table,$strFields,$strWhere,$num,$offset)
	{
		$this->db->order_by('sid','desc');
		$this->db->select($strFields);
		$res=$this->db->get_where($table,$strWhere,$num,$offset);
		return $res->result();
	}

	public function queryObjectOrderby($table,$strWhere,$strOrder,$num,$offset)
	{
		$this->db->order_by($strOrder);
		$res=$this->db->get_where($table,$strWhere,$num,$offset);
		return $res->result();
	}

	public function queryObject6Params($table,$strFields,$strWhere,$strOrder,$num,$offset)
	{
		$this->db->order_by($strOrder);
		$this->db->select($strFields);
		$res=$this->db->get_where($table,$strWhere,$strOrder,$num,$offset);
		return $res->result();
	}

	// ------------------------------------------------------------------------


	// ------------------------------------------------------------------------
	//	插入数据部分
	// ------------------------------------------------------------------------
	/**
	 * 基类新增数据入口
	 * 
	 * @args array 获取方法参数
	 * @argsCount int 获取参数个数
	 * @return 根据参数个数返回对应方法
	 */
	public function insertObject()
	{
		$args=func_get_args();
		$argsCount=count($args);
		switch ($argsCount) {
			case 2:
				return self::insertObject2Params($args[0],$args[1]);	//1.string-表名 2.array-数据数组
				break;
			
			default:
				# code...
				break;
		}
	}

	public function insertObject2Params($table,$data)
	{
		$res=$this->db->insert($table, $data);
		//echo $this->db->last_query();
		return $res;
	}

	// ------------------------------------------------------------------------
	

	// ------------------------------------------------------------------------
	//	更新数据部分
	// ------------------------------------------------------------------------
	/**
	 * 基类更新数据入口
	 * 
	 * @args array 获取方法参数
	 * @argsCount int 获取参数个数
	 * @return 根据参数个数返回对应方法
	 */
	public function updateObject()
	{
		$args=func_get_args();
		$argsCount=count($args);
		switch ($argsCount) {
			case 3:
				return self::updateObject3Params($args[0],$args[1],$args[2]);	//[更新单条数据] 1.string-表名 2.array-数据数组 3.array- 条件数组
				break;
			case 4:
				return self::updateObject4Params($args[0],$args[1],$args[2],$args[3]); //更新数据 1.string-表名 2.array-数据数组 3.array- 条件数组 4.bool-是否开启CI过滤[防注入],false关闭过滤，防止列名被转义
				break;
			default:
				# code...
				break;
		}
	}


	public function updateObject3Params($table,$data,$strWhere)
	{
		$res=$this->db->update($table, $data,$strWhere);
		log_message('INFO',$this->db->last_query());
		return $res;
	}

	public function updateObject4Params($table,$data,$strWhere,$escape)
	{
		foreach ($data as $key => $value) {
			$this->db->set($key,$value,$escape);
		}
		$this->db->where($strWhere);
		$res=$this->db->update($table);
		return $res;
	}

	// ------------------------------------------------------------------------
	

	// ------------------------------------------------------------------------
	//	删除数据部分
	// ------------------------------------------------------------------------
	/**
	 * 基类删除数据入口
	 * 
	 * @args array 获取方法参数
	 * @argsCount int 获取参数个数
	 * @return 根据参数个数返回对应方法
	 */
	public function deleteObject()
	{
		$args=func_get_args();
		$argsCount=count($args);
		switch ($argsCount) {
			case 2:
				return self::deleteObject2Params($args[0],$args[1]);	//[删除单条数据] 1.string-表名 2.条件数组
				break;
			
			default:
				# code...
				break;
		}
	}

	public function deleteObject2Params($table,$strWhere)
	{
		$res=$this->db->delete($table,$strWhere);
		//echo $this->db->last_query();
		return $res;
	}

	// ------------------------------------------------------------------------

}

/* End of file MY_Model.php */
/* Location: ./application/models/MY_Model.php */
?>