<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	/**
	* 自定义BaseController类
	*
	* 预先加载CI框架一些基础类库&权限控制&登录状态判断
	* @author	顾小松(npctest@cdhexinyi.com)
	* @date		2015-04-06 18:23:12         
	* @since	1.0
	*/
	Class MY_Controller extends CI_Controller
	{
	 	public	function __construct($type = null,$controller)
		{
			parent::__construct($controller);
			switch ($type) {
				case 'app':
					break;
				case 'web':
					session_save_path('assets/tmp');
					Session_start();
					$config = $this->load->config('stencil');
					$this->load->library('stencil', $config);
					$this->load->library('slices');
					$this->load->helper('stencil');
					break;
				default:			
					break;
			}	
		 	$this->load->database();	//加载数据库类库
		 	$this->load->library('common'); //加载自定义公共类库common
		}	
	}
?>