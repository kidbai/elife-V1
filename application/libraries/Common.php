<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	 * 自定义公共辅助类
	 * 
	 * @author 顾小松(npctest@cdhexinyi.com)
	 * @date 2015-04-07 21:24:45
	 * @since 1.0.0
	 */
	class Common{

		protected $CI;

		public function __construct()
		{
			$this->CI =& get_instance();
		}

		/**
		 * [get_Clinet_Ip 获取客户端IP地址信息]
		 * 
		 * @return [string] [IP地址 例如8.8.8.8]
		 * @date 2015-04-07 21:25:09
		 * @since 1.0.0
		 */
		public function get_Clinet_Ip()
		{
			 global $ip;
		    if (getenv("HTTP_CLIENT_IP"))
		        $ip = getenv("HTTP_CLIENT_IP");
		    else if(getenv("HTTP_X_FORWARDED_FOR"))
		        $ip = getenv("HTTP_X_FORWARDED_FOR");
		    else if(getenv("REMOTE_ADDR"))
		        $ip = getenv("REMOTE_ADDR");
		    else $ip = "Unknow";
		    return $ip;
		}

		public function getSerialNumber()
		{
			date_default_timezone_set("Asia/Shanghai");
       	 	return date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
		}

		/**
		 * [arrayRecursive 对数组以自定义函数进行递归处理]
		 * @param  [type]  &$array             [数据数组]
		 * @param  [type]  $function           [函数名 例如'urlencode']
		 * @param  boolean $apply_to_keys_also [是否应用于数组键]
		 * @return [type]                      [处理过的数组]
		 */
		public function arrayRecursive(&$array, $function, $apply_to_keys_also = false)
		{
		    static $recursive_counter = 0;
		    if (++$recursive_counter > 1000) {
		        die('possible deep recursion attack'); 	//递归隐含危险可能造成服务器stack爆满，服务器宕机
		    }
		    foreach ($array as $key => $value) {
		        if (is_array($value)) {
		            arrayRecursive($array[$key], $function, $apply_to_keys_also);
		        } else {
		            $array[$key] = $function($value);
		        }
		 
		        if ($apply_to_keys_also && is_string($key)) {
		            $new_key = $function($key);
		            if ($new_key != $key) {
		                $array[$new_key] = $array[$key];
		                unset($array[$key]);
		            }
		        }
		    }
		    $recursive_counter--;
		}



		/**
		 * [sendTemplateSMS 云通信发送模版短信]
		 * @param  [string] $to [对方手机号码，多个以英文逗号隔开]
		 * @return [bool]     [是否发送成功]
		 */
		public function sendYunTemplateSMS($to)
		{
			include_once("Rest.php");
			//主帐号
			$accountSid= 'aaf98f894c7d3aca014c8e06f95b0788';
			//主帐号Token
			$accountToken= '0a157b56e40141409f10d89b8f2656cb';
			//应用Id
			$appId='aaf98f894c7d3aca014c8e076254078a';
			//请求地址，格式如下，不需要写https://
			$serverIP='sandboxapp.cloopen.com';
			//请求端口 
			$serverPort='8883';
			//REST版本号
			$softVersion='2013-12-26';
			// 初始化REST SDK
			#$this->load->library('rest');
			$rest =  new REST($serverIP,$serverPort,$softVersion);
    		$rest->setAccount($accountSid,$accountToken);
    		$rest->setAppId($appId);
    		 // 发送模板短信
		     echo "Sending TemplateSMS to $to <br/>";
		     $verifyCode=rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
		     echo $verifyCode."<br/>";
		     $datas=array($verifyCode,'10');
		     $tempId=1;
		     $result = $rest->sendTemplateSMS($to,$datas,$tempId);
		     if($result == NULL ) {
		         echo "result error!";
		         break;
		         return false;
		     }
		     if($result->statusCode!=0) {
		         echo "error code :" . $result->statusCode . "<br>";
		         echo "error msg :" . $result->statusMsg . "<br>";
		         //TODO 添加错误处理逻辑
		          return false;
		     }else{
		         echo "Sendind TemplateSMS success!<br/>";
		         // 获取返回信息
		         $smsmessage = $result->TemplateSMS;
		         echo "dateCreated:".$smsmessage->dateCreated."<br/>";
		         echo "smsMessageSid:".$smsmessage->smsMessageSid."<br/>";
		         //TODO 添加成功处理逻辑
		         return true;
		     }
		}

		/**
		 * [sendYunPianSMS 发送云片网接口短信]
		 * @param  [type] $to []
		 * @return [type]     [description]
		 */
		public function sendYunPianSMS($to)
		{
			$url='http://yunpian.com/v1/sms/tpl_send.json';
			$verifyCode=rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
			$exprise=10;
			$data=array(
						'apikey'=>'32d6e44743afb22b777efb51220dde0e',
						'mobile'=>$to,
						'tpl_id'=>'751333',
						'tpl_value'=>"#code#=".$verifyCode."&#minutes#=".$exprise
						);
			$data=http_build_query($data); 	//生成 URL-encode 之后的请求字符串(对请求参数进行编码)
			// 创建一个cURL资源
			$ch  =  curl_init ();
			// 设置URL和相应的选项
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt ($ch,CURLOPT_URL,$url);
			curl_setopt ($ch, CURLOPT_POSTFIELDS,$data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(  
            'Accept:text/plain;charset=utf-8',
            'Content-Type:application/x-www-form-urlencoded;charset=utf-8',  
            'Content-Length: '.strlen($data)));  
			ob_start();  
			// 抓取URL并把它传递给浏览器
	        curl_exec($ch);  
	        $return_content = ob_get_contents();  
	        ob_end_clean(); 
			// 关闭cURL资源，并且释放系统资源
			curl_close ( $ch );
			//echo $return_content;
			$arrayresult=json_decode($return_content);
			//log_message('error',$arrayresult);
			//var_dump($arrayresult);
			if(count($arrayresult)>0)
			{
				$res=self::stdClassToArray($arrayresult);
				if($res['code']==0)
				{
					$this->CI->load->model('SMS_Model');
					$addres=$this->CI->SMS_Model->addSMS($to,$verifyCode,$exprise);
					$back["code"]='1';
					$back["msg"]='发送成功！';
					$back["data"]=$verifyCode;
					$back["serTime"] = date("Y-m-d H:i:s");
					$back=json_encode($back,JSON_UNESCAPED_UNICODE);
					echo $back;
				}
				else
				{
					//echo $return_content;
					//echo "<br />";
					//echo $arrayresult['code'];
					$back["code"]='0';
					$back["msg"]='抱歉，发送失败。请联系服务商！';
					$back["serTime"] = date("Y-m-d H:i:s");
					$back=json_encode($back,JSON_UNESCAPED_UNICODE);
					echo $back;
				}
			}else
			{
				//echo $return_content;
			}
		}

		/**
		 * [getReturnSMS 获取回复消息]
		 * @return [type] [description]
		 */
		public function getReturnSMS()
		{
			$info=urldecode($_POST['sms_reply']);
			$f = fopen('test.txt', 'a+');
			fwrite($f, $info);
			fclose($f);
			echo 'SUCCESS';
		}

		/**
		 * [processPic 图片缩略图处理]
		 * @return [type] [图片处理配置]
		 */
		public function processPic($source,$dir,$width,$height)
		{
			#$CI=&get_instance();
			$config['image_library'] = 'gd2'; //设定图像库
			$config['source_image'] = $source; //原图片地址
			$config['new_image'] = $dir; //缩略图指定地址
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = FALSE;
			$config['thumb_marker']='_thumb_'.$width.'_'.$height; //设置缩略图后缀
			$config['width'] = $width; //宽
			$config['height'] = $height; //高
			return $config;
		}
 		
 		/**
 		 * [procPicRotate 图片处理类配置 旋转图片逆时针270°/顺时针90°]
 		 * @param  [type] $source [图片路径]
 		 * @return [type]         [配置$config]
 		 */
 		public function procPicRotate($source)
 		{
 			$config['image_library'] = 'gd2'; //设定图像库
			$config['source_image'] = $source; //原图片地址
			$config['rotation_angle']='270'; //设定图片逆时针旋转270°=顺时针90°
			return $config;
 		}

 		/**
 		 * [uploadFiles 上传图片]
 		 * @return [type] [description]
 		 */
 		public function uploadFiles()
 		{
 			$config['upload_path'] = './assets/upload/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '100';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';
			//【如果你在 config文件夹内的 autoload.php 文件中自动加载了 upload 类，或者在构造函数内加载了的话，可以调用初始化函数 initialize 来加载设置。】
			#$this->upload->initialize($config);	
 		}

 		/**
 		 * [stdClassToArray 数组对象转成数组]
 		 * @param  [type] $array [description]
 		 * @return [type]        [description]
 		 */
 		public function stdClassToArray($array)
 		{
		   if(is_object($array))
		   {
		   		$array = (array)$array;
		   }
		   if(is_array($array))
		   {	
		   		foreach($array as $key=>$value)
		    	{
		    		$array[$key] =self::stdClassToArray($value);
		   		}
		   }
		   return $array;
		}

		/**
		 * [proc_Token 服务端端生成APP Token后返回]
		 * @param  [type] $equipmentNo [description]
		 * @param  [type] $mobileType  [description]
		 * @return [type]              [description]
		 */
		public function procAppToken($mobile,$equipmentNo,$mobileType)
		{
			$privateKey=md5('cdhexinyi2015'.uniqid());
			$token=md5($privateKey.$equipmentNo);
			$token=md5($token.$mobileType);
			$data=array(
				'mobile'=>$mobile,
				'token_id'=>$token,
				'mobileType'=>$mobileType,
				'expires_time'=>'1209600',
				'last_time'=>date("Y-m-d H:i:s")
				);
			$this->CI->load->model('Apptoken_model');
			$query = $this->CI->Apptoken_model->queryToken($mobile);
			if($query['token_id']!=""&&isset($query['token_id'])){
				$this->CI->Apptoken_model->logout($mobile);  //删除已有token
			}
			$back=$this->CI->Apptoken_model->addToken($data);
			if($back){
				return $token;
			}else{
				return false;
			}
		}

		/**
		 * 处理IOS上传图片类 
		 * @Date 2015-06-16 10:57:01
		 * @author npctest
		 * @param useType -> 模块名字
		 * @param picBuffer -> 图片流
		 */
		public function uploadIOSPictures($useType,$picBuffer)
		{
			$count=count($picBuffer);
			$picArray=array();
			for($i=0;$i<$count;$i++)
			{
				$byte = str_replace(' ','',$picBuffer[$i]);   //处理数据 
			    $byte = str_ireplace("<",'',$byte);
			    $byte = str_ireplace(">",'',$byte);
			    $byte=pack("H*",$byte);      //16进制转换成二进制
			    date_default_timezone_set("Asia/Shanghai");
			    $filename=$useType.self::getMillisecond()."_".$i.".png";
			    $picArray[$i]="assets/upload/".$filename;
	     		$file = fopen("./assets/upload/".$filename,"w");//打开文件准备写入
				fwrite($file,$byte);//写入
				fclose($file);//关闭
			}
			$arrayCount=count($picArray);
			if($arrayCount>0){
					$this->CI->load->model('Picture_model');
					$picNum=self::getMillisecond();
					for($i=0;$i<$arrayCount;$i++){
						$data=array(
								'picNum'=>$picNum,
								'picAddr'=>base_url()."".$picArray[$i],
								'useType'=>$useType,
								'addTime'=>date("Y-m-d H:i:s")
							);
						$res=$this->CI->Picture_model->addPicture($data);
					}
			}
			return $picNum;
		}

		/**
		 * 通用上传图片
		 * @Date 2015-06-16 11:00:52
		 * @author npctest
		 * 
		 * @param useType 使用途径
		 */ 
		public function uploadImage($useType)
		{
			$picAddr=array(); //图片地址
			$config = array(); //图片存储配置
		    $this->CI->load->library('upload');
		    if(!empty($_FILES)){
		    	foreach($_FILES as $key=>$value){
			   // if($value['name']!=''){
				    $type = strstr($value['name'],'.');
				    $super = date("YmdHis").uniqid().rand(1,999999); //时间加上随机数
				    $fileName = $useType."_".$super.$type; //生成随机名
				    unset($config);
				    $config['upload_path'] = "assets/upload/";
				    $config['allowed_types'] = 'gif|jpg|png|jpeg';
				    $config['file_name'] = $fileName;
				    $this->CI->upload->initialize($config);
				    if(!$this->CI->upload->do_upload($key)){
						    //$error = array('error' => $this->upload->display_errors());
					   return false;
					   exit();
					}else{
					     $data = array('upload_data' => $this->CI->upload->data());
					     $picAddr[] = base_url()."assets/upload/".$data['upload_data']['file_name'];
					}
				//}
				}
				$arrayCount=count($picAddr);
				if($arrayCount>0){
					$this->CI->load->model('Picture_model');
					$picNum=self::getMillisecond();
					for($i=0;$i<$arrayCount;$i++){
						$data=array(
								'picNum'=>$picNum,
								'picAddr'=>$picAddr[$i],
								'useType'=>$useType,
								'addTime'=>date("Y-m-d H:i:s")
							);
						$res=$this->CI->Picture_model->addPicture($data);
					}
					return $picNum;
				}else{
					return false;
				}
		    }else{
		    	return false;
		    }	
		}


		/**
		 * [getMillisecond 生成随机数]
		 * @return [type] [description]
		 */
		function getMillisecond() {
				list($t1, $t2) = explode(' ', microtime());
				return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
		}

		/**
		 * 生成CI自带验证码
		 * @return 验证码图像
		 */
		public function createCaptcha()
		{
			$this->CI->load->helper('captcha');
			$vals=array(
						'img_path' => 'assets/captcha/',
					    'img_url' => 'assets/captcha/',
					    'img_width' => '80',
					    'img_height' => '36',
					    'expiration' => 7200
    					);
			$cap=create_captcha($vals);
			return $cap['image'];
		}

		/**
		 * 验证-验证码
		 * @Date 2015-06-17 16:54:23
		 * @author npctest
		 * 
		 * @param code -> 验证码
		 * @return ture/false 
		 */
		public function verifyCode($code)
		{
			if(isset($_SESSION['validatecode'])){
				if(strtoupper($code) == strtoupper($_SESSION['validatecode'])){
					log_message('INFO','输入的验证码是:'.$code."|".$_SESSION['validatecode']);
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
	}
?>