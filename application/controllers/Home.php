<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct()
	{
		parent::__construct('web','Home');
		$this->stencil->slice('head'); //加载head部分所需资源
		$data = array();
		if(isset($_SESSION['userInfo'])){
			$userInfo =  (array)$_SESSION['userInfo'];
			$data['userInfo'] = self::getUserInfo($userInfo); //获取登录信息及权限.
		}else{
			self::index();
		}
		$this->stencil->data($data);
		$this->stencil->slice('header'); //加载页面head部分页面内容
	}

// ------------------------------------------------------------------------数据加载准备
	public function getUserInfo($userInfo)
	{
		$this->load->model('Community_model');
		$commdesc = $this->Community_model->getCommdesc($userInfo['commid']);
		$userInfo['commdesc'] = $commdesc['remark'];
		return $userInfo;
	}

// ------------------------------------------------------------------------登录部分
	public function index()
	{
		if(isset($_SESSION['userInfo'])){
			$userInfo = (array)$_SESSION['userInfo'];
			redirect('home/dashboard');
		}else{
			redirect('propLogin','refresh');
		}
	}

	public function getPHP()
	{
		echo phpinfo();
	}

// ------------------------------------------------------------------------主页面部分

	//主界面
	public function dashboard()
	{	
		$this->stencil->layout('main_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('dashboard.php');
	}

// ------------------------------------------------------------------------用户管理
	//用户管理
	public function owner_manage()
	{
		$data['script_path'] = [];
		// $data['script_path_1'] = 'assets/js/user-manage.js';
		array_push($data['script_path'], 'assets/js/owner-manage.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('owner-manage.php', $data);
	}

	//用户编辑
	public function owner_manage_edit()
	{
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->css('bootstrap-datetimepicker.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$data['script_path'] = array();
		array_push($data['script_path'], 'assets/js/owner-manage-edit.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.min.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.zh-CN.js');
		$this->stencil->paint('owner-manage-edit.php', $data);
	}

// ------------------------------------------------------------------------账单管理
	//账单管理 
	public function bill_manage()
	{
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/jquery-migrate-1.2.1.min.js');
		array_push($data['script_path'], 'assets/js/jquery.jqprint-0.3.js');
		array_push($data['script_path'], 'assets/js/bill-manage.js');
		$this->stencil->paint('bill-manage.php', $data);
	}

	//线下缴费
	public function bill_offline_pay()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/bill-offline-pay.js');
		array_push($data['script_path'], 'assets/js/bootstrap-dialog.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->css('bootstrap-dialog.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('bill-offline-pay.php', $data);
	}

	//规则设置
	public function bill_rule()
	{
		$data['script_path'] = array();
		array_push($data['script_path'], 'assets/js/bill-rule.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('bill-rule.php', $data);
	}

	public function order_record()
	{
		$data['script_path'] = array();
		array_push($data['script_path'], 'assets/js/order-record.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('order-record.php', $data);
	}

	//规则编辑
	public function bill_rule_edit()
	{
		$data['script_path'] = [];
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('bill-rule-edit.php', $data);
	}

// ------------------------------------------------------------------------家用报修
	//家用报修
	public function resident_repair()
	{	
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->css('bootstrap-dialog.min');
		$this->stencil->css('bootstrap-datetimepicker.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$data['script_path'] = array();
		// $userInfo =  (array)$_SESSION['userInfo'];
		// $this->load->model('Repair_model');
		// $data['result'] = $this->Repair_model->getRepairByCommid($userInfo['commid'],20,self::getNowPage(20));
		array_push($data['script_path'], 'assets/js/bootstrap-dialog.js');
		array_push($data['script_path'], 'assets/js/common.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.min.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.zh-CN.js');
		array_push($data['script_path'], 'assets/js/resident-repair.js');
		$this->stencil->paint('resident-repair.php', $data);
	}

	/**
	 * 获取家用报修信息
	 */ 
	public function getRepairJson()
	{
		$userInfo =  (array)$_SESSION['userInfo'];
		// $filter = $this->input->post('filter',TRUE);
		$this->load->model('Repair_model');
		$data['draw'] = $this->input->post('draw', TRUE);
		$start = $this->input->post('start', TRUE);
		$length =$this->input->post('length',TRUE);
		$data['extra_search'] = $this->input->post('extra_search', TRUE);
		$filter = $this->input->post('extra_search', TRUE);
		if(isset($filter)){  
			$data['data'] = $this->Repair_model->getSearchInfo($filter,$length,$start);
			$data['recordsFiltered'] = $this->Repair_model->getSearchCount($filter);
			$data['recordsTotal'] = $this->Repair_model->getSearchCount($filter);
		}else{
			$data['data'] = $this->Repair_model->getRepairByCommid($userInfo['commid'],$length,$start);
			$data['recordsFiltered'] =15;
			$data['recordsTotal'] =15;
		}
		
		
		//log_message('INFO','输出JSON：'.json_encode($data,JSON_UNESCAPED_UNICODE));
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
	}

	// 
	//新增/编辑 家用报修
	public function resident_repair_edit()
	{
		$commid = self::validateComm();
		$this->load->model('Repair_model');
		if(!$commid){
			$data['result'] = $this->Repair_model->getTableFields();
		}else{
			$orderNo = $this->uri->segment(4);
			if(!isset($orderNo)){
				self::messageTool('param-err');
				exit();
			}
			$data['result'] = $this->Repair_model->getRepairByRid($orderNo);
		}
		
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->css('bootstrap-datetimepicker.min');
		$this->stencil->css('bootstrap-dialog.min');
		$this->stencil->css('blueimp-gallery/blueimp-gallery.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->load->model('Staff_model');
		$data['staff'] = $this->Staff_model->getAllStaff($commid);
		$data['script_path'] = array();

		// array_push($data['script_path'], 'assets/js/dialog.js');
		array_push($data['script_path'], 'assets/js/bootstrap-dialog.js');
		array_push($data['script_path'], 'assets/js/resident-repair-edit.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.min.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.zh-CN.js');
		array_push($data['script_path'], 'assets/js/blueimp-gallery/blueimp-gallery.min.js');
		array_push($data['script_path'], 'assets/js/public-monitoring-details.js');
		$this->stencil->paint('resident-repair-edit.php', $data);
	}

	//处理完工
	public function resident_repair_proc()
	{
		$commid = self::validateComm();
		$this->load->model('Repair_model');
		if(!$commid){
			self::messageTool('param-err');
			exit();
		}else{
			$orderNo = $this->uri->segment(4);
			if(!isset($orderNo)){
				self::messageTool('param-err');
				exit();
			}
			$res = $this->Repair_model->procRepair($orderNo);
			if($res){
				self::messageTool('modify-ok','home/resident_repair');
			}else{
				self::messageTool('modify-err');
			}
		}	
	}

	//作废订单
	public function repair_order_cancel()
	{
		$orderNo = $this->input->post('orderNo', TRUE);
		$propReply = $this->input->post('propReply', TRUE);
		$data = array(
				'status' =>'4',
				'propReply' => $propReply,
			);
		$this->load->model('Repair_model');
		$res = $this->Repair_model->updateRepairByProp($data,$orderNo);
		if($res){
			//redirect('home/resident_repair');
			self::messageTool('modify-ok','home/resident_repair');
		}else{
			self::messageTool('modify-err');
		}
	}

	//家用报修提交数据
	public function repairSubmit()
	{
		$orderNo = $this->input->post('orderNo', TRUE);
		$forwardTime = $this->input->post('forwardTime', TRUE);
		$orderDesc =$this->input->post('orderDesc', TRUE);
		$staffCode = $this->input->post('staffCode', TRUE);
		$propReply = $this->input->post('propReply', TRUE);
		$status = $this->input->post('status', TRUE);
		if($status=='0'){
			$status = '1';
		}
		$data = array(
			'status' => $status,
			'staffCode' => $staffCode,
			'propReply' => $propReply,
			'orderDesc' => $orderDesc,
			'forwardTime' => $forwardTime
		);	
		$this->load->model('Repair_model');
		$res = $this->Repair_model->updateRepairByProp($data,$orderNo);
		$this->output->set_header('Content-Type: application/json; charset=utf-8');
		echo json_encode($res);
		// if($res){
		// 	//redirect('home/resident_repair');
		// 	self::messageTool('modify-ok','home/resident_repair');
		// }else{
		// 	self::messageTool('modify-err');
		// }
	}

	public function repairDel()
	{
		$orderNo = $this->input->post('orderNoArray', TRUE);
		// var_dump($orderNo[0]);
		$this->load->model('Repair_model');
		$res = $this->Repair_model->delRepair($orderNo[0]);
		// $res = true;
		$this->output->set_header('Content-Type: application/json; charset=utf-8');
		echo json_encode($res);
	}

// ------------------------------------------------------------------------公共监察
	//公共监察
	public function public_monitoring()
	{
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		// $userInfo = (array)$_SESSION['userInfo'];
		// $this->load->model('Monitor_model');
		// $data['result'] = $this->Monitor_model->getPublicMinitors($userInfo['commid'],20,self::getNowPage());
		$data['script_path'] = array();
		array_push($data['script_path'], 'assets/js/public-monitoring.js');
		$this->stencil->paint('public-monitoring.php', $data);
	}

	public function getPublicMonitor()
	{
		$userInfo =  (array)$_SESSION['userInfo'];
		$this->load->model('Monitor_model');
		$data['draw'] = $this->input->post('draw', TRUE);
		$start = $this->input->post('start', TRUE);
		$length =$this->input->post('length',TRUE);
		$data['data'] = $this->Repair_model->getPublicMinitors($userInfo['commid'],$length,$start);
		$data['recordsFiltered'] =15;
		$data['recordsTotal'] =15;
		//log_message('INFO','输出JSON：'.json_encode($data,JSON_UNESCAPED_UNICODE));
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}

	//公共监察详情
	public function public_monitoring_details()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/blueimp-gallery/blueimp-gallery.min.js');
		array_push($data['script_path'], 'assets/js/public-monitoring-details.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->css('blueimp-gallery/blueimp-gallery.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('public-monitoring-details.php', $data);
	}

// ------------------------------------------------------------------------访客通行

	public function department()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/department.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('department.php', $data);
	}

// ------------------------------------------------------------------------访客通行

	public function owner_info()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/owner-info.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('owner-info.php', $data);
	}


// ------------------------------------------------------------------------访客通行
	//访客通行
	public function visitor()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/visitor.js');
		array_push($data['script_path'], 'assets/js/bootstrap-dialog.js');
		array_push($data['script_path'], 'assets/js/common.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.min.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.zh-CN.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->css('bootstrap-datetimepicker.min');
		$this->stencil->css('bootstrap-dialog.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('visitor.php', $data);
	}

// ------------------------------------------------------------------------职员管理
	//职员管理
	public function staff_info()
	{			
		$nowPage = $this->uri->segment(3);
		if(!isset($nowPage)){
			$nowPage = 1;
		}
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else
		{
			$nowSize=$pageSize*($nowPage-1);
		}
		$currentSize = (20 * ($nowPage - 1));
		$userInfo = (array)$_SESSION['userInfo'];
		$commid = $userInfo["commid"];
		$this->load->model('Staff_model');
		$data['result'] = $this->Staff_model->getStaffList($commid,20,$nowSize);
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$data['script_path'] = array();
		array_push($data['script_path'], 'assets/js/staff-info.js');
		$this->stencil->paint('staff-info.php', $data);
	}

	//职员信息编辑
	public function staff_info_edit()
	{
		$commid = $this->uri->segment(3);
		if(isset($commid)){
			$userInfo = (array)$_SESSION['userInfo'];
			if($commid == $userInfo['commid']){	
				$code = $this->uri->segment(4);
				if(isset($code)){
					$this->load->model('Staff_model');
					$data['result'] = $this->Staff_model->getStaffInfo($commid,$code);
					$this->stencil->layout('pages_layout.php');
					$this->stencil->css('font-awesome.min');
					$this->stencil->css('jquery.fileupload');
					$this->stencil->css('jquery.fileupload-ui');
					$this->stencil->slice('sidebar');
					$this->stencil->slice('jsfile');
					$data['script_path'] = [];
					array_push($data['script_path'], 'assets/js/jQuery-File-Upload-9.9.3/jquery.ui.widget.js');
					array_push($data['script_path'], 'assets/js/jQuery-File-Upload-9.9.3/jquery.fileupload.js');
					array_push($data['script_path'], 'assets/js/jQuery-File-Upload-9.9.3/jquery.fileupload-process.js');
					array_push($data['script_path'], 'assets/js/staff-info-edit.js');
					$this->stencil->paint('staff-info-edit.php', $data);
				}else{
					echo "<script>alert('缺失工号!');location.href=history.go(-1);</script>";
				}
			}else{
				echo "<script>alert('您无权访问其他小区信息!');location.href=history.go(-1);</script>";
			}			
		}else{
			//新增操作
		}	
	}

	//职员评论
	public function staff_comment()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/staff-comment.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('staff-comment.php', $data);
	}

	//职员评论详情
	public function staff_comment_details()
	{	
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$data['script_path'] = [];
		$this->load->model('Staff_model');
		$this->stencil->paint('staff-comment-details.php', $data);			
	}

// ------------------------------------------------------------------------小区动态
	//小区动态
	public function community_news()
	{	
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/community-news.js');
		$this->stencil->paint('community-news.php', $data);
	}

	//小区动态详情
	public function community_news_details()
	{
		$data['script_path'] = [];
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('community-news-details.php', $data);
	}

	//小区动态编辑
	public function community_news_actions()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/kindeditor-master/kindeditor-all-min.js');
		array_push($data['script_path'], 'assets/kindeditor-master/lang/zh-CN.js');
		array_push($data['script_path'], 'assets/js/jquery.hotkeys.js');
		array_push($data['script_path'], 'assets/js/community-news-actions.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('community-news-actions.php', $data);
	}

// ------------------------------------------------------------------------办事指南
	//办事指南
	public function guide()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/guide.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('guide.php', $data);
	}

	//办事指南编辑
	public function guide_actions()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/kindeditor-master/kindeditor-all-min.js');
		array_push($data['script_path'], 'assets/kindeditor-master/lang/zh-CN.js');
		array_push($data['script_path'], 'assets/js/jquery.hotkeys.js');
		array_push($data['script_path'], 'assets/js/guide-actions.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('guide-actions.php', $data);	
	}

// ------------------------------------------------------------------------用户统计
	//用户统计
	public function statistics_users_table()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/statistics-users-table.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('statistics-users-table.php', $data);	
	}

	public function statistics_users_chart()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/echarts-2.2.2/build/dist/echarts.js');
		array_push($data['script_path'], 'assets/js/blue-theme.js');
		array_push($data['script_path'], 'assets/js/statistics-users-chart.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('statistics-users-chart.php', $data);	
	}

// ------------------------------------------------------------------------账单统计
	//账单统计
	public function statistics_bill_table()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/statistics-bill-table.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('statistics-bill-table.php', $data);	
	}

	public function statistics_bill_chart()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/echarts-2.2.2/build/dist/echarts.js');
		array_push($data['script_path'], 'assets/js/blue-theme.js');
		array_push($data['script_path'], 'assets/js/statistics-bill-chart.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('statistics-bill-chart.php', $data);	
	}

// ------------------------------------------------------------------------监察统计
	//监察统计
	public function statistics_monitoring_table()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/statistics-monitoring-table.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('statistics-monitoring-table.php', $data);	
	}

	public function statistics_monitoring_chart()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/echarts-2.2.2/build/dist/echarts.js');
		array_push($data['script_path'], 'assets/js/blue-theme.js');
		array_push($data['script_path'], 'assets/js/statistics-monitoring-chart.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('statistics-monitoring-chart.php', $data);		
	}

// ------------------------------------------------------------------------访客统计
	//访客统计
	public function statistics_visitor_table()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/statistics-visitor-table.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('statistics-visitor-table.php', $data);	
	}

	public function statistics_visitor_chart()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/echarts-2.2.2/build/dist/echarts.js');
		array_push($data['script_path'], 'assets/js/blue-theme.js');
		array_push($data['script_path'], 'assets/js/statistics-visitor-chart.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('statistics-visitor-chart.php', $data);	
	}

// ------------------------------------------------------------------------反馈建议	

	public function feedback()
	{
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->css('bootstrap-dialog.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$data['script_path'] = array();
		array_push($data['script_path'], 'assets/js/bootstrap-dialog.js');
		array_push($data['script_path'], 'assets/js/common.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.min.js');
		array_push($data['script_path'], 'assets/js/bootstrap-datetimepicker.zh-CN.js');
		array_push($data['script_path'], 'assets/js/feedback.js');
		$this->stencil->paint('feedback.php', $data);	
	}

// ------------------------------------------------------------------------反馈建议编辑

	public function feedback_edit() 
	{
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->css('blueimp-gallery/blueimp-gallery.min');
		$data['script_path'] = array();
		array_push($data['script_path'], 'assets/js/blueimp-gallery/blueimp-gallery.min.js');
		array_push($data['script_path'], 'assets/js/public-monitoring-details.js');
		$this->stencil->paint('feedback_edit.php', $data);	
	}


// ------------------------------------------------------------------------系统配置
	//操作日记
	public function operation_log()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/operation-log.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('operation-log.php', $data);	
	}

	//数据匹配
	public function operation_data_match()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/jQuery-File-Upload-9.9.3/jquery.ui.widget.js');
		array_push($data['script_path'], 'assets/js/jQuery-File-Upload-9.9.3/jquery.fileupload.js');
		array_push($data['script_path'], 'assets/js/jQuery-File-Upload-9.9.3/jquery.fileupload-process.js');
		array_push($data['script_path'], 'assets/js/operation-data-match.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->css('jquery.fileupload');
		$this->stencil->css('jquery.fileupload-ui');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('operation-data-match.php', $data);	
	}

	//数据匹配编辑
	public function operation_data_match_edit()
	{
		$data['script_path'] = [];
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('operation-data-match-edit.php', $data);
	}

	//参数设置
	public function operation_config()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/operation-config.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('operation-config.php', $data);	
	}

	//参数设置编辑
	public function operation_config_edit()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/operation-config.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('operation-config-edit.php', $data);	
	}

	public function operation_sys_backup()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/operation-sys-backup.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('operation-sys-backup.php', $data);	
	}

	//用户管理
	public function user_manage()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/user-manage.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('user-manage.php', $data);
	} 

	//用户权限设置
	public function user_manage_edit()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/user-manage.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('user-manage-edit.php', $data);
	}

	//角色管理
	public function role_manage()
	{
		$data['script_path'] = [];
		array_push($data['script_path'], 'assets/js/role-manage.js');
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('role-manage.php', $data);
	}

	//角色管理编辑
	public function role_manage_edit()
	{
		$data['script_path'] = [];
		$this->stencil->layout('pages_layout.php');
		$this->stencil->css('font-awesome.min');
		$this->stencil->slice('sidebar');
		$this->stencil->slice('jsfile');
		$this->stencil->paint('role-manage-edit.php', $data);
	}

	//消息推送
	public function get_push_msg()
	{
		$get_push_msg = $this->input->get('get_push_msg', TRUE);
		//if get_push_msg = true -> sql
		$data['num'] = 2;
		$data['type'] = 'resident-repair';
		$this->output->set_header('Content-Type: application/json; charset=utf-8');
		echo json_encode($data);
	}

	//退出登录
	public function logout()
	{
		unset($_SESSION["userInfo"]);
		redirect("./");
	}

// ------------------------------------------------------------------------公共方法
    /**
     * 验证是否是当前小区
     * @Date 2015-06-24 17:37:28
     * @author npctest
     */ 
	public function validateComm()
	{
		$commid = $this->uri->segment(3);
		if(isset($commid))
		{
			$userInfo = (array)$_SESSION['userInfo'];
			if($commid == $userInfo['commid']){
				return $commid; //返回当前小区编号
			}else{
				self::messageTool('permission-err');  //无权限信息提示
				exit();
			}
		}else{
			return false; //不存在小区编号
		}		
	}

	/**
	 * 分页相关信息
	 * @Date 2015-06-24 17:55:14
	 * @author npctest
	 * 
	 * @Param pageSize ->每页条数 默认20条
	 */ 
	public function getNowPage($pageSize = 20)
	{
		$nowPage = $this->uri->segment(3);
		if(!isset($nowPage)){
			$nowPage = 1;
		}
		if(intval($nowPage) <= 1){
			$nowSize = 0;
		}else{
			$nowSize = $pageSize*($nowPage-1);
		}
		return $nowSize;
	}

	/**
	 * 公共提示Message
	 * @Date 2015-06-24 17:54:08
	 * @auhtor npctest
	 */ 
	public function messageTool($type,$url="")
	{
		$msg = "";
		$target ="";
		switch ($type) {
			case 'add-err':
				$msg = "添加失败！";
				break;
			case 'add-ok':
				$msg = "添加成功！";
				break;
			case 'del-err':
				$msg = "删除失败！";
				break;
			case 'del-ok':
				$msg = "删除成功！";
				break;
			case 'modify-err':
				$msg = "修改失败！";
				break;
			case 'modify-ok':
				$msg = "修改成功！";
				break;
			case 'permission-err':
				$msg = "对不起，您无权访问！";
				break;
			case 'param-err':
				$msg = "参数不准确，请检查数据！";
				break;
			default:
				$msg = "未知错误！";
				break;
		}
		if($url==""){
			$target = "history.go(-1);";
		}else{
			$target = "location.href='".base_url().$url."';";
		}
		log_message('INFO','输出：'."<script>alert('".$msg."');".$target."</script>");
		echo "<script>alert('".$msg."');".$target."</script>";
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */