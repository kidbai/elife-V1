<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	* 
	*/
	class Test extends MY_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('City_Model');
			$this->load->model('Owner_Model');
		}

		public function index()
		{
			$this->load->library('common');
			$res=$this->common->get_Clinet_Ip();
			echo $res;
		}

		public function testPic()
		{
			$test=array('a'=>array('aa'=>'哈哈','bb'=>'呵呵'),'b'=>array('cc'=>'哒哒','dd'=>'默默'));
			$data=array('http://www.baidu.com','http://www.sina.com','http://www.163.com');
			
			foreach($test as $key=>$val)
			{
				$test[$key]['picUrl']=$data;
				$res=json_encode($test,JSON_UNESCAPED_UNICODE);
				echo $res;
			}
			
		}

		public function getAllCity()
		{
			#$this->load->model('City_Model');
			$data['result']=$this->City_Model->queryAllCity(10,0);
			foreach ($data['result'] as $item){
				echo var_dump($item);
				echo "<br/>";
			}
		}

		public function getAllRoom()
		{
			$commid=$_GET["commid"];
			$this->load->model('Community_Model');
			$back=$this->Community_Model->getAllRoom($commid);

			if(count($back)>0)
			{
				$res['code']='1';
				$res['msg']='拉取成功';
				$res['data']=$back;
			}
			else
			{
				$res['code']='0';
				$res['msg']='无该小区信息';
			}
			$temp=self::_findChildren($back,0);
			var_dump($temp);
			$res=json_encode($res,JSON_UNESCAPED_UNICODE);
			echo $res;
		}

		 function _findChildren($list, $p_id){    //数据层级化，
          $r = array();
          foreach($list as $id=>$item){
            if($item['building'] == $p_id) {
                   $length = count($r);
                  $r[$length] = $item;
                  if($t = $this->_findChildren($list, $item['roomdesc']) ){
                      $r[$length]['children'] = $t;
                  }                
            }
          }
          return $r;
   		 } 

		/**
		 * 创建父节点树形数组
		 * 参数
		 * $ar 数组，邻接列表方式组织的数据
		 * $id 数组中作为主键的下标或关联键名
		 * $pid 数组中作为父键的下标或关联键名
		 * 返回 多维数组
		 **/
		function find_parent($ar, $id='id', $pid='pid') { 
		  foreach($ar as $v) $t[$v[$id]] = $v;
		  foreach ($t as $k => $item){
		    if( $item[$pid] ){
		      if( ! isset($t[$item[$pid]]['parent'][$item[$pid]]) )
		         $t[$item[$id]]['parent'][$item[$pid]] =& $t[$item[$pid]];
		          $t[$k]['reference'] = true;
		    }
		  } 
		  return $t;
		}

		/**
		 * 创建子节点树形数组
		 * 参数
		 * $ar 数组，邻接列表方式组织的数据
		 * $id 数组中作为主键的下标或关联键名
		 * $pid 数组中作为父键的下标或关联键名
		 * 返回 多维数组
		 **/
		function find_child($ar, $id='id', $pid='pid') {
		  foreach($ar as $v) $t[$v[$id]] = $v;
		  foreach ($t as $k => $item){
		    if( $item[$pid] ) {
		      $t[$item[$pid]]['child'][$item[$id]] =& $t[$k];
		      $t[$k]['reference'] = true;
		    }
		  }
		  return $t;
		}

		public function addCity()
		{
			$data=array(
						'code'=>'CZ',
						'name'=>'常州',
						'remark'=>'重工业二级城市',
						'land_id'=>1,
						'property_id'=>1
						);
			
			$res=$this->City_Model->addCity($data);
			echo $res;
		}

		public function updateCity()
		{
			echo "222";
			$data=array(
						'code'=>'CZ',
						'name'=>'常州',
						'remark'=>'服务业城市',
						'land_id'=>1,
						'property_id'=>1
						);
			
			$res=$this->City_Model->updateCity($data,5);
			echo $res;
		}

		public function delCity()
		{
			$res=$this->City_Model->delCityById(1);
			echo $res;
		}

		public function VerifyAccount()
		{
			$data='{"telNo":"15390442829","password":"hexinyi666888"}'; 
			var_dump(json_decode($data, true)); 
			$res=json_decode($data, true);
			#echo $res['telNo'];
			#echo $res['password'];
			#echo md5($res['password']);
			$json=array('code'=>1,'msg'=>'登录成功','token_id'=>'sxf2js923sdc291wsd');
			#$json = json_encode($array);
			$this->load->library('common');
			$this->common->arrayRecursive($json,'urlencode', true);
			$json = json_encode($json);
			echo urldecode($json);
			#$res=$this->Owner_Model->checkAccount()
		}
	}
	
?>