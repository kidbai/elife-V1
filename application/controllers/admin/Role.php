<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		
	}

	/**
	 * [addRole 根据组ID]
	 * @param string $value [description]
	 */
	public function addRole($data,$gid)
	{
		$query=$this->db->insert('el_role', $data,$gid);
		return $query;
	}

	/**
	 * [updateRole description]
	 * @param  [type] $data [description]
	 * @param  [type] $rid  [description]
	 * @return [type]       [description]
	 */
	public function updateRole($data,$rid)
	{
		$result=$this->db->update('el_role', $data, $rid);
	}
?>