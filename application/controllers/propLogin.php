<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class propLogin extends MY_Controller {

	public function __construct()
	{
		parent::__construct('web','propLogin');
		$this->stencil->slice('head'); //加载head部分所需资源
		//$data = array();
		// if(isset($_SESSION['userInfo'])){
		// 	$userInfo =  (array)$_SESSION['userInfo'];
		// 	$data['userInfo'] = self::getUserInfo($userInfo); //获取登录信息及权限.
		// }
		// $this->stencil->data($data);
		$this->stencil->slice('header'); //加载页面head部分页面内容
	}

	/**
	 * 登录功能
	 * @Date 2015-06-27 16:16:11
	 * @author npctest
	 * 
	 */ 
	public function index()
	{
		if(isset($_SESSION['userInfo'])){
			$userInfo = (array)$_SESSION['userInfo'];
			redirect('home/dashboard');
		}else{
			$this->stencil->layout('admin_login_layout');
			$this->stencil->css('font-awesome.min');
			$this->stencil->paint('login.php');
		}
	}


	//生成验证码
	public function securimage()
	{
		$this->load->library('validatecode');
		$img = $this->validatecode->doimg();
		log_message('INFO','验证码：'.$this->validatecode->getCode());
		return $img;
	}

	//预加载界面-验证登录
	public function loader()
	{
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);
		$code = $this->input->post('validatecode', TRUE);
		$this->load->library('common');
		// $res = $this->common->verifyCode($code);
		$res = true;
		if($res){
			$this->load->model('Prop_model');
			$result = $this->Prop_model->validateAccount($username,md5($password));
			if($result){
				$userInfo = array(
						'userName' => $result['account'],
						'type' => $result['type'],
						'name' => $result['name'],
						'commid' => $result['comm_id']
					);
				$_SESSION['userInfo'] = $userInfo;
				// $this->stencil->layout('loader_layout.php');
				// $this->stencil->css('font-awesome.min');
				// $this->stencil->slice('jsfile');
				// $this->stencil->paint('loader.php');
				$this->output->set_header('Content-Type: application/json; charset=utf-8');
				echo json_encode('1');
			}else{
				$this->output->set_header('Content-Type: application/json; charset=utf-8');
				echo json_encode('2');
			}
		}else{
			$this->output->set_header('Content-Type: application/json; charset=utf-8');
			echo json_encode('3');
		}	
	}
}

/* End of file propLogin.php */
/* Location: ./application/controllers/propLogin.php */