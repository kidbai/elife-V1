<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	* 
	*/
	class Test extends MY_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('City_Model');
			$this->load->model('Owner_Model');
		}

		public function index()
		{
			$this->load->library('common');
			$res=$this->common->get_Clinet_Ip();
			echo $res;
		}

		public function getAllCity()
		{
			#$this->load->model('City_Model');
			$data['result']=$this->City_Model->queryAllCity(10,0);
			foreach ($data['result'] as $item){
				echo var_dump($item);
				echo "<br/>";
			}
		}

		public function addCity()
		{
			$data=array(
						'code'=>'CZ',
						'name'=>'常州',
						'remark'=>'重工业二级城市',
						'land_id'=>1,
						'property_id'=>1
						);
			
			$res=$this->City_Model->addCity($data);
			echo $res;
		}

		public function updateCity()
		{
			echo "222";
			$data=array(
						'code'=>'CZ',
						'name'=>'常州',
						'remark'=>'服务业城市',
						'land_id'=>1,
						'property_id'=>1
						);
			
			$res=$this->City_Model->updateCity($data,5);
			echo $res;
		}

		public function delCity()
		{
			$res=$this->City_Model->delCityById(1);
			echo $res;
		}
		// ------------------------------------------------------------------------登录部分API
		public function verifyAccount()
		{
			$telNo=$this->input->post('telNo', TRUE);
			$pwds=$this->input->post('password', TRUE);
			$pwds=md5($pwds);
			$this->load->model('Owner_Model');
			$data=array('mobile'=>$telNo,'passwords'=>$pwds);
			$res=$this->Owner_Model->checkAccount($data);
			$this->load->library('common');
			$json=array('code'=>0,'msg'=>'登录失败');
			if($res==1)
			{
				$json=array('code'=>1,'msg'=>'登录成功','token_id'=>'sxf2js923sdc291wsd');	
			}
			#$this->common->arrayRecursive($json,'urlencode', true);
			$json = json_encode($json);
			#echo urldecode($json);
			#return urldecode($json);
			echo $json;
			return $json;
		}

		public function verifyAccountByParams()
		{
			$telNo=$this->input->get('telNo', TRUE);
			$pwds=$this->input->get('password', TRUE);
			$pwds=md5($pwds);
			$this->load->model('Owner_Model');
			$data=array('mobile'=>$telNo,'passwords'=>$pwds);
			$res=$this->Owner_Model->checkAccount($data);
			$this->load->library('common');
			$json=array('code'=>0,'msg'=>'登录失败');
			if($res==1)
			{
				$json=array('code'=>1,'msg'=>'登录成功','token_id'=>'sxf2js923sdc291wsd');	
			}
			$this->common->arrayRecursive($json,'urlencode', true);
			$json = json_encode($json);
			echo urldecode($json);
			return urldecode($json);		
		}

		// ------------------------------------------------------------------------注册部分API
		public function getCity()
		{
			$this->load->model('City_Model');
			$res['citys']=$this->City_Model->queryAllCity();
			foreach ($res['citys'] as $item) {
				echo $item->name;
			}

			$this->load->library('common');
			$this->common->arrayRecursive($res,'urlencode', true);
			$res = json_encode($res);
			echo urldecode($res);
			return urldecode($res);
		}

		public function sendSMS()
		{
			$this->load->library('common');
			$this->common->sendYunPianSMS('13064302555');
		}

		public function getSMS()
		{
			$this->load->library('common');
			$this->common->getReturnSMS();
		}

		public function testArray()
		{
			
		}
	}
	
?>