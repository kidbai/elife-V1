<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	* 3Elife生活APP端接口
	* @Date 2015-06-15 00:52:02
	* @author npctest 
	*/
class Api extends MY_Controller
{
//----------------------------------------------------------------------------构造函数
	function __construct()
	{
		parent::__construct('app','Api');	
	}

//----------------------------------------------------------------------------index默认获取IP地址
	public function index()
	{
		$res = $this->common->get_Clinet_Ip();
		echo $res;
	}

//----------------------------------------------------------------------------登录部分API
	/**
	 * 验证APP登录 -> post
	 * @Date 2015-06-14 20:27:13
	 * @author npctest
	 * 
	 * @param telNo -> 手机号码
	 * @param password -> 密码
	 * @param equimentNo -> UUID
	 * @param mobileType -> 手机类型
	 * 
	 * @return JSON -> 数据JSON对象
	 */
	public function verifyAccountByParams()
	{
		$accept = self::filterPostDataToArray();
		$telNo = $accept['telNo'];
		//log_message('INFO',$telNo);
		$pwds = $accept['password'];
		//log_message('INFO',$pwds);
		$unique = $accept['equimentNo']; 
		//log_message('INFO',$unique);
		$mobileType=$accept['mobileType']; //手机类型
		$pwds = md5($pwds);
		$this->load->model('Owner_model');
		$res = $this->Owner_model->checkAccount($telNo,$pwds);   //验证帐号与密码是否匹配
		if(isset($res)){
			$back = $this->common->procAppToken($telNo,$unique,$mobileType);  //获取服务器token
			if($back){
				$res['token_id']=$back;
			}else{
				self::errorAction();
				exit();
			}
			self::successAction($res);	
		}else{
			self::errorAction();
		}
	}

//----------------------------------------------------------------------------注册部分API
	
	/**
	 *获取城市列表 -> HTTP GET
	 * @Date 2015-06-14 20:21:29
	 * @author npctest
	 */ 
	public function getCity()
	{
		$this->load->model('City_model');
		$data = $this->City_model->queryAllCity();
		self::successAction($data);
	}

	/**
	 * 获取小区列表 -> HTTP POST
	 * @Date 2015-06-14 20:19:06
	 * @author npctest
	 * 
	 * @param cityid->城市ID
	 */ 
	public function getCommunity()
	{
		$accept = self::filterPostDataToArray();
		$cid = $accept['cityid'];
		$this->load->model('Community_model');
		$data = $this->Community_model->getAllCommunityByCid($cid);
		self::successAction($data);
	}

	/**
	 *获取小区房间列表 -> HTTP POST
	 * @Date 2015-06-14 20:23:48 
	 * @author npctest
	 * 
	 * @param commid->小区ID
	 */
	public function getRoom()
	{
		$accept = self::filterPostDataToArray();
		$commid = $accept['commid'];
		$this->load->model('Community_model');
		$data = $this->Community_model->getAllRoom($commid);
		self::successAction($data);
	}

	/**
	 * 获取某房间下业主电话 -> HTTP POST
	 * @Date 2015-06-14 20:29:18
	 * @author npctest
	 * 
	 * @param roomid->房间唯一编号
	 */ 
	public function getParMobi()
	{
		$accept = self::filterPostDataToArray();
		$roomid = $accept["roomid"];
		$this->load->model('Owner_model');
		$data = $this->Owner_model->getMobiles($roomid);
		self::successAction($data);
	}

	/**
	 * 发送短信 -> HTTP POST
	 * @Date 2015-06-14 20:32:33
	 * @author npctest
	 * 
	 * @param token_id->系统内部固定token值:sxf2js923sdc291wsd
	 * @param telNo->接受短信验证码
	 * 
	 */ 
	public function sendSMS()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept["token_id"];
		$to = $accept["telNo"];
		log_message('error',$to);
		if($token_id == "sxf2js923sdc291wsd")
		{
			$this->common->sendYunPianSMS($to);
		}else
		{
			self::refuseAction();
		}
	}

	/**
	 * 注册用户接口 -> HTTP POST
	 * @Date 2015-06-15 00:23:00
	 * @author npctest
	 * 
	 * @param name -> 名字
	 * @param roleid -> 角色ID
	 * @param roomdesc -> 房间号唯一标识
	 * @param telNo -> 电话号码
	 * @param pwd -> 密码
	 */ 
	public function regAccount()
	{
		$accept = self::filterPostDataToArray();
		$name = $accept['name'];
		$type_id = $accept['roleid'];
		$roomdesc = $accept['roomdesc'];
		$mobile = $accept['telNo'];
		$password = $accept['pwd'];
		if(strlen($password)<6&&strlen($password>18)){
			$res['code'] = "0";
			$res['msg'] = "密码必须6-18位之间";
			$res['serTime'] = self::getDateTime();
			$res = json_encode($res,JSON_UNESCAPED_UNICODE);
			echo $res;
			exit();
		}
		$this->load->model('Owner_model');
		if(!isset($name)){
			$contact = $this->Owner_model->getOwnerName($roomdesc,$mobile);
			$name = $contact['name'];
		}

		$data=array(
			'username'=>$name,
			'owner_code'=>'CD'.$this->common->getMillisecond(),
			'room_id' => $roomdesc,
			'type' => $type_id,
			'mobile' => $mobile,
			'passwords' => md5($password),
			'reg_time' => self::getDateTime()
			);
	
		$back = $this->Owner_model->addAccount($data);
		if(count($back)>0){
			$result['code'] = '1';
			$result['msg'] = '注册成功！';
			$result['serTime'] = self::getDateTime();
		}else{
			$result['code'] = '0';
			$result['msg'] = '注册失败！';
			$result['serTime'] = self::getDateTime();
		}
		$result=json_encode($result,JSON_UNESCAPED_UNICODE);
		echo $result;
	}

// ---------------------------------------------------------------------------轮播图片
	public function get($value='')
	{
		# code...
	}

//----------------------------------------------------------------------------个人信息
	/**
	 * 获取个人信息 -> HTTP POST getPersonInfo
	 * @Date 2015-06-15 00:35:15
	 * @author npctest
	 * 
	 * @param token_id -> token
	 * @param ownerUser -> 用户唯一标识
	 */ 
	public function getPersonInfo()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept["token_id"];
		self::verifyToken($token_id);
		$ownerUser=$accept["ownerUser"];
		$this->load->model('Owner_model');
		$data=$this->Owner_model->getOwnerInfo($ownerUser);
		self::successAction($data);
	}

	/**
	 * 禁止某子账号用户 cancelAccout
	 * @Date 2015-06-22 18:07:53
	 * @author npctest
	 * 
	 * @param token_id
	 * @param telNo -> 目标帐号的手机号
	 * @param commid -> 小区编号
	 */ 
	public function cancelAccout()
	{
		$accpet = self::filterPostDataToArray();
		$token_id = $accept['token_id'];
		self::verifyToken($token_id);
		$mobile = $accept['telNo'];
		$commid = $accept['commid'];
		$this->load->model('Owner_model');
		$res = $this->Owner_model->cancelAccout($mobile,$commid);
		if($res){
			self::successAction();
		}else{
			self::errorAction();
		}
	}

	/**
	 * 注销帐号/切换用户 -> HTTP POST
	 * @Date 2015-06-15 01:42:29
	 * @author npctest
	 * 
	 * @param token_id -> token
	 * @param mobile ->用户手机号码
	 */ 
	public function offAccount()
	{
		$accept =  self::filterPostDataToArray();
		$token_id = $accept['token_id'];
		self::verifyToken($token_id);
		$mobile = $accept['mobile'];
		$this->load->model('Apptoken_model');
		$res = $this->Apptoken_model->logout($mobile);
		if($res){
			self::successAction();
		}else{
			self::errorAction();
		}
	}

	/**
	 * 修改密码 -> HTTP POST
	 * @Date 2015-06-15 01:45:38
	 * @author npctest
	 * 
	 * @param token_id -> token
	 * @param ownerUser -> 用户唯一标识
	 * @param mobile -> 手机号码
	 * @param verifyCode 短信验证码
	 */ 
	public function modifyPwd()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept['token_id'];
		self::verifyToken($token_id);
		$verifyCode = $accept['verifyCode'];
		$this->load->model('Sms_model');
		$res = $this->Sms_model->queryCode($verifyCode);
		$ownerUser = $accept['ownerUser'];
		$mobile = $accept['mobile'];
		$newPwd = $accept['newPwd'];
	}

//----------------------------------------------------------------------------访客通行

	/**
	 * 根据用户编号读取访客记录
	 * @Date 2015-06-15 17:50:05
	 * @author npctest
	 * 
	 * @param token_id
	 * @param pageSize -> 条数
	 * @param nowPage -> 当前页
	 * @param ownerUser -> 用户唯一标识 
	 */
	public function getVisitors()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept['token_id'];
		self::verifyToken($token_id);
		$res=array();
		$pageSize=intval($accept['pageSize']); //条数
		$nowPage=intval($accept['nowPage']);  //当前页
		$ownerUser = $accept['ownerUser'];
		$nowSize=0;
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else
		{
			$nowSize=$pageSize*($nowPage-1);
		}

		$this->load->model('Visitor_model');
		try
		{
			$back=$this->Visitor_model->queryVisitorsByOwnerUser($ownerUser,$pageSize,$nowSize);
			$res=self::successAction($back);
		}
		catch(Exception $e)
		{
			$res=self::errorAction();
		}
	}

	/**
	 * 撤销访客通行(数据库真实删除)
	 * @Date 2015-06-15 17:51:25
	 * @author npctest
	 * 
	 * @param token_id
	 * @param sid -> 访客记录编号
	 */ 
	public function cancelVisitor()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept['token_id'];
		self::verifyToken($token_id);
		$res=array();
		$vid=$accept['sid'];
		$this->load->model('Visitor_model');
		$back=$this->Visitor_model->cancelVisitorBySid($vid);
		if($back>0)
		{
			$res=self::successAction();
		}else{
			$res=self::errorAction();
		}
	}

	/**
	 * 删除访客通行记录(更新数据库状态)
	 * @Date 2015-06-15 17:53:01
	 * @author npctest
	 * 
	 * @param token_id
	 * @param sid -> 访客记录编号
	 */
	public function delVisitor()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept['token_id'];
		self::verifyToken($token_id);
		$vid=$accept['sid'];
		$this->load->model('Visitor_model');
		$strWhere=array('sid'=>$vid);
		$data=array('status'=>'9');
		$back=$this->Visitor_model->delVisitorBySid($data,$strWhere);
		if($back>0)
		{
			$res=self::successAction();
		}else{
			$res=self::errorAction();
		}
	}

	/**
	 * 添加访问通行
	 * @Date 2015-06-15 17:54:17
	 * @author npctest
	 * 
	 * @param token_id
	 * @param timescale -> 访问时间段
	 * @param visitor -> 访客信息
	 * @param remarks -> 访客备注
	 * @param roomid -> 房间号唯一标识
	 * @param ownerUser ->用户唯一标识
	 */ 
	public function addVisitor()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept['token_id'];
		self::verifyToken($token_id);
		$roomid = $accept['roomid'];
		$ownerUser = $accept['ownerUser'];
		$timeScale=$accept['timescale'];
		$visitor=$accept['visitor'];
		$remarks=$accept['remarks'];
		$data=array(
					'name'=>$visitor,
					'timeScale'=>$timeScale,
					'remarks'=>$remarks,
					'status'=>0,
					'roomid'=>$roomid,
					'owner_User'=>$ownerUser,
					'addTime'=>self::getDateTime()
			);
		$this->load->model('Visitor_model');
		$back=$this->Visitor_model->addVisitor($data);
		if($back>0)
		{
			$res=self::successAction();
		}else
		{
			$res=self::errorAction();
		}
		
	}

//----------------------------------------------------------------------------家用报修

	/**
	 * 获取业主下的报修信息 HTTP POST
	 * @Date 2015-05-21 11:12:59
	 * @author npctest
	 * 
	 * @param token_id -> token
	 * @param pageSize -> 条数
	 * @param nowPage ->当前页数
	 * @param status -> 状态 0-待处理 1-处理中 2-已完成(待评价) 3-已完成(已评价) 4-无效 5-用户撤销
	 */
	public function getRepairs()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept["token_id"];
		self::verifyToken($token_id);
		$pageSize=intval($accept['pageSize']); //条数
		$nowPage=intval($accept['nowPage']);  //当前页	
		$ownerUser=$accept['ownerUser'];
		if(isset($accept['status'])){
			$status = $accept['status'];
		}else{
			$status = 0;
		}
		$nowSize=0;
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else
		{
			$nowSize=$pageSize*($nowPage-1);
		}

		$this->load->model('Repair_model');
		try
		{
			$back=$this->Repair_model->getRepairsByOwnerUser($ownerUser,$pageSize,$nowSize,$status);
			self::successAction($back);
		}
		catch(Exception $e)
		{
			self::exceptionAction();
			exit();
		}
	}

	/**
	 * 撤销报修信息(数据库删除操作) HTTP POST
	 * @Date 2015-06-15 11:25:45
	 * @author npctest
	 * 
	 * @param token_id
	 * @param orderNo -> 工单号
	 */ 
	public function cancelRepair()
	{
		$accept = self::filterPostDataToArray();
		self::verifyToken($accept['token_id']);
		$rid=$accept["orderNo"];
		$this->load->model('Repair_model');
		$back=$this->Repair_model->delRepair($rid);
		if($back){
			self::successAction();
		}else{
			self::errorAction();
		}
	}

	/**
	 * 删除报修记录(数据库只是更新状态) HTTP POST
	 * @Date 2015-06-15 11:27:37
	 * @author npctest
	 * 
	 * @param orderNo -> 工单
	 */ 
	public function delRepair()
	{
		$accept = self::filterPostDataToArray();
		self::verifyToken($accept['token_id']);
		$rid=$accept["orderNo"];
		$this->load->model('Repair_model');
		$back=$this->Repair_model->updateRepair($rid);
		if($back){
			self::successAction();
		}else{
			self::errorAction();
		}
	}

	/**
	 * 新增报修信息
	 * @Date 2015-06-15 11:29:12
	 * @author npctest
	 * 
	 * @param token_id
	 * @param orderDesc -> 报修信息描述
	 * @param PicBuffer -> 报修上传图片(可选参数)|与device绑定
	 * @param device -> 设备标识，例如:ios/android(可选参数)|与PicBuffer绑定
	 * @param roomid -> 房间号唯一标识
	 * @param ownerUser ->用户唯一标识
	 * @param forwardTime -> 预约时间
	 */
	public function addRepair()
	{
		$result = $this->common->uploadImage('repair');
		if($result){
			$picNum = $result;
		}else{
			$picNum = "";
		}
		$accept = self::filterPostImageToArray();
		self::verifyToken($accept['token_id']);
		$orderDesc=$accept['orderDesc'];		
		$roomid = $accept['roomid'];
		$ownerUser = $accept['ownerUser'];
		$forwardTime = $accept['forwardTime'];
		$this->load->model('Repair_model');
		$dataModel = array(
			'orderNo'=>$this->common->getMillisecond(),
			'roomid'=>$roomid,
			'ownerUser'=>$ownerUser,
			'orderDesc'=>$orderDesc,
			'forwardTime'=>$forwardTime,
			'repairPic'=>$picNum,
			'status'=>0,
			'addTime'=>self::getDateTime()
			);
		$res = $this->Repair_model->addRepair($dataModel);
		if($res){
			self::successAction();
		}else{
			self::errorAction();
		}
		
	}

	/**
	 * 添加报修评价 HTTP POST
	 * @Date 2015-06-15 11:36:15
	 * @author npctest
	 * 
	 * @param token_id
	 * @param orderNo -> 工单
	 * @param comment -> 评价内容
	 * @param serviceStar -> 服务星级评价(int)
	 * @param techStar -> 技术星级评价(int)
	 * @param PicBuffer -> 上传图片(可选参数)|与device绑定
	 * @param device -> 设备标识，例如：ios/android |与PicBuffer绑定
	 */
	public function addRepairComment()
	{
		$result = $this->common->uploadImage('repairComment');
		if($result){
			$picNum = $result;
		}else{
			$picNum = "";
		}
		$accept = self::filterPostImageToArray();
		self::verifyToken($accept['token_id']);
		$orderNo = $accept['orderNo'];
		$comment = $accept['comment'];
		$serviceStar = $accept['serviceStar'];
		$techStar = $accept['techStar'];
		$data = array(
			'comment' => $comment,
			'serviceStar' => $serviceStar,
			'techStar' => $techStar,
			'replyPic' => $picNum,
			'status' => 3,
			'replyTime' => self::getDateTime()
			);
		$this->load->model('Repair_model');
		$res = $this->Repair_model->addRepairComment($data,$orderNo);
		if($res){
			self::successAction();
		}else{
			self::errorAction();
		}
	}

//----------------------------------------------------------------------------公共监察
	/**
	 * 获取所有公共监察列表信息 HTTP POST
	 * @Date 2015-06-15 10:02:10
	 * @author npctest
	 * 
	 * @param token_id 
	 * @param pageSize -> 每页条数
	 * @param nowPage -> 当前页数
	 */
	public function getPublicMonitor()
	{
		$accept = self::filterPostDataToArray();
		self::verifyToken($accept['token_id']);
		$pageSize = $accept["pageSize"];
		$nowPage = $accept["nowPage"];
		$commid = $accept['commid'];
		if(intval($nowPage)<=1){
			$nowSize=0;
		}else{
			$nowSize=$pageSize*($nowPage-1);
		}

		$this->load->model('Monitor_model');
		try
		{
			$back=$this->Monitor_model->getPublicMinitors($commid,$pageSize,$nowSize);
			self::successAction($back);
		}
		catch(Exception $e)
		{
			self::exceptionAction();
			exit();
		}
	}

	/**
	 * 获取个人的公共监察信息 HTTP POST
	 * @Date 2015-06-15 10:08:45
	 * @author npctest
	 * 
	 * @param token_id
	 * @param ownerUser -> 用户唯一标识
	 * @param pageSize -> 每页条数
	 * @param nowPage -> 当前页数
	 */
	public function getMyMonitor()
	{
		$accept = self::filterPostDataToArray();
		self::verifyToken($accept['token_id']);
		$pageSize=$accept["pageSize"];
		$nowPage=$accept["nowPage"];
		$ownerUser=$accept['ownerUser'];
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else
		{
			$nowSize=$pageSize*($nowPage-1);
		}

		$this->load->model('Monitor_model');
		try
		{
			$back=$this->Monitor_model->getMonitorsByOwnerUser($ownerUser,$pageSize,$nowSize);
			self::successAction($back);
		}
		catch(Exception $e)
		{
			self::exceptionAction();
			exit();
		}
	}

	/**
	 * 获取公共监察评论 HTTP POST
	 * @Date 2015-06-15 10:20:01
	 * @author npctest
	 * 
	 * @param token_id
	 * @param pageSize -> 每页条数
	 * @param nowPage -> 当前页数
	 * @param monitorId -> 监察信息编号
	 */
	public function getMonitorComment()
	{
		$accept = self::filterPostDataToArray();
		self::verifyToken($accept['token_id']);
		$pageSize=$accept["pageSize"];
		$nowPage=$accept["nowPage"];
		$mid=$accept["monitorId"];
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else
		{
			$nowSize=$pageSize*($nowPage-1);
		}

		$this->load->model('Monitor_model');
		try
		{
			$back=$this->Monitor_model->getMonitorCommentBySid($mid,$pageSize,$nowSize);
			self::successAction($back);
		}
		catch(Exception $e)
		{
			self::exceptionAction();
			exit();
		}
	}

	/**
	 * 添加新的公共监察 HTTP POST
	 * @Date 2015-06-15 10:26:01
	 * @author npctest
	 * 
	 * @param token_id
	 * @param roomid -> 房号唯一标识
	 * @param commid -> 小区编号
	 * @param ownerUser -> 用户唯一标识
	 * @param monitorDesc -> 监察内容
	 * @param PicBuffer -> 可选参数/图片附件
	 * @param categoryId -> 监察分类
	 */
	public function addMonitor()
	{
		$result = $this->common->uploadImage('monitor');
		if($result){
			$picNum = $result;
		}else{
			$picNum = "";
		}
		$accept = self::filterPostImageToArray();
		self::verifyToken($accept['token_id']);
		$roomid = $accept['roomid'];
		$commid = $accept['commid'];
		$ownerUser = $accept['ownerUser'];
		$monitorDesc = $accept['monitorDesc'];
		$categoryId = $accept['categoryId'];
		$data=array(
				'owner_User' => $ownerUser,
				'roomid' => $roomid,
				'commid' => $commid,
				'content' => $monitorDesc,
				'monitorPicId' => $picNum,
				'addTime' => self::getDateTime(),
				'status' => 1, //默认状态
				'categoryid'=> $categoryId
			);
		try{
			$this->load->model('Monitor_model');
			$back=$this->Monitor_model->addNewMonitor($data);
			if($back>0)
			{
				self::successAction();
			}else{
				self::errorAction();
			}
		}
		catch(Exception $e)
		{
			self::exceptionAction();
		}
	}

	/**
	 * 添加监察评论 HTTP POST
	 * @Date 2015-06-15 10:41:42
	 * @author npctest
	 * 
	 * @param token_id
	 * @param monitorId -> 监察信息编号
	 * @param ownerUser -> 用户唯一标识
	 * @param roomid -> 房间号唯一标识
	 * @param commentContent -> 评论信息内容
	 */
	public function addMonitorComment()
	{
		$accept = self::filterPostDataToArray();
		self::verifyToken($accept['token_id']);
		$monitorId=$accept['monitorId'];
		$ownerUser=$accept['ownerUser'];
		$roomid=$accept['roomid'];
		$content=$accept['commentContent'];
		$data=array(
				'monitor_id'=>$monitorId,
				'owner_user'=>$ownerUser,
				'roomid'=>$roomid,
				'comment'=>$content,
				'addtime'=>self::getDateTime(),
				'status'=>1
					);
		try{
				$this->load->model('Monitor_model');
				$back=$this->Monitor_model->addNewMonitorComment($data);
				self::successAction();
			}
		catch(Exception $e)
		{
			self::exceptionAction();
			exit();
		}
	}

	/**
	 * 获取监察分类 HTTP Post
	 * @Date 2015-06-15 10:47:05
	 * @author npctest
	 * 
	 * @param token_id
	 */
	public function getMonitorCategory()
	{
		$accept = self::filterPostDataToArray();
		self::verifyToken($accept['token_id']);
		try{
			 $this->load->model('Monitor_model');
			 $back=$this->Monitor_model->queryCategoryInfo();
			 self::successAction($back);
			}
		catch(Exception $e){
			self::exceptionAction();
			exit();
		}
	}

//----------------------------------------------------------------------------服务之星
	/**
	 * 获取员工信息列表
	 * @Date 2015-06-15 16:14:10
	 * @author npctest
	 * 
	 * @param token_id
	 * @param commid -> 小区ID
	 * @param pageSize -> 条数
	 * @param nowPage -> 当前页数
	 */
	public function getStaff()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept["token_id"];
		self::verifyToken($token_id);
		$commid=$accept["commid"];	//小区编号
	    $pageSize=$accept["pageSize"];
	    $nowPage=$accept["nowPage"];
	    if(intval($nowPage)<=1)
	    {
	    	$nowSize=0;
	    }else{
	    	$nowSize=$pageSize*($nowPage-1);
	    }
	    $this->load->model('Staff_model');
	    $back=$this->Staff_model->getStaffList($commid,$pageSize,$nowSize);
	    self::successAction($back);
	}

	/**
	 * 职员点赞
	 * @Date 2015-06-15 16:15:54
	 * @author npctest
	 * 
	 * @param token_id
	 * @param staffId ->职员编号
	 * @param operType ->点赞操作类型，例如0-赞|1-取消赞
	 */
	public function addGood()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept['token_id'];
		self::verifyToken($token_id);
		$staffId=$accept['staffId'];	//职员编号
		$operType=$accept['operType']; //点赞操作类型 0-赞|1-取消赞
		$this->load->model('Staff_model');
		$back=$this->Staff_model->addGood($staffId,$operType);
		if($back>0)
		{
			self::successAction();
		}else{
			self::errorAction();
		}	
	}


	/**
	 * 添加职员评论
	 * @Date 2015-06-15 16:17:11
	 * @author npctest
	 * 
	 * @param token_id
	 * @param staffId -> 职员编号
	 * @param ownerUser -> 用户唯一标识
	 * @param content -> 评论内容
	 */
	public function addStaffComment()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept['token_id'];
		self::verifyToken($token_id);
		$staffId=$accept['staffId'];
		$ownerUser=$accept['ownerUser'];
		$content=$accept['content'];
		$data=array(
						'staff_id'=>$staffId,
						'addTime'=>self::getDateTime(),
						'owner_User'=>$ownerUser,
						'content'=>$content
					);
		$this->load->model("Staff_model");
		$back=$this->Staff_model->addStaffComment($data);
		if($back>0){
			self::successAction();
		}else{
			self::errorAction();
		}
	}

// ---------------------------------------------------------------------------小区动态&办事指南
	/**
	 * 获取小区动态/办事指南信息列表
	 * @Date 2015-06-17 10:38:24
	 * @author npctest
	 * 
	 * @param token_id
	 * @param type -> 类别 小区动态-0/办事指南-1
	 * @param commid -> 小区编号
	 * @param pageSize -> 页数
	 * @param nowPage -> 当前页
	 */ 
	public function getAppWebList()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept['token_id'];
		self::verifyToken($token_id);
		$commid = $accept['commid'];
		$type = $accept['type'];
		$pageSize=$accept["pageSize"];
	    $nowPage=$accept["nowPage"];
	    if(intval($nowPage)<=1){
	    	$nowSize=0;
	    }else{
	    	$nowSize=$pageSize*($nowPage-1);
	    }
	    switch ($type) {
	    	case '0':
	    		$this->load->model('Notice_model');
	    		$data = $this->Notice_model->getNotice($commid,$pageSize,$nowSize);
	    		break;
	    	case '1':
	    		$this->load->model('Guide_model');
	    		$data = $this->Guide_model->getGuide($commid,$pageSize,$nowSize);
	    		break;
	    	default:
	    		self::errorAction();
	    		break;
	    }
	    self::successAction($data);
	}

	/**
	 * 获取小区动态/办事指南详情
	 * @Date 2015-06-17 10:42:38
	 * @author npctest
	 * 
	 * @param token_id
	 * @param type -> 类别 小区动态-0/办事指南-1
	 * @param sid -> 小区动态/办事指南 编号
	 */ 
	public function getAppWebDetail()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept['token_id'];
		self::verifyToken($token_id);
		$sid = $accept['sid'];
		$type = $accept['type'];
		switch ($type) {
			case '0':
				$this->load->model('Notice_model');
				$data = $this->Notice_model->getNoticeDetail($sid);
				break;
			case '1':
				$this->load->model('Guide_model');
				$data = $this->Guide_model->getGuideDetail($sid);
				break;
			default:
				self::errorAction();
				break;
		}
		self::successAction($data);
	}

//----------------------------------------------------------------------------反馈建议
	/**
	 * 获取反馈建议
	 * @Date 2015-06-15 16:20:46
	 * @author npctest
	 * 
	 * @param token_id 
	 * @param commid -> 小区编号
	 * @param pageSize -> 条数
	 * @param nowPage -> 当前页数
	 * @param ownerUser -> 用户唯一标识
	 */
	public function getFeedback()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept["token_id"];
		self::verifyToken($token_id);
		$commid=$accept["commid"];
		$pageSize=$accept["pageSize"];
		$nowPage=$accept["nowPage"];
		$ownerUser=$accept["ownerUser"];
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else{
			$nowSize=($nowPage-1)*$pageSize;
		}
		try{
			$this->load->model("Feedback_model");
			$res=$this->Feedback_model->getFeedback($commid,$ownerUser,$pageSize,$nowSize);
			self::successAction($res);
		}catch(Exception $e){
			self::errorAction();
		}
	}

	/**
	 * 添加反馈建议
	 * @Date 2015-06-16 18:45:41
	 * @author npctest
	 * 
	 * @param token_id
	 * @param ownerUser -> 用户唯一标识
	 * @param content -> 内容信息
	 * @param commid -> 小区编号
	 * @param PicNum -> 图片可选参数/上传图片以附件上传
	 */ 
	public function addFeedback()
	{
		$result = $this->common->uploadImage('feedback');
		if($result){
			$picNum = $result;
		}else{
			$picNum = "";
		}
		$accept = self::filterPostImageToArray();
		$token_id = $accept["token_id"];
		self::verifyToken($token_id);
		$ownerUser = $accept['ownerUser'];
		$content = $accept["content"];
		$commid = $accept["commid"];
		$data = array(
				'content' => $content,
				'commid' => $commid,
				'owner_user' => $ownerUser,
				'feed_pic' => $picNum,
				'add_time' => self::getDateTime()
			);
		$this->load->model('Feedback_model');
		$res = $this->Feedback_model->addFeedback($data);
		log_message('error',json_encode($res));
		if($res){
			self::successAction();
		}else{
			self::errorAction();
		}
	}

	/**
	 * 获取反馈建议回复
	 * @Date 2015-06-15 16:21:48
	 * @author npctest
	 * 
	 * @param token_id
	 * @param fid -> 反馈建议编号
	 */
	public function getFeedbackComment()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept["token_id"];
		self::verifyToken($token_id);
		$fid = $accept["fid"];
		$pageSize = $accept['pageSize'];
		$nowPage = $accept["nowPage"];
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else{
			$nowSize=($nowPage-1)*$pageSize;
		}
		$this->load->model("Feedback_model");
		$res = $this->Feedback_model->getFeedbackComment($fid,$pageSize,$nowSize);
		self::successAction($res);
	}

	/**
	 * 添加反馈建议回复
	 * @Date 2015-06-16 19:00:03
	 * @author npctest
	 * 
	 * @param token_id
	 * @param fid -> 反馈建议信息编号
	 * @param comment -> 回复内容
	 * @param owner_user -> 用户唯一标识
	 */ 
	public function addFeedbackComment()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept["token_id"];
		self::verifyToken($token_id);
		$fid = $accept["fid"];
		$comment = $accept["comment"];
		$ownerUser = $accept["ownerUser"];
		$data = array(
				'feed_id' => $fid,
				'comment' => $comment,
				'account' => $ownerUser,
				'type' => '1',  // 0-物业|1-用户
				'add_time' => self::getDateTime()
			);
		$this->load->model("Feedback_model");
		$res = $this->Feedback_model->addFeedbackComment($data);
		if($res){
			self::successAction();
		}else{
			self::errorAction();
		}
	}

//----------------------------------------------------------------------------费用账单
	/**
	 * 获取账单信息列表
	 * @Date 2015-06-17 11:14:19
	 * @author npctest
	 * 
	 * @param token_id
	 * @param roomid -> 房号唯一标识
	 * @param pageSize -> 条数
	 * @param nowPage -> 当前页
	 */
	public function getBillList()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept['token_id'];
		self::verifyToken($token_id);
		$roomid = $accept["roomid"];
		$pageSize = $accpet["pageSize"];
		$nowPage = $accept["nowPage"];
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else{
			$nowSize=($nowPage-1)*$pageSize;
		}
		$this->load->model("Bill_model");
		$res = $this->Bill_model->getBillByRoomId($roomid,$pageSize,$nowSize);
		self::successAction($res);	
	}

	/**
	 * 获取账单详情
	 * @Date 2015-06-17 11:15:22
	 * @author npctest
	 * 
	 * @param token_id
	 * @param billNo -> 账单编号
	 */ 
	public function getBiilDetail()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept['token_id'];
		self::verifyToken($token_id);
		$billNo = $accept['billNo'];
		$this->load->model('Bill_model');
		$res = $this->Bill_model->getBillDetail($billNo);
		self::successAction($res);
	}

	/**
	 * 支付结果回调
	 * @Date 2015-06-17 11:16:46
	 * @author npctest
	 */ 
	public function getPayCostResult()
	{
		
	}

// ---------------------------------------------------------------------------消息中心
	/**
	 * 获取消息中心列表
	 * @Date 2015-06-18 10:49:00
	 * @author npctest
	 * 
	 * @param token_id
	 * @param ownerUser -> 用户唯一标识
	 */ 
	public function getMessage()
	{
		$accept = self::filterPostDataToArray();
		$token_id = $accept['token_id'];
		$ownerUser = $accept['ownerUser'];
		$this->load->model('Message_model');
		$pageSize=$accept["pageSize"];
		$nowPage=$accept["nowPage"];
		if(intval($nowPage)<=1)
		{
			$nowSize=0;
		}else{
			$nowSize=($nowPage-1)*$pageSize;
		}
		$data = $this->Message_model->getMessageList($ownerUser,$pageSize,$nowSize);
		self::successAction($data);
	}

//----------------------------------------------------------------------------APP接口通用工具方法

	public function getSMS()
	{
		//$this->load->library('common');
		$this->common->getReturnSMS();
	}



	public function getThumb()
	{
		$source='./assets/upload/test2.png';
		$dir='./assets/thumb/';
		$config=$this->common->processPic($source,$dir,150,150);
		$this->load->library('image_lib', $config);  //加载图片处理类
		$this->image_lib->rotate();
		if($this->image_lib->rotate())
		{
			$this->image_lib->clear();
		}
		$config=$this->common->processPic($source,$dir,150,150);
		if($this->image_lib->resize()) //调用图片处理对象生成缩略图
		{
			echo "成功1";
			$config_thumb=$this->common->processPic($source,$dir,110,110);
			$this->image_lib->initialize($config_thumb);
			if($this->image_lib->resize())
			{
				echo "成功2";
				$this->image_lib->clear();
				$config_thumb2=$this->common->processPic($source,$dir,650,350);
				$this->image_lib->initialize($config_thumb2);
				$this->image_lib->resize();
			}else{
				echo "失败2";
			}
		}else{
			echo "<br />失败";
			echo "<br />". $this->image_lib->display_errors();
		}
	}

	/**
	 * 获取服务器时间【外部】
	 */ 
	public function getSerTime()
	{
		date_default_timezone_set("Asia/Shanghai");
		$res=array("currentDate"=>date("Y-m-d"));
		self::successAction($res);
	}

	/**
	 * 获取服务器时间【内部】
	 */ 
	public function getDateTime()
	{   
		date_default_timezone_set("Asia/Shanghai");
		return date("Y-m-d H:i:s");
	}

	/**
	 * 过滤JSON对象多余符号,并进行urldecode解码,最后转成数组
	 * @Date 2015-06-14 21:51:35
	 * @author npctest
	 */
	public function filterGetDataToArray()
	{
		//CI框架 $this->uri->segment(num) 获取controller/开始第几个/后的值
		$data=$this->uri->segment(4);
		// if(strstr($data,"(null)=")){
		// 	$data=str_replace("(null)=","",$data);	
		// }
		//log_message('INFO',json_encode($data));
		$data=urldecode($data);
		$data=json_decode($data,JSON_UNESCAPED_UNICODE);
		return $data;
	}

	/**
	 * 过滤JSON对象多余符号,并进行urldecode解码,最后转成数组
	 * @Date 2015-06-14 21:51:35
	 * @author npctest
	 * @return array 返回数组
	 */
	public function filterPostDataToArray()
	{
		//$this->common->uploadImage('repair');
		$data = file_get_contents("php://input");
		$data=urldecode($data);
		log_message('INFO',$data);
		$data=json_decode($data,JSON_UNESCAPED_UNICODE);
		return $data;		
	}

	public function filterPostImageToArray()
	{
		//$data = file_get_contents("php://input");
		$data =  $this->input->post('data', TRUE);
		$data=urldecode($data);
		//log_message('INFO',$data);
		$data=json_decode($data,JSON_UNESCAPED_UNICODE);
		return $data;		
	}

	/**
	 * 验证TOKEN是否合法【内部】
	 */
	public function verifyToken($token_id="")
	{
		//$token_id=$this->input->post('token_id', TRUE);
		$this->load->model('Apptoken_model');
		$back=$this->Apptoken_model->matchToken($token_id);
		if($back){
			return;
		}else{
			self::refuseAction();
			exit();
		}
	}

	/**
	 * 初始化验证TOKEN是否合法【外部】
	 */
	public function matchToken()
	{
		$accept = self::filterPostDataToArray();
		$token_id=$accept['token_id'];
		$this->load->model('Apptoken_model');
		$back=$this->Apptoken_model->matchToken($token_id);
		if($back)
		{
			self::successAction();
		}
		else
		{
			self::refuseAction();
			exit();
		}
	}

//----------------------------------------------------------------------------APP接口内部JSON返回封装方法
	
	/**
	 * 返回token异常失败JSON
	 */ 
	public function refuseAction()
	{
		$res["code"]=110;
		$res["msg"]="非法请求！";
		$res["serTime"]=self::getDateTime();
		$res=json_encode($res,JSON_UNESCAPED_UNICODE);
		echo $res;
	}

	/**
	 * 返回服务器失败JSON
	 */
	public function errorAction()
	{
		$res["code"]=0;
		$res["msg"]="操作失败";
		$res["serTime"]=self::getDateTime();
		$res=json_encode($res,JSON_UNESCAPED_UNICODE);
		echo $res;
		exit();
	}

	/**
	 * 返回服务器成功JSON
	 */ 
	public function successAction($data=array())
	{
		$res["code"]=1;
		$res["msg"]="操作成功！";
		$res["data"]=$data;
		$res["serTime"]=self::getDateTime();
		$res=json_encode($res,JSON_UNESCAPED_UNICODE);
		echo $res;
	}

	/**
	 * 异常处理返回JSON
	 */ 
	public function exceptionAction()
	{
		$res["code"]=500;
		$res["msg"]="服务器内部错误！";
		$res["serTime"]=self::getDateTime();
		$res=json_encode($res,JSON_UNESCAPED_UNICODE);
		echo $res;
	}

}
	
/* End of file Api.php */
/* Location: ./application/controllers/app/Api.php */