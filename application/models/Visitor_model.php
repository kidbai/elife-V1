<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visitor_model extends MY_Model {

	protected $tableName = 'el_visitor';

	public function __construct()
	{
		parent::__construct('Visitor_model');
	}

	/**
	 * 添加一个新访客记录
	 */
	public function addVisitor($data)
	{
		$res=parent::insertObject($this->tableName,$data);
		return $res;
	}

	/**
	 * 根据房间号获取该房间号下的访客记录
	 */
	public function queryVisitorByRoomId($roomid,$count,$currentSize)
	{
		$res=parent::queryObject($this->tableName,$roomid,$count,$currentSize);
		return $res;
	}

	/**
	 *根据业主查询该业主访客记录
	 */
	public function queryVisitorsByOwnerUser($ownerUser,$count,$currentSize)
	{
		$strWhere=array('owner_user'=>$ownerUser,'status <>'=>'9');
		$res = parent::queryObject($this->tableName,$strWhere,$count,$currentSize);
		return $res;
	}
	
	/**
	 * 根据系统编码删除访客通行信息记录
	 */ 
	public function cancelVisitorBySid($vid)
	{
		$strWhere=array('sid'=>$vid);
		$res=parent::deleteObject($this->tableName,$strWhere);
		return $res;
	}

	/**
	 * 根据系统编码更新访客通行信息记录的状态【9-不对业主显示】
	 */
	public function delVisitorBySid($data,$strWhere)
	 {
	 	$res=parent::updateObject($this->tableName,$data,$strWhere);
		return $res;
	 } 
}
?>