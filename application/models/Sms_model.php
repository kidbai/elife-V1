<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms_model extends MY_Model {

	protected $tableName = 'el_sms';

	public function __construct()
	{
		parent::__construct('Sms_model');
		date_default_timezone_set('PRC');	
	}

	public function addSMS($to,$verifyNum,$exprise)
	{
   		$addtime= date("Y-m-d h:i:s");
		$res=parent::insertObject($this->tableName,array('mobile'=>$to,'sms'=>$verifyNum,'add_time'=>$addtime,'expires_time'=>$exprise));
		return $res;
	}

	public function verifySMS($to,$verifyNum)
	{
		$currentTime=date("Y-m-d h:i:s");
		$res=parent::queryObject($this->tableName,array('mobile'=>$to,'sms'=>$verifyNum));
	}
}

/* End of file SMS_Model.php */
/* Location: ./application/models/SMS_Model.php */