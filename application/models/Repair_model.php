<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Repair_model extends MY_Model {

	protected $tableName = 'el_home_repair';

	public function __construct()
	{
		parent::__construct('Repair_model');
	}

	/**
	 * 根据用户编号获取报修
	 * @Date 2015-06-24 18:28:39
	 * @author npctest
	 */ 
	public function getRepairsByOwnerUser($ownerUser,$count,$currentSize,$status=0)
	{
		switch ($status) {
			case 0:
				$strWhere = " and el_home_repair.status <> 5 and el_home_repair.is_show is true "; //默认
				break;
			case 1:
				$strWhere = "and el_home_repair.status = 1 "; //处理中
				break;
			case 2:
				$strWhere = " and el_home_repair.status = 2 and el_home_repair.is_show is true"; //已完成(待处理)
			default:
				$strWhere = " and el_home_repair.status <> 5 and el_home_repair.is_show is true "; //待处理
				break;
		}
		$strSQL="select el_home_repair.*, ifnull(el_staff.`name`,'') as staffName,
				 ifnull(el_staff.mobile,'')  as staffMobile 
				from el_home_repair
				left join el_staff on el_home_repair.staffCode=el_staff.`code`
				where el_home_repair.ownerUser= '".$ownerUser."' ".$strWhere."
				order by el_home_repair.sid desc
				limit ".$currentSize.",".$count."";
		$res = parent::queryObject($strSQL);
		$array=$this->common->stdClassToArray($res); //数组对象转数组
		foreach($array as $key=>$val)
		{    
			// ------------------------------------------------------------------------获取报修图片
			if($array[$key]['repairPic']!=null&&$array[$key]['repairPic']!=""){
				$repairPicData=$this->db->query("select picAddr from el_picture where picNum='".$array[$key]['repairPic']."'")->result();
				$repairPicData=$this->common->stdClassToArray($repairPicData); //数组对象转数组
				$repairArr=array();
				foreach($repairPicData as $rkey=>$rval){
					$repairArr[$rkey]=$repairPicData[$rkey]['picAddr'];
				}
				$array[$key]['repairPicAddr']=(array)$repairArr;
			}
			// ------------------------------------------------------------------------获取回复图片
			if($array[$key]['replyPic']!=null&&$array[$key]['replyPic']!=""){
				$replyPicData=$this->db->query("select picAddr from el_picture where picNum='".$array[$key]['replyPic']."'")->result();
				$replyPicData=$this->common->stdClassToArray($replyPicData);
				$replyArr=array();
				foreach($replyPicData as $ckey=>$cval){
					$replyArr[$ckey]=$replyPicData[$ckey]['picAddr'];
				}
				$array[$key]['commentPicAddr']=(array)$replyArr;
			}	
		}
		return $array;
	}

	/**
	 * 根据工单编号获取报修信息
	 * @Date 2015-06-23 10:53:50
	 * @author npctest
	 * 
	 * @param orderNo -> 工单编号
	 */ 
	public function getRepairByRid($orderNo)
	{
		$strSQL="select el_home_repair.*, ifnull(el_staff.`name`,'') as staffName,
				 ifnull(el_staff.mobile,'')  as staffMobile 
				 from el_home_repair
				 left join el_staff on el_home_repair.staffCode=el_staff.`code`
				 where el_home_repair.orderNo= '".$orderNo."'";
		//$condition = array('orderNo' => $orderNo);
		$res = parent::queryObjectBySQLData($strSQL);
		return $res;
	}

	public function getRepairByCommid($commid, $count, $currentSize)
	{
		//$condition = array('roomId like ' => $commid."-%");
		//$res = parent::queryObject($this->tableName,$condition,$count,$currentSize);
		$strSQL =  "select 
			(@rowNO := @rowNo+1) as rowNo,
			 sid,
			 orderNo,
			 roomId,
			 orderDesc,
			 ownerUser,
			 status,
			 forwardTime,
			 addTime,
			 staffName,
			 staffMobile,
			 serviceStar,
			 techStar
FROM(  
select
			 a.sid,
			 a.orderNo,
			 a.roomId,
			 a.orderDesc,
			 a.ownerUser,
			 a.status,
			 a.forwardTime,
			 a.addTime,
			 ifnull(b.`name`,'') as staffName,
			 ifnull(b.mobile,'')  as staffMobile,
			 case a.serviceStar when 0 then '/'  else CONCAT(a.serviceStar,'分') end as serviceStar,	
			 case a.techStar when 0 then '/'  else CONCAT(a.techStar,'分') end as techStar 
			 from el_home_repair a
			 left join el_staff b on a.staffCode=b.`code`
			 where a.roomId like '".$commid."-%' 
			 order by a.status asc,a.sid desc 
			 limit ".$currentSize.",".$count.") as c,
			 (select @rowNO :=".$currentSize.") d";
		$res = parent::queryObject($strSQL);
		return $res;
	}

	public function addRepair($data)
	{
		$res = parent::insertObject($this->tableName,$data);
		return $res;
	}

	/**
	 * 订单完工
	 * @Date 2015-06-26 18:57:54
	 * @author npctest
	 * 
	 * @param orderNo -> 工单
	 * @param commid ->小区编号
	 */ 
	public function procRepair($orderNo)
	{
		$condition = array('orderNo' => $orderNo);
		$data = array('status' => '2');
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 删除报修记录/用户取消显示信息
	 * @Date 2015-06-23 15:22:09
	 * @author npctest
	 * 
	 * @param rid -> 工单
	 */ 
	public function updateRepair($rid)
	{
		$data = array('is_show' => false);
		$condition = array('orderNo'=>$rid);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 物业更新数据
	 * @Date 2015-06-23 18:53:09
	 * @author npctest
	 * 
	 * @param data -> 数据集
	 * @param orderNo -> 工单
	 */ 
	public function updateRepairByProp($data,$orderNo)
	{
		$condition = array('orderNo' => $orderNo);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 撤销报修
	 * @Date 2015-06-23 15:18:04
	 * @author npctest
	 * 
	 * @param rid -> 工单
	 */ 
	public function delRepair($rid)
	{
		$condition = array('orderNo' => $rid);
		$data = array('status' => '5');
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 添加报修评论
	 * @Date 2015-06-24 18:27:11
	 * @author npctest
	 * 
	 * @param data -> 数据集
	 * @param orderNo -> 工单
	 */ 
	public function addRepairComment($data,$orderNo)
	{
		$condition = array('orderNo' => $orderNo);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 获取表列名信息
	 * $Date 2015-06-24 18:35:07
	 * @author npctest
	 */ 
	public function getTableFields()
	{
		$res = parent::queryTableFields($this->tableName);
		$data = array();
		foreach($res as $item){
			$data[$item->Field] = '';
		}
		return $data;
	}

	public function getSearchInfo($filter,$count,$currentSize)
	{
		$condition = $filter;
		$res = parent::queryObject($this->tableName,$condition,$count,$currentSize);
		return $res;
	}

	public function getSearchCount($filter)
	{
		$condition = $filter;
		$res = parent::queryObjectReturnCount($this->tableName,$filter);
		return $res;
	}
}
?>