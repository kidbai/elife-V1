<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bill_model extends MY_Model {

	protected $tableName='el_bill';

	public function __construct()
	{
		parent::__construct('Bill_Model');	
	}


	public function getBillByRoomId($roomid,$pageCount,$nowSize)
	{
		$condition=array('roomid'=>$roomid);
		$res=parent::queryObject($this->tableName,$condition);
		return $res->result();
	}

	public function getRuleByRoomId($commid,$type)
	{
		$condition=array('commid'=>$roomid,'type'=>$type);
		$res=parent::queryObject('el_rule',$condition);
		return $res->row_array();
	}
}

/* End of file Bill_Model.php */
/* Location: ./application/models/Bill_Model.php */