<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Monitor_model extends MY_Model {

	protected $tableName = 'el_public_monitor';

	public function __construct()
	{
		parent::__construct('Monitor_model');
	}

	/**
 	 * 获取个人监察信息
 	 * @Date 2015-06-15 10:57:41
 	 * @author npctest
 	 * 
 	 * @param ownerUser -> 用户唯一标识
 	 * @param count -> 条数
 	 * @param $currentSize 当前偏移量(当前页索引的开始数)
	 */
	public function getMonitorsByOwnerUser($ownerUser,$count,$currentSize)
	{
		//$strWhere=array('owner_user'=>$ownerUser);
		$strSQL="select el_owner_user.username,
			el_monitor_category.category_name,
			(select count(1) from el_monitor_comment where el_monitor_comment.monitor_id=el_public_monitor.sid) as commentCount,
			el_public_monitor.*
			from el_public_monitor 
			join el_owner_user 
			on el_owner_user.owner_code=el_public_monitor.owner_user
			join el_monitor_category
			on el_public_monitor.categoryid=el_monitor_category.sid
			where el_public_monitor.owner_user='".$ownerUser."'
			order by el_public_monitor.sid desc
			limit ".$currentSize.",".$count."";
		$res = parent::queryObject($strSQL);
		$array = $this->common->stdClassToArray($res);
		foreach($array as $key=>$val)
		{    
			// ------------------------------------------------------------------------获取监察图片
			if($array[$key]['monitorPicId']!=null&&$array[$key]['monitorPicId']!=""){
				$monitorPicData=$this->db->query("select picAddr from el_picture where picNum='".$array[$key]['monitorPicId']."'")->result();
				$monitorPicData=$this->common->stdClassToArray($monitorPicData); //数组对象转数组
				$monitorArr=array();
				foreach($monitorPicData as $rkey=>$rval){
					$monitorArr[$rkey] = $monitorPicData[$rkey]['picAddr'];
				}
				$array[$key]['monitorPicAddr']=(array)$monitorArr;	
			}
				
		}
		return $array;
	}

	/**
	 * 获取所有公共监察信息
	 * @Date 2015-06-15 11:00:13
	 * @author npctest
	 * 
	 * @param count -> 当前条数
	 * @param currentSize 当前偏移量(当前页索引的开始数)
	 */
	public function getPublicMinitors($commid,$count,$currentSize)
	{
		$strSQL="select el_owner_user.username,
			el_monitor_category.category_name,
			(select count(1) from el_monitor_comment where el_monitor_comment.monitor_id=el_public_monitor.sid) as commentCount,
			el_public_monitor.*
			from el_public_monitor 
			join el_owner_user 
			on el_owner_user.owner_code=el_public_monitor.owner_user
			join el_monitor_category
			on el_public_monitor.categoryid=el_monitor_category.sid
			where el_public_monitor.commid ='".$commid."'
			order by el_public_monitor.sid desc
			limit ".$currentSize.",".$count."";
		$res = parent::queryObject($strSQL);
		$array = $this->common->stdClassToArray($res);
		foreach($array as $key=>$val)
		{    
			// ------------------------------------------------------------------------获取监察图片
			if($array[$key]['monitorPicId']!=null&&$array[$key]['monitorPicId']!=""){
				$monitorPicData=$this->db->query("select picAddr from el_picture where picNum='".$array[$key]['monitorPicId']."'")->result();
				$monitorPicData=$this->common->stdClassToArray($monitorPicData); //数组对象转数组
				$monitorArr=array();
				foreach($monitorPicData as $rkey=>$rval){
					$monitorArr[$rkey] = $monitorPicData[$rkey]['picAddr'];
				}
			$array[$key]['monitorPicAddr']=(array)$monitorArr;
			}	
		}
		return $array;
	}

	/**
	 * 获取监察评论
	 * @Date 2015-06-15 11:02:17
	 * @author npctest
	 * 
	 * @param mid -> 监察信息编号
	 * @param count -> 条数
	 * @param currentSize -> 当前偏移量(当前页索引的开始数)
	 */ 
	public function getMonitorCommentBySid($mid,$count,$currentSize)
	{
		$strSQL="select el_owner_user.username,
			case el_owner_user.type when '0' then '业主' 
			when '1' then '家庭成员'
			when '2' then '租客' end as type,
			el_monitor_comment.*
			from el_monitor_comment 
			join el_owner_user
			on el_owner_user.owner_code=el_monitor_comment.owner_user
			where el_monitor_comment.monitor_id='".$mid."'
			order by sid desc
			limit ".$currentSize.",".$count."";
		$res = parent::queryObject($strSQL);
		return $res;
	}

	/**
	 * 增加新的监察信息
	 * @Date 2015-06-15 11:03:11
	 * @author npctest
	 * 
	 * @param data -> 添加信息数据集(array)
	 */ 
	public function addNewMonitor($data)
	{
		$res = parent::insertObject($this->tableName,$data);
		//log_message('INFO',$this->db->last_query());
		return $res;
	}

	/**
	 * 新增监察评论
	 * @Date 2015-06-15 11:04:05
	 * @author npctest
	 * 
	 * @param data -> 添加评论数据集(array)
	 */
	public function addNewMonitorComment($data)
	{
		$res = parent::insertObject('el_monitor_comment',$data);
		return $res;
	}

	/**
	 * 更新监察信息
	 * @Date 2015-06-15 11:04:46
	 * @author npctest
	 * 
	 * @param data -> 更新数据集(array)
	 * @param mid -> 监察信息编号 
	 */
	public function updateMonitorBySid($data,$mid)
	{
		$condition = array('sid'=>$mid);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 删除监察信息
	 * @Date 2015-06-15 11:05:57
	 * @author npctest
	 * 
	 * @param mid -> 监察信息
	 */ 
	public function deleteMonitor($mid)
	{
		$condition = array('sid'=>$mid);
		$res = parent::deleteObject($this->tableName,$condition);
		return $res;
	}

	/**
	 * 删除监察评论
	 * @Date 2015-06-15 11:10:01
	 * @author npctest
	 * 
	 * @param mcid -> 监察评论编号
	 */ 
	public function deleteMoniComment($mcid)
	{
		$condition = array('sid' => $mcid);
		$res = parent::deleteObject('el_monitor_comment',$condition);
		return $res;
	}

	/**
	 * 获取监察分类
	 * @Date 2015-06-15 11:10:46
	 * @author npctest
	 */ 
	public function queryCategoryInfo()
	{
		$strSQL = "select * from el_monitor_category";
		$res = parent::queryObject($strSQL);
		return $res;
	}
}
?>