<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apptoken_model extends MY_Model {

	public $tableName='el_app_token';

	public function __construct()
	{
		parent::__construct('Apptoken_model');
		
	}

	/**
	 * 添加token
	 * @Date 2015-06-13 12:37:29
	 * @author npctest
	 * 
	 * @param data -> 添加的数据集合(array)
	 */
	public function addToken($data)
	{
		$back=parent::insertObject($this->tableName,$data);
		return $back;
	}

	/**
	 * 注销删除token
	 * @Date 2015-06-13 12:36:53
	 * @author npctest
	 * 
	 * @param mobile -> 手机号码
	 */
	public function logout($mobile)
	{
		$condition=array('mobile'=>$mobile);
		$res=parent::deleteObject($this->tableName,$condition);
		return $res;
	}

	/**
	 * 更新token
	 * @Date 2015-06-13 12:36:14
	 * @author npctest
	 * 
	 * @param token -> 旧token
	 */
	public function updateToken($token)
	{
		$condition=array('token_id' => $token);
		$data=array('last_time' => date("Y-m-d H:i:s")); //更新最近一次token时间
		$res=parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 *查询token
	 * @Date 2015-06-15 02:11:54
	 * @author npctest
	 * 
	 * @param mobile -> 手机号码 
	 */
	public function queryToken($mobile)
	{
		$condition = array('mobile' => $mobile);
		$res=parent::queryObjectData($this->tableName,$condition);
		return $res;
	}

	/**
	 * 验证token是否有效
	 * @Date 2015-06-13 12:35:59
	 * @author npctest
	 * 
	 * @param token -> token
	 */
	public function matchToken($token)
	{
		$condition=array('token_id'=>$token);
		$res=$this->db->get_where($this->tableName,$condition);
		//log_message('INFO',$this->db->last_query());
		$back=$res->row_array();
		$lastTime=$back['last_time'];
		$lastTimestamp=strtotime($lastTime);
		$expires=$back['expires_time'];
		$currentTime=date("Y-m-d H:i:s");
		$currentTimestamp=strtotime($currentTime);
		$result=intval($currentTimestamp)-intval($lastTimestamp);
		//log_message('INFO',"结果是：".$result."|当前时间：".$currentTimestamp."|最后次登录时间：".$lastTimestamp); //判断过期时间
		if($result>=$expires){
			$this->db->delete($this->tableName,$condition); //过期删除已有token
			return false;
		}else{
			self::updateToken($token);
			return true;
		}
	}
}

/* End of file AppToken_Model.php */
/* Location: ./application/models/AppToken_Model.php */