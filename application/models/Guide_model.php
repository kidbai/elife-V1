<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Guide_model extends MY_Model {

	public $tableName = "el_guide";

	public function __construct()
	{
		parent::__construct('Guide_model');	
	}

	/**
	 * 查询办事指南信息
	 * @Date 2015-06-17 15:57:12
	 * @author npctest
	 * 
	 * @param commid -> 小区编号
	 * @param pageSize -> 条数
	 * @param currentSize ->当前偏移量(当前页开始索引数)
	 * @return res(数组对象) -> 返回数据集合
	 */ 
	public function getGuide($commid,$pageSize,$currentSize)
	{
		$condition = array('commid' => $commid);
		$column = ("sid,title,add_time");
		$res = parent::queryObject($this->tableName,$column,$condition,$pageSize,$currentSize);
		return $res;
	}

	/**
	 * 查询办事指南详情
	 * @Date 2015-06-18 10:38:36
	 * @author npctest
	 * 
	 * @param gid -> 办事指南编号
	 */ 
	public function getGuideDetail($gid)
	{
		$condition = array('sid' => $gid);
		$res = parent::queryObject($this->tableName,$condition);
		return $res;
	}

	/**
	 * 新增办事指南
	 * @Date 2015-06-17 15:55:12
	 * @author npctest
	 * 
	 * @param data -> 新增信息的数据集合(array)
	 * @return res(bool) -> true/false
	 */ 
	public function addGuide($data)
	{
		$res = parent::insertObject($this->tableName,$data);
		return $res;
	}

	/**
	 * 更新办事指南信息
	 * @Date 2015-06-17 15:54:06
	 * @author npctest
	 * 
	 * @param data -> 所需更新的数据集合(array)
	 * @param gid -> 办事指南编号
	 * @return res(bool) -> true/false
	 */ 
	public function updateGuide($gid,$data)
	{
		$condition = array('sid' => $gid);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 物业删除(更新状态为9，不可见)
	 * @Date 2015-06-17 15:51:59
	 * @author npctest
	 * 
	 * @param gid -> 办事指南编号
	 * @return res(bool) -> true/false
	 */ 
	public function updateDelGuide($gid)
	{
		$condition = array('sid' => $gid);
		$data = array('status' => 9);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 服务中心删除(数据库真实删除：慎用！)
	 * @Date 2015-06-17 15:53:00
	 * @author npctest
	 * 
	 * @param gid -> 办事指南编号
	 * @return res(bool) -> true/false
	 */ 
	public function deleteGuide($gid)
	{
		$condtion = array('sid' => $gid);
		$res = parent::deleteObject($this->tableName,$condition);
		return $res;
	}
}

/* End of file News_model.php */
/* Location: ./application/models/News_model.php */