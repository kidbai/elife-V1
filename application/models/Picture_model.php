<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Picture_model extends MY_Model {

	public $tableName='el_picture';

	public function __construct()
	{
		parent::__construct('Picture_model');
	}

	/**
	 * [addPicture 添加图片]
	 * @param [type] $data [description]
	 */
	public function addPicture($data)
	{
		$res=parent::insertObject($this->tableName,$data);
		return $res;
	}

	/**
	 * [queryPicture 根据唯一标识码查询图片地址]
	 * @param  [string] $uniqid [唯一标识]
	 * @return [type]         [description]
	 */
	public function queryPicture($uniqid)
	{
		$condition=array('picNum'=>$uniqid);
		$res=parent::queryObject($this->tableName,$condition);
		return $res;
	}

	
}

/* End of file Picture_model.php */
/* Location: ./application/models/Picture_model.php */