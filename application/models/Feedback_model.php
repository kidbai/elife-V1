<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_model extends MY_Model {

	public $tableName = "el_feedback";

	public function __construct()
	{
		parent::__construct('Feedback_model');
		
	}

	/**
	 * 获取反馈建议信息
	 */ 
	public function getFeedback($commid,$ownerUser,$pageSize,$currentSize)
	{
		$condition = array('owner_user' => $ownerUser,'commid' => $commid);
		$res = parent::queryObject($this->tableName,$condition,$pageSize,$currentSize);
		$array = $this->common->stdClassToArray($res);
		foreach($array as $key=>$val)
		{    
			// ------------------------------------------------------------------------获取评论数量
			$commentCount = parent::queryObjectReturnCount('el_feed_comment',array('feed_id'=>$array[$key]['sid']));

			// ------------------------------------------------------------------------获取反馈建议图片
			if($array[$key]['feed_pic']!=null&&$array[$key]['feed_pic']!=""){
				$feedPicData=$this->db->query("select picAddr from el_picture where picNum='".$array[$key]['feed_pic']."'")->result();
				$feedPicData=$this->common->stdClassToArray($feedPicData); //数组对象转数组
				$feedArr=array();
				foreach($feedPicData as $rkey=>$rval){
					$feedArr[$rkey] = $feedPicData[$rkey]['picAddr'];
				}
				$array[$key]['feedPicData'] = (array)$feedArr;	
			}
			$array[$key]['feedCommentCount'] = $commentCount;	
		}
		return $array;
	}

	/**
	 * 添加反馈建议
	 */ 
	public function addFeedback($data)
	{
		$res = parent::insertObject($this->tableName,$data);
		return $res;
	}

	/**
	 * 删除反馈建议
	 */ 
	public function deleteFeedback($fid)
	{
		$condition = array('sid' => $fid);
		$res = parent::deleteObject($this->tableName,$condition);
		return $res;
	}

	/**
	 * 更新反馈建议
	 */ 
	public function updateFeedback($data,$fid)
	{
		$condtion = array('sid' => $fid);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 获取反馈建议回复
	 */ 
	public function getFeedbackComment($fid,$count,$currentSize)
	{
		$condition = array('feed_id' => $fid);
		$strOrder = "sid";
		$res = parent::queryObjectOrderby('el_feed_comment',$condition,$strOrder,$count,$currentSize);
		return $res;
	}

	/**
	 * 新增反馈建议回复
	 */ 
	public function addFeedbackComment($data)
	{
		$res = parent::insertObject('el_feed_comment',$data);
		return $res;
	}

	/**
	 * 更新反馈建议回复
	 */ 
	public function updateFeedbackComment($fcid,$data)
	{
		$condition = array('sid' => $fcid);
		$res = parent::updateObject('el_feed_comment',$data,$condtion);
	}

	/**
	 * 删除反馈建议回复
	 */ 
	public function deleteFeedbackComment($fcid)
	{
		$condition = array('sid' => $fcid);
		$res = parent::deleteObject('el_feed_comment',$condition);
	}
}

/* End of file Feedback_model.php */
/* Location: ./application/models/Feedback_model.php */