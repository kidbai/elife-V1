<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Owner_model extends MY_Model {

	protected $tableName = 'el_owner_user';

	public function __construct()
	{
		parent::__construct('Owner_model');
	}

	//检查帐号
	public function checkAccount($telNo,$pwds)
	{
		$strSQL="select  a.sid,
				 a.type,
				 a.mobile,
				 a.owner_code as ownerUser,
				 a.room_id as roomdesc,
				 case a.head_thumb when '' then 'assets/head_thumb/default.png'
				 when NULL then 'assets/head_thumb/default.png' end as headThumb,
				 a.last_time,
				 b.remark
				from el_Owner_User a join 
				el_community b
				on substring_index(a.room_id,'-',3)=b.sid
				where mobile='".$telNo."' and passwords='".$pwds."'";
		$res=$this->db->query($strSQL);
		if(isset($res)){
			$condition = array('mobile' => $telNo,'passwords' => $pwds);
			$data = array('last_time' => date('Y-m-d H:i:s'));
			parent::updateObject($this->tableName,$data,$condition);
		}
		return $res->row_array();
	}

	//获取个人信息
	public function getOwnerInfo($uid)
	{
		$strSQL="select 
				b.`name` as commName,
				a.sid,
				a.username,
				a.owner_code,
				a.room_id,
				a.type,
				a.pid,
				case a.type when 0 then '业主'
				when 1 then '家庭成员'
				when 2 then '租客' end as typeName,
				a.mobile,
				a.parking_number,
				a.car_number
				from el_owner_user a
				join el_community b
				on SUBSTRING_INDEX(a.room_id,'-',1)=b.sid
				where a.owner_code='".$uid."'
				order by type";
		$res=parent::queryObject($strSQL);
		$array=$this->common->stdClassToArray($res);
		foreach($array as $key=>$val)
		{    
			if(isset($array[$key]["type"])&&$array[$key]["type"]==0)
			{
				$strChildSQL="select a.username,
							a.mobile,
							a.type,
							case a.type when 0 then '业主'
							when 1 then '家庭成员'
							when 2 then '租客' end as typeName
							from el_owner_user a
							where a.`status`<>9 
					 		and pid=".$array[$key]["sid"];
				$back=parent::queryObject($strChildSQL);
				$array[$key]["childOwner"]=$back;
			}	
		}
		return $array;

	}

	//是否存在帐号
	public function checkExistAccount($data)
	{
		$res=parent::queryObject($this->tableName,$data);
		return $res;
	}

	//新增帐号
	public function addAccount($data)
	{
		$res=parent::insertObject($this->tableName,$data);
		return $res;
	}

	//获取主人
	public function getOwnerName($roomid,$mobile)
	{
		$condition=array('roomdesc'=>$roomid,'mobile'=>$mobile);
		$res=$this->db->get_where('el_owner_contact',$condition);
		return $res->row_array();
	}

	//获取手机号码
	public function getMobiles($roomid)
	{
		$condition=array('roomdesc'=>$roomid);
		$this->db->select('name,mobile');
		$res=$this->db->get_where('el_owner_contact',$condition);
		return $res->result();
	}

	//禁止子账号使用
	public function cancelAccount($mobile,$commid)
	{
		$condition = array('telNo' => $mobile,'commid' => $commid);
		$data = array('status' => '9','del_time' => date('Y-m-d H:i:s'));
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}
}

/* End of file Owner_Model.php */
/* Location: ./application/models/Owner_Model.php */