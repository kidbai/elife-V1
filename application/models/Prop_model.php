<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prop_model extends MY_Model {

	public $tableName = "el_prop_manager";

	public function __construct()
	{
		parent::__construct('Prop_model');
	}

	/**
	 * 验证物业登录
	 * @Date 2015-06-18 16:38:44
	 * @author npctest
	 * 
	 * @param username -> 登录帐号
	 * @param password -> 登录密码
	 */ 
	public function validateAccount($username,$password)
	{
		$condition = array('account' => $username,'pwd' => $password);
		$res = parent::queryObjectData($this->tableName,$condition);
		//log_message('INFO','登录信息：'.json_encode($res));
		if(isset($res['account'])){
			$data = array('last_time' => date("Y-m-d H:i:s"));
			parent::updateObject($this->tableName,$data,$condition);
			$back = parent::queryObjectData($this->tableName,$condition);
			return $back;
		}else{
			return false;
		}
	}
}

/* End of file Prop_model.php */
/* Location: ./application/models/Prop_model.php */