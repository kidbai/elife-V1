<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	* 职员model类
	*/
class Staff_model extends MY_Model
{
	protected $tableName = 'el_staff';

	function __construct()
	{
		parent::__construct('Staff_model');
	}

	public function getStaffInfo($commid,$code)
	{
		$condition=array('comm_id'=>$commid,'code' => $code);
		$res=parent::queryObjectData($this->tableName,$condition);
		return $res;
	}

	public function getStaffList($commid,$count,$currentSize)
	{
		$condition = array('comm_id' => $commid);
		$res = parent::queryObject($this->tableName,$condition,$count,$currentSize);
		return $res;
	}

	public function getAllStaff($commid)
	{
		$condition = array('comm_id' => $commid);
		$res = parent::queryObject($this->tableName,$condition);
		return $res;
	}

	public function addGood($staffId,$operType)
	{
		$operAction=intval($operType)==0?"month_good+1":"month_good-1";
		$data=array("month_good"=>$operAction);
		$strWhere=array("sid"=>$staffId);
		$res=parent::updateObject($this->tableName,$data,$strWhere,FALSE);
		return $res;
	}

	public function addStaffComment($data)
	{
		$res=parent::insertObject('el_staff_comment',$data);
		return $res;
	}

}
?>