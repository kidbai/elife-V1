<?php defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	* 城市数据模型
	*
	* @author	顾小松(npctest@cdhexinyi.com)
	* @date		2015-04-06 18:23:12         
	* @since	1.0.0
	*/
	class City_model extends MY_Model{

		
		protected $tableName = 'el_city';

		public function __construct()
		{
			parent::__construct('City_model');		
		}

		/**
		 * 添加城市方法
		 * 
		 * @param [array] $data [城市数组[id,name]]
		 * @return bool 是否添加成功（0:失败 1：成功）
		 */
		public function addCity($data)
		{
			$res=parent::insertObject($this->tableName,$data);
			return $res;
		}

		
		/**
		 * 根据城市缩写查询具体城市名称
		 * 
		 * @param [string] $code [城市首字母缩写]
		 * @return array 城市数据集
		 */
		public function queryCityByCode($code)
		{
			$result=parent::queryObject($this->tableName,array('code'=>$code));
			return $result;
		}

		/**
		 * 查询所有所有城市数据
		 * 
		 * @param  [type] $strwhere [description]
		 * @param  [type] $num      [description]
		 * @param  [type] $offset   [description]
		 * @return [type]           [description]
		 */
		public function queryAllCity()
		{
			$res = parent::queryTable($this->tableName);
			return $res;
		}

		/**
		 * 根据条件查询城市数据
		 * 
		 * @param  [type] $fields   [description]
		 * @param  [type] $strwhere [description]
		 * @param  [type] $strOrder [description]
		 * @param  [type] $num      [description]
		 * @param  [type] $offset   [description]
		 * @return [type]           [description]
		 */
		public function queryCityByWhere($fields,$strwhere,$strOrder,$num,$offset)
		{

			$res = parent::queryObject($this->tableName,$fields,$strwhere,$strOrder,$num,$offset);
			return $res;
		}


		/**
		 * 根据城市系统id删除对应城市数据[真实删除]
		 * 
		 * @param [int] $cid [城市系统id号]
		 * @return bool true/flase
		 */
		public function delCityById($sid)
		{
			$res=parent::deleteObject($this->tableName,array('sid'=>$sid));
			return $res;
		}

		public function updateCity($data,$sid)
		{
			$res=parent::updateObject($this->tableName,$data,array('sid'=>$sid));
			return $res;
		}
	}
	
?>	