<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Community_model extends MY_Model {

	protected $tableName = 'el_community';

	public function __construct()
	{
		parent::__construct('Community_model');	
	}

	public function addCommunity($data,$land_id,$property_id)
	{
		$strWhere=array('land_id'=>$land_id,'property_id'=>$property_id);
		$res=parent::insertObject($this->tableName,$data,$strWhere);
		return $res;
	}

	public function getAllCommunity($num,$offset)
	{
		$res=parent::queryObject($this->tableName,$num,$offset);
		return $res;
	}

	public function getAllCommunityByCid($cid)
	{
		$res=parent::queryObject($this->tableName,array('city_id'=>$cid));
		return $res;
	}

	public function getAllRoom($commid)
	{
		$sql="select
			el_community.`name`,
			el_building.building,
			el_unit.unit,
			el_floor.floor,
			el_room.remarks,
			CONCAT(el_community.sid ,'-',el_building.building,'-',el_unit.unit,'-',el_room.remarks) as roomdesc
			from el_community 
			join el_building on el_building.community_id=el_community.sid
			join el_unit on el_unit.building_id=el_building.sid
			join el_floor on el_floor.unit_id=el_unit.unit
			join el_room on el_floor.floor=el_room.floor_id
			where community_id=".$commid." 
			order by community_id,building_id,unit_id,floor_id,remarks";
		$res=parent::queryObject($sql);
		return $res;
	}


	public function getCommunityById($type,$keywords,$num,$offset)
	{
		$no=$type==0?'land_id':'property_id';
		$strWhere=array($no=>$keywords);
		$res=parent::queryObject($this->tableName,$strwhere,$num,$offset);
		return $res;
	}

	public function getCommdesc($commid)
	{
		$condition = array('sid' => $commid);
		$res = parent::queryObjectData($this->tableName,$condition);
		return $res;
	}

	public function updCommunityById($data,$sid)
	{
		$strwhere=array('sid'=>$sid);
		$res=parent::updateObject($this->tableName,$data,$strWhere);
		return $res;
	}

	public function delCommunityById($sid)
	{
		$strWhere=array('sid'=>$sid);
		$res=parent::deleteObject($this->tableName,$sid);
		return $res;
	}
}

/* End of file Community_Model.php */
/* Location: ./application/models/Community_Model.php */