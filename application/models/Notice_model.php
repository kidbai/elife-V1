<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notice_model extends MY_Model {

	protected $tableName = "el_notice";

	public function __construct()
	{
		parent::__construct('Notice_model');		
	}

	/**
	 * 查询小区动态信息
	 * @Date 2015-06-17 15:57:12
	 * @author npctest
	 * 
	 * @param commid -> 小区编号
	 * @param pageSize -> 条数
	 * @param currentSize ->当前偏移量(当前页开始索引数)
	 * @return res(数组对象) -> 返回数据集合
	 */ 
	public function getNotice($commid,$pageSize,$currentSize)
	{
		$condition = array('commid' => $commid);
		$column = "sid,title,add_time,level";
		$res = parent::queryObject($this->tableName,$column,$condition,$pageSize,$currentSize);
		//log_message('INFO',json_encode($res));
		return $res;
	}

	/**
	 * 查询小区动态详情
	 * @Date 2015-06-18 10:36:57
	 * @author npctest
	 * 
	 * @param nid -> 小区动态编号
	 */ 
	public function getNoticeDetail($nid)
	{
		$condition = array('sid' => $nid);
		$res = parent::queryObject($this->tableName,$condition);
		return $res;
	}

	/**
	 * 新增小区动态
	 * @Date 2015-06-17 15:55:12
	 * @author npctest
	 * 
	 * @param data -> 新增信息的数据集合(array)
	 * @return res(bool) -> true/false
	 */ 
	public function addNotice($data)
	{
		$res = parent::insertObject($this->tableName,$data);
		return $res;
	}

	/**
	 * 更新小区动态信息
	 * @Date 2015-06-17 15:54:06
	 * @author npctest
	 * 
	 * @param data -> 所需更新的数据集合(array)
	 * @param nid -> 小区动态编号
	 * @return res(bool) -> true/false
	 */ 
	public function updateNotice($nid,$data)
	{
		$condition = array('sid' => $nid);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 物业删除(更新状态为9，不可见)
	 * @Date 2015-06-17 15:51:59
	 * @author npctest
	 * 
	 * @param nid -> 小区动态编号
	 * @return res(bool) -> true/false
	 */ 
	public function updateDelNotice($nid)
	{
		$condition = array('sid' => $nid);
		$data = array('status' => 9);
		$res = parent::updateObject($this->tableName,$data,$condition);
		return $res;
	}

	/**
	 * 服务中心删除(数据库真实删除：慎用！)
	 * @Date 2015-06-17 15:53:00
	 * @author npctest
	 * 
	 * @param nid -> 小区动态编号
	 * @return res(bool) -> true/false
	 */ 
	public function deleteNotice($nid)
	{
		$condtion = array('sid' => $nid);
		$res = parent::deleteObject($this->tableName,$condition);
		return $res;
	}
}

/* End of file News_model.php */
/* Location: ./application/models/News_model.php */